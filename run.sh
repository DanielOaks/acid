#!/bin/bash

JAR=acid/target/acid-acid-4.0-SNAPSHOT-jar-with-dependencies.jar
# TODO: LD_PRELOAD configurable here?
java -XX:+HeapDumpOnOutOfMemoryError -Xms64m -Xmx256m -jar $JAR >geoserv.log 2>&1 &
echo "Acidictive started"

