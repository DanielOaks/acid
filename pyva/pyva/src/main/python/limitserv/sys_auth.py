from pseudoclient.sys_auth import AuthManager

class LimitServAuthManager(AuthManager):
	def __init__(self, module):
		AuthManager.__init__(self, module)

	def onAccept(self, user, request, action, channel):
		if action == 'news':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', True)
				self.module.msg(user, 'Enabled news in @b%s@b.' % channel)
		elif action == 'nonews':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', False)
				self.module.msg(user, 'Disabled news in @b%s@b.' % channel)
		else:
			return False

		return True
	
	def reject_not_enough_users(self, user, channel):
		self.reject(user, 'Channel @b%s@b does not have enough users to request %s.' % (channel, self.module.config.get('limitserv', 'nick')))
