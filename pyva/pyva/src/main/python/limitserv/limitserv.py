#!/usr/bin/python pseudoserver.py
#psm-limitserv.py
# module for pypseudoserver
# written by celestin - martin <martin@rizon.net>

import sys
import types
from istring import istring
from pseudoclient import sys_log, sys_options, sys_channels, inviteable
from utils import *

from pyva import *
import logging
from core import *
from plugin import *

import cmd_admin, sys_auth, limitmanager

class limitserv(
	AcidPlugin,
	inviteable.InviteablePseudoclient
):
	initialized = False
	
	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.auth.start()
		self.limit_monitor.start()
		
	def bind_function(self, function):
		func = types.MethodType(function, self, limitserv)
		setattr(limitserv, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'j', 'callback': self.bind_function(list[command][0]), 
												'usage': list[command][1]}))

	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "limitserv"
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(self.config.get('limitserv', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'limitserv:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(self.config.get('limitserv', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'limitserv:channel' configuration option: %s" % err)
			raise
		
		self.bind_admin_commands()
					
	def start(self):
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS limitserv_chans (item SMALLINT(5) NOT NULL AUTO_INCREMENT, channel VARCHAR(35), \
				PRIMARY KEY (item), UNIQUE KEY (channel));")
		except Exception, err:
			self.log.exception("Error creating table for limitserv module (%s)" % err)
			raise
		
		try:
			AcidPlugin.start(self)
			inviteable.InviteablePseudoclient.start(self)

			self.options = sys_options.OptionManager(self)
			self.elog = sys_log.LogManager(self)
			self.auth = sys_auth.LimitServAuthManager(self)
			self.channels = sys_channels.ChannelManager(self)
			self.limit_monitor = limitmanager.LimitManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for limitserv module (%s)' % err)
			raise
			
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')
		
		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for limitserv module (%s)' % err)
			raise

		self.initialized = True
		self.elog.debug('Started threads.')
		return True

	def onSync(self):
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.log.debug('Joined channels.')
	
	def stop(self):
		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()
		
		if hasattr(self, 'limit_monitor'):
			self.limit_monitor.stop()
			
	def join(self, channel):
		me = self.inter.findUser(self.nick)
		me.joinChan(channel)
		self.msg('ChanServ', 'OP %s' % channel)                   # and the channel is not empty. For now, use /cs op

	def part(self, channel):
		me = self.inter.findUser(self.nick)
		me.partChan(channel)

	def msg(self, target, message):
		if message != '':
			self.inter.privmsg(self.nick, target, format_ascii_irc(message))

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count
		
	def notice(self, target, message):
		if message != '':
			self.inter.notice(self.nick, target, format_ascii_irc(message))
	
	def onPrivmsg(self, source, target, message):
		if inviteable.InviteablePseudoclient.onPrivmsg(self, source, target, message) and \
			target == self.nick:
			# if inviteable fell through, we don't have anything else to do
			self.msg(source, 'Invalid message. Say help for a list of valid messages.')

	def do_accept(self, nick, channel):
		chan = self.inter.findChannel(channel)
		if chan.size() < self.options.get('required_users', int, 20):
			self.auth.reject_not_enough_users(nick, channel)
		else:
			# super call
			inviteable.InviteablePseudoclient.do_accept(self, nick, channel)

	def onChanModes(self, prefix, channel, modes):
		if not self.initialized:
			return

		if not modes == '-z':
			return

		if channel in self.channels:
			self.channels.remove(channel)
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)
	
	def onJoin(self, user, channel):
		if self.channels.is_valid(channel) and channel not in self.limit_monitor:
			self.limit_monitor.insert(channel)
	
	def onPart(self, user, channel):
		self.onJoin(None, channel)

	def onKick(self, kicker, victim, channel, reason):
		self.onJoin(None, channel)
	
	def getCommands(self):
		return self.commands_admin
