import json
import re
from urllib import urlencode
from feed import HtmlFeed, XmlFeed, get_json
from utils import unescape

class Google(object):
	def __init__(self):
		pass
	
	# Google shutdown their iGoogle "API", 
	#def calc(self, expr):
	#	url = 'http://www.google.com/ig/calculator?'
	#	url += urlencode({'q': expr})
	#	page = HtmlFeed(url) #, fake_ua=True)
	#	text = page.html()
	#	# Convert to valid JSON: {foo: "1"} -> {"foo" : "1"},
	#	#                        {"foo": "\x25"} -> {"foo": "\u0025"}
	#	result = re.sub("([a-z]+):", '"\\1" :', text)
	#	# XXX: HACK; using two substitutes instead of one, but I can't into regex
	#	result = re.sub('\\\\x([0-9a-f]{2})', '\\u00\\1', result)
	#	result = re.sub('\\\\x([0-9a-f]{1})', '\\u000\\1', result)
	#
	#	result = unicode(result, 'unicode_escape')
	#	result = unescape(result)
	#	# Special case: fractions; those are <sup></sup> unicode-slash <sub></sub>
	#	result = re.sub(u'<sup>([^<]*)</sup>\u2044<sub>([^<]*)</sub>', ' \\1/\\2', result)
	#	# Also un-html-ify <sup>/</sup> because google doesn't send proper plaintext
	#	result = re.sub('<sup>([^<]*)</sup>', '^\\1', result)
	#	# Same with <sub>.
	#	result = re.sub('<sub>([^<]*)</sub>', '_\\1', result)
	#	result = json.loads(result.encode('utf-8'))
	#	return result
	
	def search(self, query, userip=None):
		url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&'
		parameters = {'q': query}
		if userip:
			parameters['userip'] = userip
		url += urlencode(parameters)
		json = get_json(url)
		return json
	
	def yt_search(self, query, num=1):
		url = 'https://gdata.youtube.com/feeds/api/videos?v=2&max-results=5&'
		url += urlencode({'q': query})
		xml = XmlFeed(url)
		entry = xml.elements('/feed/entry[%d]' % num)
		if not entry:
			return None
		
		v = entry[0]
		return {
			'title': v.text('title'),
			'duration': v.int('media:group/yt:duration/@seconds'),
			'uploaded': v.text('media:group/yt:uploaded'),
			'id': v.text('media:group/yt:videoid'),
			'rating': v.decimal('gd:rating/@average'),
			'rate_count': v.int('gd:rating/@numRaters'),
			'favorite_count': v.int('yt:statistics/@favoriteCount'),
			'view_count': v.int('yt:statistics/@viewCount'),
			'liked': v.int('yt:rating/@numLikes'),
			'disliked': v.int('yt:rating/@numDislikes')
			}
