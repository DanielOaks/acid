from pseudoclient.collection import *
from pseudoclient.sys_users import *

class InternetsUser(CollectionEntity):
	location = None

	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

class InternetsUserManager(UserManager):
	def __init__(self, module):
		UserManager.__init__(self, module, InternetsUser)
