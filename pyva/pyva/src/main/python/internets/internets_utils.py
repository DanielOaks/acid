
def get_tempcolor(temp):
	if temp == None:
		return ''
	
	if temp < -20:
		code = '@c12@b'
	elif temp < -10:
		code = '@c12'
	elif temp < 0:
		code = '@c10'
	elif temp < 10:
		code = '@c09'
	elif temp < 20:
		code = '@c08'
	elif temp < 30:
		code = '@c07'
	elif temp < 35:
		code = '@c04'
	else:
		code = '@c04@b'
	
	return code
	
def format_temperature(temp):
	code = get_tempcolor(temp)
	return '%s%s' % (code, temp)

def format_weather(code, message):
	return message.replace('@sep', '%s::@o' % code)

