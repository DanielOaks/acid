from pseudoclient.collection import *
from pseudoclient.sys_channels import *

class ErepublikChannel(CollectionEntity):
	citizen = None
	company = None
	mass = False
	news = False

	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

class ErepublikChannelManager(ChannelManager):
	def __init__(self, module):
		ChannelManager.__init__(self, module, ErepublikChannel)
