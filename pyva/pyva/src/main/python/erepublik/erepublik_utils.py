
class Object:
	def __init__(self, **entries): 
		self.__dict__.update(entries)
	
	def __repr__(self): 
		return '<%s>' % str('\n '.join('%s : %s' % (k, repr(v)) for (k, v) in self.__dict__.iteritems()))

def format_ordinal(n, add_thousands_sep=False):
	if add_thousands_sep:
		nstr = format_thousand(n)
	else:
		nstr = str(n)
	suffix = {1: 'st', 2: 'nd', 3: 'rd'}.get(4 if 10 <= n % 100 < 20 else n % 10, "th")
	return nstr + suffix

def format_citizen_message(citizen, message, sex=None):
	if citizen == None:
		return message
	else:
		if hasattr(citizen, 'sex'):
			sex = citizen.sex.lower() if citizen.sex != None else ''
		else:
			sex = sex.lower() if sex else ''
	
	if (sex == 'x'):
		message = message.replace('@sep', '@b@c14::@o')
	elif (sex == 'f'):
		message = message.replace('@sep', '@b@c13::@o')
	
	return message

