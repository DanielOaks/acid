from pseudoclient.collection import *
from pseudoclient.sys_users import *

class ErepublikUser(CollectionEntity):
	citizen = None
	company = None
	mass = False

	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

class ErepublikUserManager(UserManager):
	def __init__(self, module):
		UserManager.__init__(self, module, ErepublikUser)
