import feed
import utils
from feed import get_json, HtmlFeed, XmlFeed
from datetime import datetime
from decimal import Decimal
from urllib import quote
from ..erepublik_utils import Object

def from_id(id):
	return Citizen(XmlFeed('http://api.erepublik.com/v2/feeds/citizens/%d' % int(id)))

def from_name(name):
	if isinstance(name, unicode):
		name = name.encode('utf-8')
	
	return Citizen(XmlFeed('http://api.erepublik.com/v2/feeds/citizen_by_name/xml/%s' % quote(name)))

def from_dict(dict):
	return from_id(dict['id'])

def from_heapy(s, by_id=False):
	c = get_json('http://api.1way.it/erep/citizen/%s/%s' % ('id' if by_id else 'name', s))
	if 'error' in c:
		raise feed.FeedError(c['error'])
	
	c['strength'] = Decimal(c['strength'])
	c['rank_points'] = c['rank']['points']
	c['rank'] = utils.get_rank_by_points(c['rank_points'])
	c['birth_date'] = datetime.strptime(c['birth_date'], '%Y-%m-%d %H:%M:%S')
	c['age'] = (datetime.utcnow() - c['birth_date']).days
	c['citizenship'] = c['citizenship']['country']
	c['country'] = c['residence']['country']
	c['region'] = c['residence']['region']
	c['is_organization'] = False
	c['employer'] = None # who cares
	c['is_party_member'] = False
	c['party'] = None # todo (?)
	c['avatar'] = c['avatar_link']
	return Object(**c)
	
def from_html(s):
	page = HtmlFeed('http://www.erepublik.com/en/citizen/profile/%d' % s)
	cname = page.html().split('<title>', 1)[1].split('</title>')[0].rsplit(' -', 1)[0]
	get_json('http://api.1way.it/erep/citizen/add/%d' % s)
	return Object(**{'id': s, 'name': cname})

class Citizen:
	"""A citizen of Erepublik"""

	def __init__(self, f):
		self.id = f.int('/citizen/id')
		self.name = f.text('/citizen/name')
		self.sex = f.text('/citizen/sex')
		self.avatar = f.text('/citizen/avatar-link')
		self.strength = f.decimal('/citizen/military-skills/military-skill[1]/points', Decimal('0'))
		self.rank_points = f.int('/citizen/military/rank-points', 0)
		self.stars = f.int('/citizen/military/stars', 0)
		self.rank = utils.get_rank_by_points(self.rank_points)
		self.damage = f.int('/citizen/military/total-damage', 0)
		self.fights = f.int('/citizen/military/fight-count', 0)
		self.level = f.int('/citizen/level')
		self.experience = f.int('/citizen/experience-points')
		self.wellness = f.decimal('/citizen/wellness')
		bd = f.text('/citizen/date-of-birth')
		self.birth_date = datetime.strptime(bd[:-1], '%Y-%m-%dT%H:%M:%S')
		self.age = (datetime.utcnow() - self.birth_date).days
		self.citizenship = {
			'name': f.text('/citizen/citizenship/country/name'),
			'id': f.int('/citizen/citizenship/country/id'),
			'code': f.text('/citizen/citizenship/country/code')}
		self.country = {
			'name': f.text('/citizen/residence/country/name'),
			'id': f.int('/citizen/residence/country/id'),
			'code': f.text('/citizen/residence/country/code')}
		self.region = {
			'name': f.text('/citizen/residence/region/name'),
			'id': f.int('/citizen/residence/region/id')}
		self.is_general_manager = f.bool('/citizen/is-general-manager')
		self.is_congressman = f.bool('/citizen/is-congressman')
		self.is_president = f.bool('/citizen/is-president')
		self.is_organization = f.bool('/citizen/is-organization')

		emp = f.text('/citizen/employer/name')

		if emp == None:
			self.employer = None
		else:
			self.employer = {'name': emp, 'id': f.int('/citizen/employer/id')}

		party = f.text('/citizen/party/name')

		if party == None:
			self.party = None
			self.is_party_member = False
		else:
			self.is_party_member = True
			self.party = {'name': party, 'id': f.int('/citizen/party/id'), 'president': f.bool('/citizen/party/president')}

		self.medals = dict(
			[medal.text('type'), medal.int('amount')]
			for medal in f.elements('/citizen/medals/medal') if medal.int('amount') > 0)

	def __str__(self):
		return '[%d] %s' % (self.id, self.name)
