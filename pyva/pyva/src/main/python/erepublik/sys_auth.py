from pseudoclient.sys_auth import AuthManager

class ErepublikAuthManager(AuthManager):
  def __init__(self, module):
    AuthManager.__init__(self, module)

  def onAccept(self, user, request, action, channel):
	if action == 'news':
		if self.module.channels.is_valid(channel):
			self.module.channels.set(channel, 'news', True)
			self.module.msg(user, 'Enabled news in @b%s@b.' % channel)
	elif action == 'nonews':
		if self.module.channels.is_valid(channel):
			self.module.channels.set(channel, 'news', False)
			self.module.msg(user, 'Disabled news in @b%s@b.' % channel)
	elif action == 'chanmass':
		if self.module.channels.is_valid(channel):
			self.module.channels.set(channel, 'mass', True)
			self.module.msg(user, 'Enabled mass in @b%s@b.' % channel)
	elif action == 'nochanmass':
		if self.module.channels.is_valid(channel):
			self.module.channels.set(channel, 'mass', False)
			self.module.msg(user, 'Disabled mass in @b%s@b.' % channel)
	elif action == 'usermass':
#		if self.module.users.is_valid(channel):
			self.module.users.set(channel, 'mass', True)
			self.module.msg(user, 'Enabled mass for @b%s@b.' % channel)
	elif action == 'nousermass':
#		if self.module.users.is_valid(channel):
			self.module.users.set(channel, 'mass', False)
			self.module.msg(user, 'Disabled mass for @b%s@b. To re-enable type @b/msg eRepublik usermass@b' % channel)
	else:
		return False

	return True
