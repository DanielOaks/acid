
import pseudoclient.sys_base

from datetime import datetime
from utils import *

#---------------------------------------------------------------------#

from pseudoclient import cmd_admin
from pseudoclient.cmd_admin import \
  admin_unregistered, \
  admin_log, \
  admin_msg, \
  admin_opt, \
  admin_db

#---------------------------------------------------------------------#

def admin_chan(self, source, target, pieces):
  def subcmd_highlight(self, action, channel, source, target, pieces):
    if channel in self.channels:
      cur_state = self.channels[channel].highlight
      self.channels.set(channel, 'highlight', not cur_state)
      self.msg(target, 'Toggled highlight for channel @b%s@b. Current state: %s.' % (channel, 'disabled' if cur_state else 'enabled'))
    else:
      self.msg(target, 'Channel @b%s@b is not in the database.' % channel)

    return True

  return cmd_admin.admin_chan(self, source, target, pieces, meta={
      'cmds':{
        'highlight': subcmd_highlight,
        'm': subcmd_highlight
      },
      'extra_info': lambda chan: ' News: @b%s@b. Highlight: @b%s@b.' \
        % ('enabled' if chan.news else 'disabled', 'enabled' if chan.highlight else 'disabled')
  })

#---------------------------------------------------------------------#

def admin_user(self, source, target, pieces):
  def extra_info(user):
    # i am too tired to think about how to write this as a lambda
    m = ''
    if user.citizen != None:
      m += ' Citizen: @b%s@b.' % user.citizen
    if user.company != None:
      m += ' Company: @b%s@b.' % user.company
    return m
    

  return cmd_admin.admin_user(self, source, target, pieces, meta={
    'extra_info': extra_info
  })

#---------------------------------------------------------------------#

def admin_sys(self, source, target, pieces):
  if len(pieces) < 2:
    return False

  starget = pieces[0]
  operation = pieces[1]
  names = []
  subsystems = []

  if 'o' in starget:
    names.append('options')
    subsystems.append(self.options)

  if 'u' in starget:
    names.append('users')
    subsystems.append(self.users)

  if 'c' in starget:
    names.append('channels')
    subsystems.append(self.channels)

  if 'a' in starget:
    names.append('auth')
    subsystems.append(self.auth)

  if 'f' in starget:
    names.append('antiflood')
    subsystems.append(self.antiflood)

#  if 'r' in starget:
#    names.append('article')
#    subsystems.append(self.article)

  if len(names) == 0:
    return False

  if operation in ['u', 'update']:
    for subsystem in subsystems:
      subsystem.force()

    self.msg(target, 'Forced update for @b%s@b.' % '@b, @b'.join(names))
  elif operation in ['r', 'reload']:
    for subsystem in subsystems:
      subsystem.reload()

    self.msg(target, 'Forced reload for @b%s@b.' % '@b, @b'.join(names))
  elif operation in ['d', 'delay']:
    if len(pieces) == 2:
      for subsystem in subsystems:
        self.msg(target, 'Auto-update delay for @b%s@b is %d seconds.' % (subsystem.name, subsystem.delay))
    else:
      try:
        seconds = int(pieces[2])
      except:
        return False

      if seconds < 10:
        self.msg(target, 'Auto-update delay must be greater than 10 seconds.')
        return True

      for subsystem in subsystems:
        subsystem.set_option('update_period', seconds)
        subsystem.reload()

      self.msg(target, 'Auto-update delay for @b%s@b set to @b%d@b seconds.' % ('@b, @b'.join(names), seconds))
  else:
    return False

  return True

def admin_status(self, source, target, pieces):
  if len(pieces) < 1:
    pass
  elif pieces[0] == 'on':
    self.online = True
    self.offline_msg = None
  elif pieces[0] == 'off':
    self.online = False
    self.offline_msg = ' '.join(pieces[1:]) if len(pieces) > 1 else None
  else:
    return False

  self.msg(target, 'Module status: @b%s@b' % ('online' if self.online else 'offline'))
  return True

def admin_stats(self, source, target, pieces):
  self.msg(target, 'Registered users: @b%d@b.' % len(self.users.list_all()))
  self.msg(target, 'Registered channels: @b%d@b.' % len(self.channels.list_all()))
  return True

def get_commands():
  return {
    'chan'       : (admin_chan,            '<ban|unban|info|add|remove|list|blist|highlight> <channel> [reason]'),
    'unreg'      : (admin_unregistered,    '<check|list|part> - remove unregistered channels'),
    'user'       : (admin_user,            '<ban|unban|info|add|remove|list|blist|highlight> <user> [reason]'),
    'stats'      : (admin_stats,           'counts registered users and channels'),
    'db'         : (admin_db,              '[on|off] - enables/disables auto commits to db'),
    'opt'        : (admin_opt,             '[get|set|clear] [option] [value] - manipulates options (list all if no arguments)'),
    'sys'        : (admin_sys,             '<subsystem> <operation> [value] - (subsystems: options (o), users (u), channels (c), news (n), auth (a), antiflood (f)) (operations: update (u), reload (r), delay (d))'),
    'log'        : (admin_log,             '[level] - gets or sets the log level (0-7).'),
    'msg'        : (admin_msg,             '<message> - sends a message to all channels'),
    'status'     : (admin_status,          '<on/off> - disables/enables API commands (use when API is down)'),
  }

