
def format_citizen_message(citizen, message, sex=None):
  if citizen == None:
    return message
  else:
    if hasattr(citizen, 'sex'):
      sex = citizen.sex.lower() if citizen.sex != None else ''
    else:
      sex = sex.lower() if sex else ''
  
  if (sex == 'x'):
    message = message.replace('@sep', '@b@c14<>@o')
  elif (sex == 'f'):
    message = message.replace('@sep', '@b@c13<>@o')
  
  return message