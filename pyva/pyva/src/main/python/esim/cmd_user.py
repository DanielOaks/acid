import itertools
import random
import re
from datetime import datetime
from decimal import Decimal, InvalidOperation
from math import ceil
from operator import itemgetter
from api import citizen, feed, region, utils
from pseudoclient.cmd_manager import *
from utils import *
from esim_utils import *

def command_citizen_register(self, manager, opts, arg, channel, sender):
  try:
    c = citizen.from_id(int(arg)) if 'id' in opts else citizen.from_name(arg)
  except ValueError, e:
    self.errormsg(channel, '%s is not a valid id.' % arg)
    return
  except feed.FeedError, e:
    self.errormsg(channel, e.msg)
    return

  if c.is_organization:
    self.errormsg(channel, "%s, @b%s@b is an organization, you cannot register an organization under your nick." % (sender, c.name))
    return

  self.users.set(sender, 'citizen', c.id)
  self.msg(channel, '%s: registered e-Sim citizen @b%s@b' % (sender, c.name))

def command_citizen_info(self, manager, opts, arg, channel, sender):
  c = self.get_citizen(opts, arg, channel, sender)

  if c == None:
    return

  if not c.is_organization:
    message = u"""@b{c.name}@b [{c.id}] @sep @bLevel@b: {c.level} [{c.exp}] @sep @bStrength@b: {c.strength} @sep \
@bRank@b: {c.rank[name]} [{c.rank[id]}] @sep @bEconomy skill@b: {c.economySkill} @sep @bCitizenship@b: {c.citizenship} \
@sep @bToday's damage@b: {dmgToday} @sep @bTotal damage@b: {dmgTotal}""".format(
      c        = c,
      dmgToday = format(c.dmgToday, ',d'),
      dmgTotal = format(c.dmgTotal, ',d')
      )
  else:
    message = "@b%s@b [%d] @sep @bOrganization@b @sep @bLocation@b: %s" % (c.name, c.id, c.citizenship)
  
  self.msg(channel, format_citizen_message(c, message))

def command_citizen_fightcalc(self, manager, opts, arg, channel, sender):
  fights = 1 if 'fights' not in opts or 'objective' in opts else opts['fights']
  if fights >= 100000 or fights == 0:
    self.errormsg(channel, 'fights: expected value (0 <= x <= 100000), got %d instead' % fights)
    return

  if 'rank' in opts and 'strength' in opts:
    c = None
  else:
    c = self.get_citizen(opts, arg, channel, sender)
  
    if c == None:
      return
    
    if c.is_organization:
      self.errormsg(channel, '%s is an organization.' % c.name)
      return
  
  colors = [3, 12, 4, 7, 9, 10]
  rank = c.rank if 'rank' not in opts else opts['rank']
  strength = c.strength if 'strength' not in opts else opts['strength']

  if 'influence' in opts:
    if not c:
      self.errormsg(channel, 'option -I needs a citizen')
      return

  if 'objective' in opts:
    objective = opts['objective']
    if objective >= 10000000000:
      self.errormsg(channel, 'option -o out of range')
      return
    
    obj_str = 'fights required for {inf:,} '.format(inf=objective)
  else:
    objective = None
    obj_str = ''
  
  region_bonus = True if 'region_bonus' in opts else False
  surrounded = True if 'surrounded' in opts else False
  ds_bonus = opts['defense_system'] if 'defense_system' in opts else 0
  mu_bonus = opts['mu_bonus'] if 'mu_bonus' in opts else 0
  
  damage = [utils.fight_calc(q, rank, strength, region_bonus, ds_bonus, mu_bonus, surrounded) for q in range(6)]
  if objective:
    damage = [1 + (objective / d) if objective % d != 0 else objective / d for d in damage]
  damagestr = ['@c%d[Q%d: @b%d@b]@c' % (z[0], q, fights * z[1]) for q, z in enumerate(zip(colors, damage))]
  
  message = '@b%(name)s@b(%(rank)s, %(strength)s strength) %(obj)sinfluence%(fights)s @sep %(damage)s' % {
    'name'    : '%s ' % format_name(c.name) if c else '',
    'rank'    : '%s [%d]' % (rank['name'], rank['id']),
    'strength': strength,
    'obj'     : obj_str,
    'fights'  : ' in %d fights' % fights if fights != 1 else '',
    'damage'  : ' '.join(damagestr)}
  
  self.msg(channel, format_citizen_message(c, message))

def command_citizen_link(self, manager, opts, arg, channel, sender):
  c = self.get_citizen(opts, arg, channel, sender)

  if c == None:
    return

  # Here
  server = 'secura' if 'secura' in opts else 'primera'
  message = '@b%(name)s@b link @sep http://%(server)s.e-sim.org/profile.html?id=%(id)d' % {
    'server': server,
    'name' : format_name(c.name),
    'id'   : c.id}
  
  self.msg(channel, format_citizen_message(c, message))

def command_citizen_donate(self, manager, opts, arg, channel, sender):
  c = self.get_citizen(opts, arg, channel, sender)

  if c == None:
    return

  # Here
  server = 'secura' if 'secura' in opts else 'primera'
  self.msg(channel, format_citizen_message(c, """@b%(name)s@b donations @sep @bMoney@b http://%(server)s.e-sim.org/donateMoney.html?id=%(id)d @sep \
@bItems@b http://%(server)s.e-sim.org/donateProducts.html?id=%(id)d""") % {
    'server': server,
    'name' : format_name(c.name),
    'id'   : c.id})

def command_citizen_companies(self, manager, opts, arg, channel, sender):
  c = self.get_citizen(opts, arg, channel, sender)

  if c == None:
    return

  # Here
  server = 'secura' if 'secura' in opts else 'primera'
  message = '@b%(name)s@b companies @sep http://%(server)s.e-sim.org/companies.html?id=%(id)d' % {
    'server': server,
    'name' : format_name(c.name),
    'id'   : c.id}
  
  self.msg(channel, format_citizen_message(c, message))

def command_citizen_message(self, manager, opts, arg, channel, sender):
  c = self.get_citizen(opts, arg, channel, sender)

  if c == None:
    return

  # Here
  server = 'secura' if 'secura' in opts else 'primera'
  self.msg(channel, format_citizen_message(c, '@b%(name)s@b message link @sep http://%(server)s.e-sim.org/composeMessage.html?id=%(id)d') % {
    'server': server,
    'name' : format_name(c.name),
    'id'   : c.id})

def command_region_info(self, manager, opts, arg, channel, sender):
  r = self.get_region(opts, arg, channel, sender)

  if r == None:
    return

  self.msg(channel, '@b%(region)s, %(country)s@b [%(id)d]%(cap)s @sep @bPopulation:@b %(pop)d%(raw)s%(cons)s%(borders)s' % {
    'region'   : r.name,
    'country'  : r.country['shortName'],
    'id'       : r.id,
    'cap'      : ' @sep @bCapital city@b' if r.is_capital else '',
    'pop'      : r.population,
    'raw'      : ' @sep @b%s@b level of @b%s@b resource@b' % (r.richness.title(), r.resource.title()) if r.richness else '',
    'cons'     : ' @sep @bDefense buildings:@b %s' % r.def_buildings if r.def_buildings else '',
    'borders'  : ' @sep @bBorders with:@b %s, and %s' % (', '.join(b['name'] for b in r.borders[:-1]), r.borders[-1]['name'])
    })

def command_highlight(self, manager, opts, arg, channel, sender):
  if not self.channels[channel].mass:
    self.notice(sender, 'The .hl command is disabled for this channel. To enable it, type @b/msg e-Sim highlight %s@b (founder only).' % channel)
    return
  
  chan = self.inter.findChannel(channel)
  senderinfo = self.inter.findUser(sender)
  if not chan or not senderinfo:
    return

  sender_modes = chan.getModes(senderinfo.getNick())
  if '@' not in sender_modes and '&' not in sender_modes and '~' not in sender_modes:
    self.notice(sender, 'This command is only available to channel ops, admins and founders.')
    return
  
  userlist = []
  members = chan.getUsers() # Set<String>
  it = members.iterator()
  while it.hasNext():
    name = it.next()

    u = self.inter.findUser(name)
    if not u or u['server'] in ['services.rizon.net', 'geo.rizon.net', 'py2.rizon.net', 'py3.rizon.net']:
      continue
    
    user = u['nick']
    userlist.append(user)
  
  if userlist:
    out = ''
    while len(userlist) > 0:
      if out:
        out += ' ' + userlist.pop()
      else:
        out += userlist.pop()
      if len(out) > 400:
        self.msg(channel, '@sep @bListen up!@b @sep %s @sep' % out)
        out = ''
    self.msg(channel, '@sep @bListen up!@b @sep %s @sep' % out)

def command_esim_info(self, manager, opts, arg, channel, sender):
  self.notice(sender, '@bRizon e-Sim Bot@b @sep @bDeveloper@b Digital_Lemon @sep @bThanks to@b @uZeroni@u, mink, ElChE and martin @sep @bHelp/feedback@b %(channel)s' % {
    'channel' : '#e-sim.bot'})

def command_esim_help(self, manager, opts, arg, channel, sender):
  command = arg.lower()

  if command == '':
    message = ['e-Sim: .help e-Sim - for e-Sim commands']
  elif command == 'e-sim':
    message = manager.get_help()
  else:
    message = manager.get_help(command)

    if message == None:
      message = ['%s is not a valid command.' % arg]

  for line in message:
    self.notice(sender, line)

id_opt = ('id', '-i', 'look up by id instead of name', {'action': 'store_true'}, ARG_YES)
nick_opt = ('nick', '-n', 'look up by irc nick', {'action': 'store_true'}, ARG_YES)
secura_opt = ('secura', '-S', 'uses Secura\'s API', {'action': 'store_true'}, ARG_YES)

class UserCommandManager(CommandManager):
  def get_prefix(self):
    return '.'

  def get_commands(self):
    return {
      'regcit': 'register_citizen',
      'regnick': 'register_citizen',
      'register_citizen': (command_citizen_register, ARG_YES, 'links an e-Sim citizen to an IRC nickname', [
        id_opt,
      ], 'citizen_name'),

      'lp': 'lookup',
      'lookup': (command_citizen_info, ARG_OPT, 'shows useful data about the citizen', [
        id_opt,
        nick_opt,
        secura_opt,
      ]),
        
      'fc': 'fightcalc',
      'fightcalc': (command_citizen_fightcalc, ARG_OPT, 'calculates influence done with different weapon qualities', [
        id_opt,
        nick_opt,
        secura_opt,
        ('strength', '-s', 'uses a custom strength', {'type': '+decimal'}, ARG_OFFLINE|ARG_OFFLINE_REQ),
        ('rank', '-r', 'uses a custom military rank (use rank ID, not name)', {'type': 'rank'}, ARG_OFFLINE|ARG_OFFLINE_REQ),
        ('region_bonus', '-b', 'adds region bonus influence (20%)', {'action': 'store_true'}, ARG_OFFLINE),
        ('defense_system', '-d', 'adds defense system bonus influence', {'type': 'ds_bonus'}, ARG_OFFLINE),
        ('mu_bonus', '-m', 'adds military unit bonus influence', {'type': 'mu_bonus'}, ARG_OFFLINE),
        ('surrounded', '-u', 'applies the surrounded debuff', {'action': 'store_true'}, ARG_OFFLINE),
        ('fights', '-f', 'specifies number of fights', {'type': '+integer'}, ARG_OFFLINE),
        ('objective', '-o', 'calculates how many fights are required to make a given amount of influence', {'type': '+integer'}, ARG_OFFLINE),
      ]),

      'link': (command_citizen_link, ARG_OPT, "return a link to the target citizen's profile", [
        id_opt,
        nick_opt,
        secura_opt,
      ]),

      'donate': (command_citizen_donate, ARG_OPT, 'link to the donation page of a citizen', [
        id_opt,
        nick_opt,
        secura_opt,
      ]),
      
      'companies': (command_citizen_companies, ARG_OPT, 'link to the citizen company page', [
        id_opt,
        nick_opt,
        secura_opt,
      ]),

      'igm': 'message',
      'message': (command_citizen_message, ARG_OPT, 'link to send a message to a citizen', [
        id_opt,
        nick_opt,
        secura_opt,
      ]),

      'region': (command_region_info, ARG_YES, 'information about a region', [
        id_opt
      ]),
      
      'hl': 'highlight',
      'highlight': (command_highlight, ARG_OPT, 'highlights all the nicks in the channel (limited to channel ops, admins and founders)', [], 'optional_message'),

      'info': (command_esim_info, ARG_NO|ARG_OFFLINE, 'Displays version and author information', []),
      'help': (command_esim_help, ARG_OPT|ARG_OFFLINE, 'Displays available commands and their usage', []),
    }
