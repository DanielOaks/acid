#!/usr/bin/python pseudoserver.py
# psm_esim.py
# module for pypseudoserver
# ground code taken from ElChE <elche@rizon.net> & martin <martin@rizon.net>
# adapted for e-Sim by Digital_Lemon
#
# esim module

import sys
import threading
import traceback
import types
from istring import istring
from datetime import datetime
from decimal import Decimal, InvalidOperation
from pseudoclient import sys_options, sys_log, cmd_manager, sys_antiflood
from utils import *

from pyva import *
import logging
from core import *
from plugin import *

from api import citizen, region, feed
import cmd_admin, cmd_private, cmd_user, esimparser, sys_auth, esim_users, esim_channels

class esim(AcidPlugin):
	def bind_function(self, function):
		func = types.MethodType(function, self, esim)
		setattr(esim, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'e', 'callback': self.bind_function(list[command][0]), 'usage': list[command][1]}))

	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "esim"
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(self.config.get('esim', 'nick'))
		except Exception, err:
			self.log.exception("Error reading 'esim:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(self.config.get('esim', 'channel'))
		except Exception, err:
			self.log.exception("Error reading 'esim:channel' configuration option: %s" % err)
			raise

		self.bind_admin_commands()

	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.users.start()
		self.auth.start()
		self.antiflood.start()

	def start(self):
		self.initialized = False

		try:
			self.options = sys_options.OptionManager(self)
			self.elog = sys_log.LogManager(self)
			self.commands_private = cmd_private.PrivateCommandManager()
			self.commands_user = cmd_user.UserCommandManager()
		except Exception, err:
			self.log.exception('Error initializing core subsystems for esim module (%s)' % err)
			raise

		self.elog.debug('Started core subsystems.')

		try:
			self.channels = esim_channels.EsimChannelManager(self)
			self.users = esim_users.EsimUserManager(self)
			self.auth = sys_auth.EsimAuthManager(self)
			self.antiflood = sys_antiflood.AntiFloodManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for esim module (%s)' % err)
			raise

		self.elog.debug('Started subsystems.')

		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.log.debug('Joined channels.')

		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for esim module (%s)' % err)
			raise

		self.initialized = True
		self.online = True
		self.elog.debug('Started threads.')
		return True

	def onSync(self):
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')

	def stop(self):
		if hasattr(self, 'antiflood'):
			self.antiflood.stop()

		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'users'):
			if self.initialized:
				self.users.force()

			self.users.stop()
			self.users.db_close()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()

	def join(self, channel):
		me = self.inter.findUser(self.nick)
		me.joinChan(channel)

	def part(self, channel):
		me = self.inter.findUser(self.nick)
		me.partChan(channel)

	def errormsg(self, target, message):
		self.msg(target, '@b@c4Error:@o %s' % message)

	def usagemsg(self, target, description, examples):
		message = '@errsep @bUsage@b %s @errsep' % description

		if examples != None:
			message += ' @bExamples@b %s @errsep' % ', '.join(examples)

		self.msg(target, message)

	def msg(self, target, message):
		if message != '':
			self.inter.privmsg(self.nick, target, format_ascii_irc(message))

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count

	def multinotice(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.notice(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count

	def notice(self, target, message):
		if message != '':
			self.inter.notice(self.nick, target, format_ascii_irc(message))

	def execute(self, manager, command, argument, channel, sender):
		full_command = '%s%s' % (command, ' %s' % argument if len(argument) else '')
		cmd = manager.get_command(command)

		if cmd == None:
			self.msg(channel, manager.invalid)
			self.elog.debug('Parsed command @b%s@b: invalid command.' % full_command)
			return

		if self.users.is_banned(sender) or self.antiflood.check_user(sender, command, argument):
			user = self.users[sender]
			message = 'You were banned by @b%s@b.' % user.ban_source

			if user.ban_reason != None:
				message += ' Reason: @b%s@b.' % user.ban_reason

			if user.ban_expiry != None:
				message += ' Expires: @b%s@b.' % datetime.fromtimestamp(user.ban_expiry)

			self.notice(sender, message)
			self.elog.debug('Parsed command @b%s@b: user is banned.' % full_command)
			return

		self.elog.command('%s%s > %s' % (sender, ':%s' % channel if channel != sender else '', full_command))

		parser = esimparser.ESimParser(add_help_option = False, option_class = esimparser.ESimParserOption)
		cmd_type = cmd[1]
		cmd_args = cmd[3]

		parser.add_option('-?', '--help', action = 'store_true')
		for cmd_arg in cmd_args:
			parser.add_option(cmd_arg[1], '--' + cmd_arg[0], **cmd_arg[3])

		try:
			(popts, pargs) = parser.parse_args(args = argument.split(' '))
		except esimparser.ESimParserError, err:
			self.msg(channel, str(err)) #TODO: Avoid str, use unicode.
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: invalid options.' % full_command)
			return

		if popts.help == True:
			manager.commands['help'][0](self, manager, {}, command, channel, sender)
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: help intercepted.' % full_command)
			return

		opt_dict = {}
		larg = ' '.join(pargs).strip()
		is_offline = True

		for cmd_arg in cmd_args:
			parg = getattr(popts, cmd_arg[0])

			if parg != None:
				if len(cmd_arg) <= 4 or not (cmd_arg[4] & cmd_manager.ARG_OFFLINE):
					is_offline = False

				if len(cmd_arg) > 4 and (cmd_arg[4] & cmd_manager.ARG_YES) and larg == '':
					self.msg(channel, 'Error: %s option requires an argument.' % cmd_arg[1])
					parser.destroy()
					self.elog.debug('Parsed command @b%s@b: option constraint was broken.' % full_command)
					return

				opt_dict[cmd_arg[0]] = parg
			elif len(cmd_arg) > 4 and (cmd_arg[4] & cmd_manager.ARG_OFFLINE and cmd_arg[4] & cmd_manager.ARG_OFFLINE_REQ):
				is_offline = False

		if not self.online and ((len(pargs) > 0 and not (cmd_type & cmd_manager.ARG_OFFLINE)) or not is_offline):
			self.notice(sender, 'The e-Sim API is offline. Please retry later.' if not self.offline_msg else self.offline_msg)
			parser.destroy()
			self.elog.debug('Parsed command @b%s@b: offline.' % full_command)
			return

		if (cmd_type & cmd_manager.ARG_YES) and (larg == None or larg == ''):
			self.msg(channel, '@bUsage@b: %s @b%s@b' % (command, cmd[4] if len(cmd) > 4 else 'argument'))
		else:
			try:
				cmd[0](self, manager, opt_dict, larg, channel, sender)
			except Exception, e:
				tb = traceback.extract_tb(sys.exc_info()[2])
				longest = 0

				for entry in tb:
					length = len(entry[2])

					if length > longest:
						longest = length

				self.elog.exception('%s%s > @b%s@b: %s' % (sender, ':%s' % channel if channel != sender else '', full_command, e))
				self.log.exception("e-Sim error!")

				for entry in tb:
					self.elog.traceback('@b%-*s@b : %d %s' % (longest, entry[2], entry[1], entry[3]))

				self.msg(channel, 'An exception occurred and has been reported to the developers. If this error persists please do not use the faulty command until it has been fixed.')

		parser.destroy()
		self.elog.debug('Parsed command @b%s@b: execution terminated.' % full_command)

	def onChanModes(self, prefix, channel, modes):
		if not self.initialized:
			return

		if not modes == '-z':
			return

		if channel in self.channels:
			self.channels.remove(channel)
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)

	def onNotice(self, source, target, message):
		if not self.initialized:
			return

		me = self.inter.findUser(self.nick)
		user = self.inter.findUser(target)
		userinfo = self.inter.findUser(source)

		if me != user or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		try:
			msg = message.strip()
		except:
			return

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo == None:
			if 'tried to kick you from' in msg:
				nick = strip_ascii_irc(sp[1])
				channel = strip_ascii_irc(sp[7])
				self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (self.nick, channel))

			return

		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][0:len(sp[5]) - 1]

		if 'Founder' in sp[2]:
			self.auth.accept(nick)
		else:
			self.auth.reject_not_founder(nick, channel)

	def onPrivmsg(self, source, target, message):
		if not self.initialized:
			return

		userinfo = self.inter.findUser(source)
		myself = self.inter.findUser(self.nick)

		sender = userinfo['nick']
		channel = target
		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		if channel == myself['nick'] and command.startswith(self.commands_private.prefix):
			self.elog.debug('Deferred parsing of private message (sender: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, command, argument))
			targs = (self.commands_private, command, argument, sender, sender)
		elif self.channels.is_valid(channel) and command.startswith(self.commands_user.prefix):
			self.elog.debug('Deferred parsing of channel message (sender: @b%s@b, channel: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, channel, command, argument))
			targs = (self.commands_user, command, argument, channel, sender)
		else:
			return

		t = threading.Thread(target=self.execute, args=targs)
		t.daemon = True
		t.start()

	def getCommands(self):
		return self.commands_admin

	def get_citizen(self, opts, arg, channel, sender, allow_org=True):
		id = None
		nick = None

		if 'nick' in opts:
			nick = arg
		elif arg == '':
			nick = sender
		elif 'id' in opts:
			try:
				id = int(arg)
			except ValueError, e:
				self.errormsg(channel, '%s is not a valid id.' % arg)
				return None

		if nick != None:
			id = self.users.get(nick, 'citizen')

		if id == None and nick != None:
			if 'nick' in opts:
				self.msg(channel, 'No citizen found linked to nick %s.' % arg)
			else:
				self.msg(channel, 'No citizen found linked to your nick. To link one type: @b.register_citizen <citizen name>@b')

			return None

		try:
			# Less effort, will change soon. Too many QQers. (YES CX, I WILL!)
			secura = True if 'secura' in opts else False
			c = citizen.from_name(arg, secura) if id == None else citizen.from_id(id, secura)
		except feed.FeedError, e:
			self.errormsg(channel, e.msg)
			return None

		if not allow_org and c.is_organization:
			self.errormsg(channel, '@b%s@b is an organization, not a citizen.' % c.name)
			return None

		return c

	def get_region(self, opts, arg, channel, sender):
		try:
			#secura = True if 'secura' in opts else False
			r = region.from_id(arg) if 'id' in opts else region.from_name(arg)
		except ValueError:
			self.errormsg(channel, '%s is not a valid id.' % arg)
			return None
		except feed.FeedError, e:
			self.errormsg(channel, e.msg)
			return

		if r == None:
			self.errormsg(channel, 'region @b%s@b not found.' % arg)

		return r
