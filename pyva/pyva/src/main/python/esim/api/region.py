import map
import utils
from decimal import Decimal


def from_id(id):
  id = int(id)
  r = utils.get_region_by_id(id)
  r_reg = map.get_region_from_regions(r)
  r_map = map.get_region_from_map(r)
  return Region(r_reg, r_map)

def from_name(name):
  r = utils.get_region_by_name(name)
  r_reg = map.get_region_from_regions(r)
  r_map = map.get_region_from_map(r)
  return Region(r_reg, r_map)

class Region:
  def __init__(self, r, m):
    self.name = r['name']
    self.id = r['id']
    self.country = map.get_country_by_id(r['homeCountry'])
    self.is_capital = r['capital']
    self.population = m['population']

    if m['occupantId'] == self.country['id']:
      self.occupied = False
    else:
      self.occupied = map.get_country_by_id(m['occupantId'])

    if r['rawRichness'] != 'NONE':
      self.richness = r['rawRichness'].lower()
      self.resource = r['resource'].lower()
    else:
      self.richness = None
      self.resource = None

    self.active_battle = m['battle']
    self.def_buildings = m['defensiveBuildings']
    self.companies = m['companies']
    
    self.borders = [utils.get_region_by_id(x) for x in r['neighbours']]
