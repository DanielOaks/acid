from pseudoclient.collection import *
from pseudoclient.sys_users import *

class EsimUser(CollectionEntity):
	citizen = None
	company = None

	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

class EsimUserManager(UserManager):
	def __init__(self, module):
		UserManager.__init__(self, module, EsimUser)
