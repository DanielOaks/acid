from pseudoclient.sys_auth import AuthManager

class EsimAuthManager(AuthManager):
  def __init__(self, module):
    AuthManager.__init__(self, module)

  def onAccept(self, user, request, action, channel):
    if action == 'highlight':
      if self.module.channels.is_valid(channel):
        self.module.channels.set(channel, 'mass', True)
        self.module.msg(user, 'Enabled mass highlighting in @b%s@b.' % channel)
    elif action == 'nohighlight':
      if self.module.channels.is_valid(channel):
        self.module.channels.set(channel, 'mass', False)
        self.module.msg(user, 'Disabled mass highlighting in @b%s@b.' % channel)
    else:
      return False

    return True
