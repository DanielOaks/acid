from pseudoclient.collection import *
from pseudoclient.sys_channels import *

class EsimChannel(CollectionEntity):
	mass = False

	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name

		# initialise those so it doesn't fuck itself later
		self.news = None
		self.highlight = None

class EsimChannelManager(ChannelManager):
	def __init__(self, module):
		ChannelManager.__init__(self, module, EsimChannel)
