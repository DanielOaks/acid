import ConfigParser
import codecs
from istring import istring
import MySQLdb as db
import logging
import logging.handlers

config = ConfigParser.ConfigParser()
config.readfp(codecs.open("config.ini", "r"))

anope_major = int(config.get('services', 'anope_major'))
if anope_major not in [1, 2]:
	raise Exception('Unknown anope major version %s' % anope_major) 

dbx = db.connect(
	host=config.get('database', 'host'),
	user=config.get('database', 'user'),
	passwd=config.get('database', 'passwd'),
	db=config.get('database', 'db'),
	unix_socket=config.get('database','sock')
)
dbx.ping(True)
dbx.autocommit(True) #no need to have transactions

logfile = config.get('logging', 'logfile')
loglevel = getattr(logging, config.get('logging', 'level').upper())
	
FORMAT = '%(asctime)s %(name)s(%(lineno)s) [%(levelname)s] %(message)s'
handler = logging.handlers.TimedRotatingFileHandler(
	logfile, when='midnight', backupCount=7)
handler.setFormatter(logging.Formatter(FORMAT))
	
log = logging.getLogger('')
log.addHandler(handler)
log.setLevel(loglevel)

import pyva_java_lang_System as javasys
import sys

class StdOutWriter(object):
	def write(self, what):
		# print is Python keyword, so call println with what.strip() instead of print
		javasys.out.println(what.rstrip())

class StdErrWriter(object):
	def write(self, what):
		javasys.err.println(what.rstrip())

sys.stdout = StdOutWriter()
sys.stderr = StdErrWriter()
