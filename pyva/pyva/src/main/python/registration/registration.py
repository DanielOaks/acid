#!/usr/bin/python pseudoserver.py
# psm_registration.py
# module for pypseudoserver
# written by B (ben@rizon.net)
# helpful registration bot!

from pyva import *
import logging
from core import *
from plugin import *

class registration(AcidPlugin):
	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "registration"
		self.log = logging.getLogger(__name__)
		
	def start(self):
		self.client = self.config.get('registration', 'nick')
		return True
		
	def onChanModes(self, prefix, chan, modes):
		if modes != "+z":
			return #ignore all other mode changes, we only want +z for starts

		me = self.inter.findUser(self.client)
		if not me:
			return

		user = self.inter.findUser(prefix)
		if not user or user.getNick() != 'ChanServ':
			return

		self.log.debug("Caught a channel registration")
		
		me.joinChan(chan)
		
		
		#s XXX lol?
		self.inter.privmsg(self.client, chan, self.config.get('registration', 'reg1'))
		self.inter.privmsg(self.client, chan, self.config.get('registration', 'reg2'))
		self.inter.privmsg(self.client, chan, self.config.get('registration', 'reg3'))
		self.inter.privmsg(self.client, chan, self.config.get('registration', 'reg4'))
		self.inter.privmsg(self.client, chan, self.config.get('registration', 'reg5'))
		self.inter.privmsg(self.client, chan, self.config.get('registration', 'reg6'))

		#logging fun
		self.inter.privmsg(self.client, self.logchan, "registration info sent to %s" % chan)
		
		me.partChan(chan)
