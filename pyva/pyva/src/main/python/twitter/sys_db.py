# sys_db.py
# Twitter Database Subsystem

from pseudoclient.umodule import UModuleSubsystem


class TwitterDatabase(UModuleSubsystem):
	"""Handles databasing for the Twitter module."""
	name = 'db'
