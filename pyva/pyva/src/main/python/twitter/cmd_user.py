#!/usr/bin/python pseudoserver.py
# psm_twitter.py's commands

from pseudoclient.cmd_manager import ARG_NO, ARG_YES, ARG_OPT, ARG_OFFLINE
from pseudoclient.umodule import DynamicCommandManager


def private_link(self, manager, opts, arg, channel, sender, userinfo):
	"""Lets client link their Twitter handle to their nick."""
	message = 'This command does not yet function. Sorry!'
	self.notice(sender, message)


def private_tweet(self, manager, opts, arg, channel, sender, userinfo):
	"""Lets client tweet from IRC!"""
	message = 'This command does not yet function. Sorry!'
	self.notice(sender, message)


class TwitterCommandManager(DynamicCommandManager):
	def __init__(self):
		DynamicCommandManager.__init__(self)

		self.add_command('link', private_link, ARG_YES, 'links the given Twitter handle to your nick', [], '@handle')
		self.add_command('tweet', private_tweet, ARG_YES, 'send a tweet from IRC!', [], 'message you wish to tweet')

		self.compile_commands()
