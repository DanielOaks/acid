#!/usr/bin/python pseudoserver.py
# psm_twitter.py

from pseudoclient.umodule import UModule

from cmd_user import TwitterCommandManager
from sys_db import TwitterDatabase


class twitter(UModule):
	name = 'twitter'
	version = '0.1a'
	developers = 'Daniel Oaks'

	def __init__(self):
		"""Start our Twitter module."""
		UModule.__init__(self, custom_commands=True)

		self.autoload_subsystem('db', TwitterDatabase)

		self.commands_private = TwitterCommandManager()
