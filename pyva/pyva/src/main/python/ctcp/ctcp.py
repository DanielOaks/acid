#!/usr/bin/python pseudoserver.py
#psm-ctcp.py
# module for pypseudoserver
# written by radicand
# doesn't do much by itself

import task
import istring
from operator import itemgetter #for sort
from collections import defaultdict
import time

from pyva import *
import logging
from core import *
from plugin import *
from threading import Timer

class ctcp(AcidPlugin):
	CTCP_BANNED = 0
	CTCP_TOTAL = 0

	blacklist = []
	uid = ""

	def __init__(self):
		AcidPlugin.__init__(self)
		self.name = "ctcp"
		self.log = logging.getLogger(__name__)

	def start(self):
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_vbl (item SMALLINT(5) NOT NULL AUTO_INCREMENT, reply VARCHAR(255), found INT(10), PRIMARY KEY (item), UNIQUE KEY (reply));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_vlist (reply VARCHAR(255), found INT(10), UNIQUE KEY (reply));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_website (reply VARCHAR(255), found INT(10), UNIQUE KEY (reply));")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS ctcp_website_replies (keyword VARCHAR(255), reply VARCHAR(255), UNIQUE KEY (keyword));")
		except Exception, err:
			self.log.exception("Error creating tables for CTCP module (%s)" % err)
			raise
		
		try:
			self.dbp.execute("SELECT reply FROM ctcp_vbl;")
			for row in self.dbp.fetchall(): self.blacklist.append(row[0])
		except Exception, err:
			self.log.exception("Error loading blacklists from DB for CTCP module (%s)" % err)
			
		try:
			num = self.dbp.execute("SELECT num FROM record WHERE name='ctcp-total';")
			if num:
				self.CTCP_TOTAL = self.dbp.fetchone()[0]

			num = self.dbp.execute("SELECT num FROM record WHERE name='ctcp-found';")
			if num:
				self.CTCP_BANNED = self.dbp.fetchone()[0]

		except Exception, err:
			self.log.exception("Error loading in historical records for CTCP module (%s)" % err)
		
		self.client = istring(self.config.get('ctcp', 'nick'))

		return True
	
	def stop(self):
		try:
			self.dbp.execute("INSERT INTO record (name, num) VALUES ('ctcp-total', %s) ON DUPLICATE KEY UPDATE num=%s;", (self.CTCP_TOTAL,self.CTCP_TOTAL))
			self.dbp.execute("INSERT INTO record (name, num) VALUES ('ctcp-found', %s) ON DUPLICATE KEY UPDATE num=%s;", (self.CTCP_BANNED,self.CTCP_BANNED))
		except Exception, err:
			self.log.exception("Error updating historical records for CTCP module (%s)" % err)

	def mkts():
		return int(time.time())

## Begin event hooks
	def onUserConnect(self, user):
		"""Sends a CTCP VERSION request to a user
		We setup a list with the timestamp we sent the request
		 and if we havent received the reply in X seconds, akill them.
		Also akill if they have a blacklisted reply.
		If user is from mibbit, request a CTCP WEBSITE, so we can track it.
		Rizon: Temporarily ignore nick `bRO-*` / eth32 / RAC until they fix their 'client'.
		"""

		nick = user.getNick()
		if nick.startswith(u'bRO-'): return #rizon
		elif nick.startswith(u'RAC_'): return #rizon
		elif u'eth32' in nick: return #rizon

		#if mibbit req website
		if user.hasMode("W"):
			self.inter.privmsg(self.client, user.getUID(), "\001WEBSITE\001")

		self.inter.privmsg(self.client, user.getUID(), "\001VERSION\001")
		self.CTCP_TOTAL += 1
		
	def onCtcpReply(self, creator, recipient, message):
		"""Receiver for CTCP VERSION replies.
		Check against active blacklist.
		Remove from timeout list too.
		#checks ctcp website too
		TODO: clean this up
		"""

		user = self.inter.findUser(creator)
		if not user:
			return

		try:
			reply = istring(message[1:-1])
		except:
			return # Can this throw?

		#ctcp website reply
		if reply.lower().startswith(u"website"):
			reply = reply[8:]
			try:
				self.dbp.execute(u"INSERT INTO ctcp_website (reply, found) VALUES(%s, 1) ON DUPLICATE KEY UPDATE found=found+1;", (reply,))
			except Exception, err:
				self.log.exception(u"Error updating ctcp_website: %s (%s)" % (reply, err))
			user['website'] = reply
			return

		#ctcp version reply
		if not reply.lower().startswith(u"version"): return
		reply = unicode(reply[8:])
		self.log.debug(u"Got CTCP VERSION: %s" % reply)

		try:
			self.dbp.execute(u"INSERT INTO ctcp_vlist (reply, found) VALUES(%s, 1) ON DUPLICATE KEY UPDATE found=found+1;", (reply,))
		except Exception, err:
			self.log.exception(u"Error updating ctcp_vlist: %s (%s)" % (reply, err))

		user['version'] = reply
		self.handleWebsiteReply(user)

		if reply in self.blacklist:
			self.CTCP_BANNED += 1

			akillserv = self.inter.findUser("GeoServ") or self.inter.findUser("OperServ")
			if akillserv:
				self.inter.privmsg(self.client, akillserv.getUID(), "AKILL ADD +3d *@%s Compromised Host - Blacklisted VERSION reply." %
					(user.getIP(),))

				self.inter.privmsg(self.client, self.logchan, "CTCP: Banned %s!%s@%s - Blacklisted VERSION: %s (#%d/%d)" %
					(user.getNick(), user.getUser(), user.getIP(), reply, self.CTCP_BANNED, self.CTCP_TOTAL))

			try:
				self.dbp.execute(u"UPDATE ctcp_vbl SET found=found+1 WHERE reply=%s;", (reply,))
			except Exception, err:
				self.log.exception("Error updating ctcp_vbl found count: %s (%s)" % (reply, err))

			#if "dnsbl_admin" in self.parent.modules:
			#	uid2, user2 = self.parent.get_user(self.uid)
			#	self.parent.modules['dnsbl_admin'].add(user['ip'], "5", "Compromised host found by %s (%s!%s@%s)" %
			#		(user2['nick'],user['nick'], user['user'], user['ip']))
	
## Begin Command hooks
	def cmd_ctcpVersionAdd(self, source, target, pieces):
		if not pieces: return False
		try:
			self.dbp.execute("INSERT INTO ctcp_vbl (reply,found) VALUES(%s,0);", (u' '.join(pieces),))
			self.inter.privmsg(self.client, target, "Added VERSION blacklist #%d" % self.dbp.lastrowid)
		except Exception, err:
			self.log.exception("Error adding version to CTCP module blacklist: (%s)" % err)
			return False

		self.blacklist.append(u' '.join(pieces))
		return True

	def cmd_ctcpVersionDel(self, source, target, pieces):
		if not pieces: return False
		try:
			num = self.dbp.execute("SELECT reply FROM ctcp_vbl WHERE item=%s", (pieces[0],))
			if not num: raise Exception("No such ID")
			else: reply = self.dbp.fetchone()[0]

			self.dbp.execute("DELETE FROM ctcp_vbl WHERE item=%s;", (pieces[0],))
			self.blacklist.remove(reply)
			self.inter.privmsg(self.client, target, "Removed blacklist for: %s" % reply)
		except Exception, err:
			self.log.exception("Error deleting version blacklist for CTCP module: (%s)" % err)
			self.inter.privmsg(self.client, target, "No such ID")
			
		return True
		
	def cmd_ctcpVersionBLList(self, source, target, pieces):
		try:
			self.dbp.execute("SELECT item, reply FROM ctcp_vbl;")
			for row in self.dbp.fetchall():
				self.inter.privmsg(self.client, target, "#%d: %s" % (row[0], row[1]))
			self.log.debug("CVMEMORY: %s" % self.blacklist)
		except Exception, err:
			self.log.exception("Error selecting version blacklists for CTCP module: (%s)" % err)
		
		self.inter.privmsg(self.client, target, "END OF CVLIST")
		return True
	def cmd_ctcpWebsiteList(self, source, target, pieces):
		try:
			w = defaultdict(lambda: 0)
			cm = 0
			cq = 0
			for k,v in self.parent.user_t.iteritems():
				if 'website' in v:
					tmp = v['website']
					w[tmp] += 1
				if v['user'] == u'cgiirc': cm += 1
				if v['user'] == u'qwebirc': cq += 1
				if v['user'].endswith(u'webchat'): cq += 1
		except Exception, err:
			self.log.exception("Error comprehending website list creation: %s" % (err,))
			self.inter.privmsg(self.client, target, "Error creating list response")
			return True
		#sort by number of hits
		w = sorted(w.items(), key=itemgetter(1))	
		for k,v in w:
			self.inter.notice(self.client, source, "%dx: %s" % (v,k))
		self.inter.notice(self.client, source, "END OF CWLIST(m=%d,q=%d)" % (cm,cq))
		return True

	ctcpReplyCache = None
	
	def handleWebsiteReply(self, user):
		if self.ctcpReplyCache == None:
			self.ctcpReplyCache = []
			self.dbp.execute("SELECT * FROM ctcp_website_replies")
			for row in self.dbp.fetchall():
				self.ctcpReplyCache.append((row[0], row[1]))
		
		for reply in self.ctcpReplyCache:
			if 'version' in user and user['version'].lower().find(reply[0].lower()) != -1:
				self.inter.notice(self.client, user.getNick(), reply[1])
			elif 'website' in user and user['website'].lower().find(reply[0].lower()) != -1:
				self.inter.notice(self.client, user.getNick(), reply[1])
		return
		
	def cmd_ctcpReplyAdd(self, source, target, pieces):
		if len(pieces) < 2:
			return False
		try:
			self.dbp.execute("INSERT INTO ctcp_website_replies (keyword, reply) VALUES(%s, %s)", (pieces[0], ' '.join(pieces[1:])))
			self.inter.privmsg(self.client, target, "Added reply for keyword %s" % pieces[0])
			self.ctcpReplyCache = None
		except Exception, err:
			self.log.exception("Error adding website reply for %s: %s" % (pieces[0], err))
			self.inter.privmsg(self.client, target, "Error creating reply keyword")
		return True

	def cmd_ctcpReplyDel(self, source, target, pieces):
		if not pieces:
			return False
		try:
			self.dbp.execute("DELETE FROM ctcp_website_replies WHERE keyword=%s", pieces[0])
			self.inter.privmsg(self.client, target, "Removed reply for keyword %s" % pieces[0])
			self.ctcpReplyCache = None
		except Exception, err:
			self.log.exception("Error removing website reply for %s: %s" % (pieces[0], err))
			self.inter.privmsg(self.client, target, "Error removing reply keyword")
		return True

	def cmd_ctcpReplyList(self, source, target, pieces):
		try:
			self.dbp.execute("SELECT * FROM ctcp_website_replies")
			for row in self.dbp.fetchall():
				self.inter.privmsg(self.client, target, "%s: %s" % (row[0], row[1]))
		except Exception, err:
			self.log.exception("Error listing website replies: %s" % err)
			self.inter.privmsg(self.client, target, "Error listing website replies.")
		return True

	def cmd_ctcpVersionList(self, source, target, pieces):
		try:
			subcmd = pieces[0].lower()
		except IndexError:
			subcmd = "top"

		try:
			arg = pieces[1]
		except IndexError:
			arg = ""

		maxrow = 25
		count = 0
		if subcmd == "top":
			if arg != "":
				try:
					maxrow = int(arg)
				except:
					pass

			# pymysql apparently converts ints to strings, the %s here is intentional!
			self.dbp.execute("SELECT reply, found FROM ctcp_vlist ORDER BY found DESC LIMIT %s", (maxrow,))

			try:
				for row in self.dbp.fetchall():
					self.inter.privmsg(self.client, target, u"%s [%s times]" % (row[0], row[1]))
					count += 1
			except Exception, err:
				self.log.exception("Error listing version replies [top]: %s" % err)
				self.inter.privmsg(self.client, target, "Error listing version replies.")

		elif subcmd == "search":
			if arg == "":
				self.inter.privmsg(self.client, target, 'Please give a string to search for.')
				return False

			try:
				maxrow = int(pieces[2])
			except:
				pass

			self.dbp.execute(u"SELECT reply, found FROM ctcp_vlist WHERE reply LIKE '%%%s%%' LIMIT %s" % (arg, maxrow))
			try:
				for row in self.dbp.fetchall():
					self.inter.privmsg(self.client, target, u"%s [%s times]" % (row[0], row[1]))
					count += 1
			except Exception, err:
				self.log.exception("Error listing version replies [search]: %s" % err)
				self.inter.privmsg(self.client, target, "Error listing version replies.")
		else:
			self.inter.privmsg(self.client, target, 'Please use one of "top" or "search."')
			return False

		self.inter.privmsg(self.client, target, "END OF VLIST (%d results)" % count)
		return True

## End Command hooks

	def getCommands(self):
		return (('addbl', {
			 'permission':'v',
			'callback':self.cmd_ctcpVersionAdd,
			'usage':"<version string> - blacklists a VERSION string"}),
			('delbl', {
			 'permission':'v',
			'callback':self.cmd_ctcpVersionDel,
			'usage':"<version string> - removes a blacklisted VERSION string"}),
			('listbl', {
			 'permission':'v',
			'callback':self.cmd_ctcpVersionBLList,
			'usage':"- shows all active blacklisted VERSION strings"}),
			('cwlist', {
			 'permission':'v',
			'callback':self.cmd_ctcpWebsiteList,
			'usage':"- shows source of all online mibbit users"}),
			('wadd', {
			 'permission' : 'v',
			 'callback' : self.cmd_ctcpReplyAdd,
			 'usage' : "<match> <reply> - sends a custom notice to specific clients"}),
			('wdel', {
			 'permission' : 'v',
			 'callback' : self.cmd_ctcpReplyDel,
			 'usage' : "<match> - removes the welcome notice for match"}),
			('wlist', {
			 'permission' : 'v',
			 'callback' : self.cmd_ctcpReplyList,
			 'usage' : "- shows the welcome notices for version matches"}),
			('vlist', {
			 'permission': 'v',
			 'callback': self.cmd_ctcpVersionList,
			 'usage': "{top|search} [num|searchstring] - shows the top client version replies or searches them. Defaults to showing top"}),
		)
