from pseudoclient.sys_auth import AuthManager

class TriviaAuthManager(AuthManager):
	def __init__(self, module):
		AuthManager.__init__(self, module)

	def onAccept(self, user, request, action, channel):
		if action == 'news':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', True)
				self.module.msg(user, 'Enabled news in @b%s@b.' % channel)
		elif action == 'nonews':
			if self.module.channels.is_valid(channel):
				self.module.channels.set(channel, 'news', False)
				self.module.msg(user, 'Disabled news in @b%s@b.' % channel)
		elif action.startswith('set_theme_'):
			reqtheme = action[10:].lower()
			settheme = ""
			for theme in self.module.themes:
				if reqtheme == theme[0].lower() or reqtheme == theme[1].lower():
					settheme = theme[0] # we only care for the table name
					break
			else:
				# XXX: Breaking compatibility; original trivia first checks
				# if the calling user is founder; we save the effort.
				self.module.notice(user, 'Theme %s not found.' % reqtheme)
				del self.requests[user]
				return True

			# XXX: Breaking compatibility; orig trivia uses malformed msg
			self.module.dbp.execute("UPDATE trivia_chans SET theme = %s WHERE id = %s",
					(settheme, self.module.get_cid(channel)))
			self.module.notice(user,
				"Updated the trivia theme of %s to %s." % (channel, reqtheme))
		else:
			return False

		return True
