# psm_listbots.py: List pypsd bots but non-dynamically
# vim: set expandtab:

import logging

from pyva import *
from core import *
from plugin import *
from istring import istring

class listbots(AcidPlugin):

	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "listbots"
		self.log = logging.getLogger(__name__)
	
	def start(self):
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS pypsd_bots (nick VARCHAR(30) NOT NULL PRIMARY KEY, description VARCHAR(200) NOT NULL) ENGINE=MyISAM;")
		except Exception, err:
			self.log.exception("Error creating table for listbots module: %s" % err)
			return False

                self.client = self.config.get('listbots', 'nick')

	###
	# Admin commands
	###
	def cmd_add(self, source, target, pieces):
		if not pieces or len(pieces) < 2:
			return False

		# We won't sanity check for the nick's existence here, after all,
		# they could be on another py instance or not be loaded right now
		# for some reason.

		desc = ""
		for i, piece in enumerate(pieces):
			if i == 0:
				continue

			if i != 1:
				desc += ' '
			desc += piece

		try:
			self.dbp.execute('INSERT INTO pypsd_bots(nick, description) VALUES(%s, %s) ON DUPLICATE KEY UPDATE description = %s',
					(pieces[0], desc, desc))
		except Exception, err:
			self.log.exception('Error updating listbots: (%s)' % err)
			return True

                self.inter.privmsg(self.client, target, 'Added %s (%s).' % (pieces[0], desc))

		return True

	def cmd_del(self, source, target, pieces):
		if not pieces:
			return False

		try:
			self.dbp.execute('SELECT nick FROM pypsd_bots WHERE nick = %s', pieces[0])
			if not self.dbp.fetchone():
                                self.inter.privmsg(self.client, target, 'Bot %s does not exist.' % (pieces[0]))
				return True

			self.dbp.execute('DELETE FROM pypsd_bots WHERE nick = %s', pieces[0])
		except Exception, err:
			self.log.exception('Error updating listbots: (%s)' % err)
			return True

                self.inter.privmsg(self.client, target, 'Deleted %s.' % pieces[0])

		return True

	def cmd_list(self, source, target, pieces):
		count = 0

		try:
			self.dbp.execute('SELECT nick, description FROM pypsd_bots')
		except Exception, err:
			self.log.exception('Error fetching listbots: (%s)' % err)
			return True

                self.inter.privmsg(self.client, target, 'List of listed bots:')

		for row in self.dbp.fetchall():
			# Sanely, we can assume that hardly any nick will be longer than
			# 9 chars, even if nicklen is 30. Or the list will look like crap,
			# either of those work.
                        self.inter.privmsg(self.client, target, '%-9s - %s' % (row[0], row[1]))
			count += 1

                self.inter.privmsg(self.client, target, 'End of list of listed bots (%d bots).' % count)

		return True

	def onPrivmsg(self, source, target, message):
                if target != self.client: # XXX uid
                        return

                u = self.inter.findUser(source)
		if not u:
			return

                msg = istring(message.strip())

		if msg not in ('HELP', 'LIST', 'COMMANDS', 'SHOWCOMMANDS', 'INFO'):
                        self.inter.notice(self.client, u.getUID(), 
					'Unknown command, do /msg %s HELP' % self.client)
			return

		try:
			self.dbp.execute('SELECT nick, description FROM pypsd_bots')
		except Exception, err:
			self.log.exception('Error fetching listbots: (%s)' % err)
			return

                self.inter.notice(self.client,  u.getUID(), 'All commands listed here are only available to channel founders.')
                self.inter.notice(self.client,  u.getUID(), 'Once the bot joined, use .help for more information on any one bot.')
                self.inter.notice(self.client, u.getUID(), ' ')

		rows = self.dbp.fetchall()

		for i, row in enumerate(rows):
                        self.inter.notice(self.client, u.getUID(), '\002%s\002: %s' % (row[0], row[1]))
                        self.inter.notice(self.client, u.getUID(), 'To request: \002/msg %s request \037#channel\037\002' % (row[0]))
                        self.inter.notice(self.client, u.getUID(), 'To remove: \002/msg %s remove \037#channel\037\002' % (row[0]))
			if i < len(rows)-1:
                                self.inter.notice(self.client, u.getUID(), ' ')

	def getCommands(self):
		return (
			('add', {
				'permission': 'B',
				'callback': self.cmd_add,
				'usage': '<nick> <description> - adds a new bot to the list (or change its description) with the given description'
				}),
			('del', {
				'permission': 'B',
				'callback': self.cmd_del,
				'usage': '<nick> - removes a bot listed'
				}),
			# For convenience, also alias list here
			('list', {
				'permission': 'B',
				'callback': self.cmd_list,
				'usage': '- displays all bots listed and their description'
				})
			)

