#
# inviteable.py
# Copyright (c) 2014 Kristina Brooks
#
# 
#

from pseudoclient.cmd_manager import *
import mythreading as threading
from datetime import datetime
from utils import strip_ascii_irc
from core import anope_major

#---------------------------------------------------------------------#
# this is terrible 

def private_channel_request(self, manager, opts, arg, channel, sender, userinfo):
	if self.channels.is_valid(arg):
		self.notice(channel, "I'm already in @b%s@b." % arg)
	elif self.channels.is_banned(arg):
		chan = self.channels[arg]
		self.elog.request('Request for banned channel @b%s@b from @b%s@b.' % (arg, sender))
		message = 'Request failed, channel @b%s@b was banned by @b%s@b.' % (arg, chan.ban_source)

		if chan.ban_reason != None:
			message += ' Reason: @b%s@b.' % chan.ban_reason

		if chan.ban_expiry != None:
			message += ' Expires: @b%s@b.' % datetime.fromtimestamp(chan.ban_expiry)

		self.notice(channel, message)
#		self.notice(channel, 'Please email @c3????@rizon.net@o to appeal.')
	else:
		self.auth.request(sender, arg, 'request')

def private_channel_remove(self, manager, opts, arg, channel, sender, userinfo):
	if not self.channels.is_valid(arg):
		self.notice(channel, "I'm not in @b%s@b." % arg)
	else:
		self.auth.request(sender, arg, 'remove')

def private_help(self, manager, opts, arg, channel, sender, userinfo):
	command = arg.lower()

	if command == '':
		message = manager.get_help()
	else:
		message = manager.get_help(command)

		if message == None:
			message = ['%s is not a valid command.' % arg]

	for line in message:
		self.notice(channel, line)

class PrivateCommandManager(CommandManager):
	def get_prefix(self):
		return ''

	def get_invalid(self):
		return 'Invalid message. Say help for a list of valid messages.'

	def get_commands(self):
		return {
			'request': (private_channel_request, ARG_YES|ARG_OFFLINE, 'requests a channel (must be founder)', [], '#channel'),
			'remove': (private_channel_remove, ARG_YES|ARG_OFFLINE, 'removes a channel (must be founder)', [], '#channel'),
			'hi': 'help',
			'hello': 'help',
			'help': (private_help, ARG_OPT, 'displays help text', []),
		}

#---------------------------------------------------------------------#

class InviteablePseudoclient(object):
	def __init__(self):
		pass

	def start(self):
		self.commands_private = PrivateCommandManager()

	# override this if you want to add additional crap 
	# (ie check if the chan is eligible or not)
	def do_accept(self, nick, channel):
		self.auth.accept(nick)

	def onNotice(self, source, target, message):
		if not self.initialized:
			return

		me = self.inter.findUser(self.nick)
		user = self.inter.findUser(target)
		userinfo = self.inter.findUser(source)

		if me != user or (userinfo != None and userinfo['nick'] != 'ChanServ'):
			return

		try:
			msg = message.strip()
		except:
			return

		if msg.startswith('[#'): #It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo == None:
			if 'tried to kick you from' in msg:
				nick = strip_ascii_irc(sp[1])
				channel = strip_ascii_irc(sp[7])
				self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (self.nick, channel))

			return

		if anope_major == 2 and msg == 'Password authentication required for that command.':
			self.elog.error('%s is not authed with services (maybe a wrong NickServ password was configured?).' % target)

		# common to both anope1 and anope2
		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]: #It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][:-1]

		if anope_major == 1:
			should_accept = 'Founder' in sp[2]
		elif anope_major == 2:
			channel = strip_ascii_irc(channel)
			should_accept = ('founder' == sp[3])

		if should_accept:
			self.do_accept(nick, channel)
		else:
			self.auth.reject_not_founder(nick, channel)

	def onPrivmsg(self, source, target, message):
		if not self.initialized:
			return False

		userinfo = self.inter.findUser(source)
		myself = self.inter.findUser(self.nick)

		sender = userinfo['nick']
		channel = target
		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		# do we have a deferal command handler?
		if hasattr(self, 'execute'):
			if channel == myself['nick'] and command.startswith(self.commands_private.prefix): # XXX uid
				self.elog.debug('Deferred parsing of private message (sender: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, command, argument))
				threading.deferToThread(self.execute, self.commands_private, command, argument, sender, sender, userinfo)
			elif self.channels.is_valid(channel) and command.startswith(self.commands_user.prefix):
				self.elog.debug('Deferred parsing of channel message (sender: @b%s@b, channel: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, channel, command, argument))
				threading.deferToThread(self.execute, self.commands_user, command, argument, channel, sender, userinfo)
		elif target == self.nick:
			cmd = self.commands_private.get_command(command)
			if cmd == None:
				# not found, forward through
				return True
			#     (self, manager,               opts,    arg,     channel, sender, userinfo):
			cmd[0](self, self.commands_private, command, argument, sender, sender, userinfo)
		else:
			# forward anyway
			return True