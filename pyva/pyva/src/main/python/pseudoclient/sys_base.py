import threading
import MySQLdb as db

class BaseSubsystem(object):
	"""Base subsystem, provides little more than stubs.
	Commonly used for user-created module-specific subsystems.
	"""
	sync_db_on_shutdown = False  # whether to sync our db when we shutdown

	def __init__(self, module):
		self.module = module

	# startup and shutdown
	def start(self):
		pass

	def reload(self):
		pass

	def stop(self):
		pass

class Subsystem(object):
	#--------------------------------------------------------------#
	# i'm not entirely sure if this functionality is still
	# required, but it seems it was removed from this module
	# but not from cmd_admin, so I can only assume the removal
	# was not intentional.

	db_up = True

	@classmethod
	def set_db_up(cls, up):
		cls.db_up = up

	@classmethod
	def get_db_up(cls):
		return cls.db_up

	#--------------------------------------------------------------#

	sync_db_on_shutdown = True  # sync db on shutdown

	def __init__(self, module, options, name):
		self.module = module
		self.__options = options
		self.__lock = threading.Lock()
		self.name = name
		self.__timer = None
		self.conn = None
		self.cursor = None
		self.reload()

	def db_open(self):
		self.conn = db.connect(
			host=self.module.config.get('database', 'host'),
			user=self.module.config.get('database', 'user'),
			passwd=self.module.config.get('database', 'passwd'),
			db=self.module.config.get('database', 'db'),
			unix_socket=self.module.config.get('database','sock')
		)
		self.conn.ping(True)
		self.conn.autocommit(True)
		self.cursor = self.conn.cursor()

	def db_close(self):
		if self.cursor != None:
			self.cursor.close()
			self.cursor = None
		
		if self.conn != None:
			self.conn.close()
			self.conn = None

	def reload(self):
		self.__delay = self.get_option('update_period', int, 120)

		if self.__timer != None:
			self.stop()
			self.on_reload()
			self.start()
		else:
			self.on_reload()

	def on_reload(self):
		pass

	def get_option(self, name, type, default):
		return self.__options.get('%s_%s' % (self.name, name), type, default)

	def set_option(self, name, value):
		return self.__options.set('%s_%s' % (self.name, name), value)

	def start(self):
		self.__timer = threading.Timer(self.__delay, self.worker)
		self.__timer.daemon = True
		self.__timer.start()

	def stop(self):
		self.__lock.acquire()

		try:
			if self.__timer != None:
				self.__timer.cancel()
				self.__timer = None
		finally:
			self.__lock.release()

	def update(self):
		self.__lock.acquire()

		try:
			self.commit()
		finally:
			self.__lock.release()

	def force(self):
		self.update()

	def commit(self):
		pass

	def worker(self):
		try:
			if self.conn != None:
				self.conn.ping(True)
		finally:
			pass

		if self.__class__.db_up:
			self.update()

		self.__timer = threading.Timer(self.__delay, self.worker)
		self.__timer.daemon = True
		self.__timer.start()

