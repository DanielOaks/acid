from datetime import datetime
from utils import *
from istring import *
import traceback
import inspect

class InvariantCollection(object):
	def __init__(self):
		self.__entries = {}

	def __len__(self):
		return len(self.__entries)

	def __getitem__(self, key):
		invariant_key = istring(key)

		if not self.__contains__(invariant_key):
			raise KeyError('Entry with key %s not present.' % key)

		return self.__entries[invariant_key]

	def __setitem__(self, key, value):
		self.__entries[istring(key)] = value

	def __delitem__(self, key):
		invariant_key = istring(key)

		if not self.__contains__(invariant_key):
			raise KeyError('Entry with key %s not present.' % key)

		del(self.__entries[invariant_key])

	def __iter__(self):
		for entry in self.__entries:
			yield entry

	def __contains__(self, item):
		if not isinstance(item, basestring):
			raise TypeError('%s is not an allowed indexing type.' % type(item))

		return istring(item) in self.__entries
	
	def list_all(self):
		return [self[item] for item in self]

class CollectionEntity(object):
	def __init__(self, id, ban_source = None, ban_reason = None, ban_date = None, ban_expiry = None):
		self.id = id
		self.ban_source = ban_source
		self.ban_reason = ban_reason
		self.ban_date = ban_date
		self.ban_expiry = ban_expiry
		self.dirty = False
		self.registered = self.banned = False

class CollectionManager(InvariantCollection):
	def __init__(self, type):
		InvariantCollection.__init__(self)
		self.__type = type
		self.__deleted_items = []

		self.db_open()

		self.load()

	def load(self):
		self.cursor.execute("CREATE TABLE IF NOT EXISTS " + self.module.name + "_" + self.name + " (id INT NOT NULL AUTO_INCREMENT, name VARCHAR(51) NOT NULL, PRIMARY KEY (id), UNIQUE KEY (name)) ENGINE=InnoDB")

		self.cursor.execute("CREATE TABLE IF NOT EXISTS " + self.module.name + "_" + self.name + "_options (id INT, name VARCHAR(32) NOT NULL, value TINYTEXT, UNIQUE KEY (id, name)) ENGINE=InnoDB")
		#self.cursor.execute("ALTER TABLE " + self.module.name + "_" + self.name + "_options ADD CONSTRAINT FOREIGN KEY IF NOT EXISTS (id) REFERENCES " + self.module.name + "_" + self.name + " (id)")

		self.cursor.execute("SELECT id, name FROM " + self.module.name + "_" + self.name)
		r = self.cursor.fetchall()

		for row in r:
			id = row[0]
			name = istring(row[1])

			self.module.elog.debug('Loading ' + name + ' for ' + self.module.name + '_' + self.name)

			self[name] = e = self.__type(id, name)

			self.cursor.execute("SELECT name,value FROM " + self.module.name + "_" + self.name + "_options WHERE `id` = %s", (id,))
			r2 = self.cursor.fetchall()
			for row2 in r2:
				setattr(e, row2[0], row2[1])

	def __get_attributes(self, obj):
		boring = dir(type('dummy', (object,), {}))
		return [item for item in inspect.getmembers(obj) if item[0] not in boring]

	def commit(self):
		try:
			deleted = [(e.id, ) for e in self.list_deleted()]
			changed = [e for e in self.list_dirty()]

			if len(deleted) > 0:
				self.cursor.executemany("DELETE FROM " + self.module.name + "_" + self.name + "_options WHERE id = %s", deleted)
				self.cursor.executemany("DELETE FROM " + self.module.name + "_" + self.name + " WHERE id = %s", deleted)
				self.module.elog.commit('Deleted %d %s from database.' % (len(deleted), self.name))

			for e in changed:
				if e.id == -1:
					self.cursor.execute("INSERT INTO " + self.module.name + "_" + self.name + " (name) VALUES(%s)", (e.name,))
					e.id = self.cursor.lastrowid
				else: # delete options as we are aboue to re-flush them
					self.cursor.execute("DELETE FROM " + self.module.name + "_" + self.name + "_options WHERE `id` = %s", (e.id,))

				for a in self.__get_attributes(e):
					attr_name = a[0]
					attr = getattr(e, attr_name)
					if attr:
						self.cursor.execute("INSERT INTO " + self.module.name + "_" + self.name + "_options (id, name, value) VALUES (%s, %s, %s)", (e.id, attr_name, attr))
			if len(changed) > 0:
				self.module.elog.commit('Committed %d %s to database.' % (len(changed), self.name))

			self.clear_deleted()

			for e in self.list_all():
				e.dirty = False
		except Exception, err:
			traceback.print_exc()
			self.module.elog.error(self.name + ' commit failed: @b%s@b' % err)

	def check(self, item):
		if not item in self:
			return

		entity = self[item]

		if entity.ban_expiry != None and entity.ban_expiry <= unix_time(datetime.now()):
			self.unban(item)

	def is_dirty(self, item):
		self.check(item)

		return item in self and self[item].dirty

	def is_valid(self, item):
		self.check(item)

		return item in self and self[item].registered and not self[item].banned

	def is_banned(self, item):
		self.check(item)

		return item in self and self[item].banned

	def get(self, item, attribute):
		if not item in self:
			return None

		return getattr(self[item], attribute)

	def set(self, item, attribute, value):
		entity = self.add(item)
		old_value = getattr(entity, attribute)

		if old_value != value:
			setattr(entity, attribute, value)
			entity.dirty = True

	def add(self, item):
		if not item in self:
			entity = self.__type(-1, item)
			entity.dirty = True
			entity.registered = True
			self[item] = entity

		self.on_added(item)

		return self[item]

	def remove(self, item):
		if not item in self:
			return

		entity = self[item]

		if entity.banned and entity.registered:
			entity.registered = False
			entity.clear()
			entity.dirty = True
		elif not entity.banned:
			if not item.lower() in self.__deleted_items:
				self.__deleted_items.append(entity)

			del(self[item])

		self.on_removed(item)

	def ban(self, item, source, reason, date, expiry):
		entity = self.add(item)
		entity.banned = True
		entity.ban_source = source
		entity.ban_reason = reason
		entity.ban_date = date
		entity.ban_expiry = expiry
		entity.dirty = True

		self.on_banned(item)

	def unban(self, item):
		if not item in self or not self[item].banned:
			return

		entity = self[item]
		entity.banned = False
		entity.ban_source = None
		entity.ban_reason = None
		entity.ban_date = None
		entity.ban_expiry = None

		if not entity.registered:
			self.remove(item)
		else:
			entity.dirty = True

		self.on_unbanned(item)

	def on_added(self, item):
		pass

	def on_removed(self, item):
		pass

	def on_banned(self, item):
		pass

	def on_unbanned(self, item):
		pass

	def clear_deleted(self):
		self.__deleted_items = []

	def list_deleted(self):
		return self.__deleted_items

	def list_dirty(self):
		items = [item for item in self]
		return [self[item] for item in items if self.is_dirty(item)]

	def list_valid(self):
		items = [item for item in self]
		return [self[item] for item in items if self.is_valid(item)]

	def list_banned(self):
		items = [item for item in self]
		return [self[item] for item in items if self.is_banned(item)]
