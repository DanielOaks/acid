ARG_NO          = 0x1
ARG_YES         = 0x2
ARG_OPT         = 0x4
ARG_OFFLINE     = 0x8
ARG_OFFLINE_REQ = 0x10

class CommandManager(object):
	def __init__(self):
		self.prefix = self.get_prefix()
		self.invalid = self.get_invalid()
		self.commands = self.get_commands()
		self.generate_help()

	def get_prefix(self):
		return ''

	def get_invalid(self):
		return ''

	def get_commands(self):
		return {}

	def get_command(self, command):
		command = command.lower()

		if not command.startswith(self.prefix):
			return None

		command = command[len(self.prefix):]

		if not command in self.commands:
			return None

		command = self.commands[command]

		if not isinstance(command, basestring):
			return command

		command = command.lower()

		if not command in self.commands:
			return None

		return self.commands[command]

	def get_help(self, command = None):
		if command == None:
			return self.help
		else:
			if command.startswith(self.prefix):
				command = command[len(self.prefix):]

			if command in self.help_command:
				return self.help_command[command]

		return None

	def add_help_command(self, command):
		cmd = self.commands[command]

		if isinstance(cmd, basestring):
			cmd = self.commands[cmd]

		message = []

		cmd_type = cmd[1]
		cmd_desc = cmd[2]
		cmd_args = cmd[3]

		message.append('@b%s%s: %s' % (self.prefix, command, cmd_desc))
		message.append(' ')

		msg = 'Usage: @b%s%s@b' % (self.prefix, command)

		if len(cmd_args) > 0:
			msg += ' ' + ' '.join(['[%s%s]' % (cmd_arg[1], (' ' + cmd_arg[0]) if ('action' not in cmd_arg[3] or cmd_arg[3]['action'] != 'store_true') else '') for cmd_arg in cmd_args])

		if len(cmd) > 4:
			argument_name = cmd[4]
		else:
			argument_name = 'argument'

		if cmd_type & ARG_YES:
			msg += ' %s' % argument_name
		elif cmd_type & ARG_OPT:
			msg += ' [%s]' % argument_name

		message.append(msg)
		message.append('')

		longest = 0

		for cmd_arg in cmd_args:
			longest = len(cmd_arg[0]) if len(cmd_arg[0]) > longest else longest

		for cmd_arg in cmd_args:
			message.append('@b--%-*s (%s)@b   %s' % (longest + 1, cmd_arg[0], cmd_arg[1], cmd_arg[2]))

		self.help_command[command] = message


	def generate_help(self):
		self.help = []

		self.help.append('@bCommands@b (type @b%shelp command name@b for detailed information):' % self.prefix)
		self.help.append(' ')

		longest = 0
		alias_dict = {}
		commands = {}

		for cmd in self.commands:
			if isinstance(self.commands[cmd], basestring):
				orig = self.commands[cmd]

				if orig in alias_dict:
					alias_dict[orig].append(cmd)
				else:
					alias_dict[orig] = [cmd]
			else:
				if not cmd in alias_dict:
					alias_dict[cmd] = []

		for key in alias_dict:
			cur = key + ('' if len(alias_dict[key]) == 0 else (' (' + ', '.join(alias_dict[key]) + ')'))
			longest = len(cur) if len(cur) > longest else longest
			commands[cur] = self.commands[key][2]

		for cmd in sorted(commands):
			self.help.append('@b%-*s@b   %s' % (longest + 1, cmd, commands[cmd]))

		self.help_command = {}

		for command in self.commands:
			self.add_help_command(command)
