import time
import threading
from sys_base import *
import istring

class AntiFloodManager(Subsystem):
	def __init__(self, module):
		Subsystem.__init__(self, module, module.options, 'antiflood')
		self.lock_user = threading.Lock()
		self.users = {}
	
	def on_reload(self):
		self.delay_base_user = self.get_option('delay_base_user', int, 1)
		self.delay_mult_pre_warn_user = self.get_option('delay_mult_pre_warn_user', int, 3)
		self.delay_mult_post_warn_user = self.get_option('delay_mult_post_warn_user', int, 10)
		self.ban_duration_user = self.get_option('ban_duration_user', int, 5 * 24 * 60 * 60)
		self.score_ban_user = self.get_option('score_ban_user', int, 6)
		self.score_warn_user = self.get_option('score_warn_user', int, 3)
		self.points_user = self.get_option('points_user', int, 1)
		self.points_repeat_user = self.get_option('points_repeat_user', int, 2)
		self.warning_message_user = self.get_option('warning_message_user', unicode, 'Slow down, wait @timeout seconds! If you continue flooding the bot you will be banned.')

	def get_delay_user(self, score):
		score_pre_warn = score
		score_post_warn = 0

		if score > self.score_warn_user:
			score_post_warn = score - self.score_warn_user
			score_pre_warn = self.score_warn_user

		return self.delay_base_user + (score_pre_warn * self.delay_mult_pre_warn_user) + (score_post_warn * self.delay_mult_post_warn_user)

	def check_user(self, username, command, argument):
		command = command.lower().strip()
		argument = argument.lower().strip()
		now = int(time.time())

		try:
			self.lock_user.acquire()

			if not username in self.users:
				self.users[username] = [now, 0, command, argument]
				return False

			user = self.users[username]

			if now - user[0] <= self.get_delay_user(user[1]):
				if user[2] == command and user[3] == argument:
					user[1] += self.points_repeat_user
				else:
					user[2] = command
					user[3] = argument
					user[1] += self.points_user
			else:
				user[1] = 0
				user[2] = command
				user[3] = argument

			user[0] = now
		finally:
			self.lock_user.release()

		if user[1] >= self.score_ban_user:
			self.module.users.ban(username, "anti-flood", "flooding", now, now + self.ban_duration_user)
			self.module.elog.warning('Banned @b%s@b for flooding.' % username)
			user[0] = -1
			return True

		if user[1] >= self.score_warn_user:
			self.module.notice(username, self.warning_message_user.replace('@timeout', istring(self.get_delay_user(user[1]))))

		return False

	def commit(self):
		new_users = {}
		now = int(time.time())
		self.lock_user.acquire()

		for username in self.users:
			user = self.users[username]

			if now - user[0] <= self.get_delay_user(user[1]):
				new_users[username] = user

		self.users = new_users
		self.lock_user.release()
