#
# umodule.py
# User Module for Acid+PyPsd
# Copyright (c) 2014 Kristina Brooks, Daniel Oaks
#
# Assumed Subsystems:
#   Elog, Options, Channels, Auth
# We assume these are loaded, so if you disable these you'll need to provide replacements or we break
#
# Optional Subsystems:
#   Users, Antiflood
#

import logging

import sys_antiflood
import sys_auth
import sys_channels
import sys_log
import sys_options
import sys_users

from pseudoclient.cmd_manager import ARG_NO, ARG_YES, ARG_OPT, ARG_OFFLINE
from sys_command import CommandManager
from sys_base import Subsystem
import inviteable
import mythreading as threading
from istring import istring
from utils import strip_ascii_irc, format_ascii_irc
from core import anope_major
from plugin import AcidPlugin


# UModule stuff
class UModuleException(Exception):
	"""Throws if UModule has an error."""
	pass


class UModule(AcidPlugin):
	"""User Module for Acid+PyPsd.

	Provides a sane base to code general user-facing bots, including loading
	  standard subsystems and standard event handling for those.
	"""
	name = None          # Name of this module (will be used in SQL tables, etc). eg: 'twitter'
	display_name = None  # Displayable name of the module (will be used in privmsgs, etc). eg: 'TwitterBot'
	version = None       # Version of this module. eg: '0.0.1', [0, 1, 8]
	developers = None    # String listing developers' names, separated by commas. eg: 'Mary, John, Alex'

	ignored_default_subsystems = []  # add subsystem names here to not load them at startup
	loaded_optional_subsystems = []  # add optional subsystem names to load them at startup

	subsystems_to_autoload = []  # internal: subsystems we're going to load
	loaded_subsystems = []       # internal: subsystems we've loaded

	def __init__(self, custom_commands=False):
		"""Initializes UModule. If custom_commands, we don't load the standard commands."""
		AcidPlugin.__init__(self)

		# standard info variables
		missing_message = "You need to define {var} in this module's class variables"

		if self.name is None:
			errmsg = missing_message.format(var='name')
			self.log.exception(errmsg)
			raise UModuleException(errmsg)

		if self.version is None:
			errmsg = missing_message.format(var='version')
			self.log.exception(errmsg)
			raise UModuleException(errmsg)

		if self.display_name is None:
			self.display_name = self.name.title()

		# display version
		# this means if self.version is something like:   [2, 4, 5]
		#   then display_version will be:   '2.4.5'
		# basically so we can always just display this variable,
		#   regardless of whether the dev versions their module as a tuple
		if isinstance(self.version, (list, tuple)):
			self.display_version = '.'.join(self.version)
		else:
			self.display_version = str(self.version)

		# user
		try:
			self.nick = istring(self.config.get(self.name, 'nick'))
		except Exception, err:
			self.log.exception("Error reading '{name}:nick' configuration option: {err}".format(name=self.name, err=err))
			raise

		try:
			self.chan = istring(self.config.get(self.name, 'channel'))
		except Exception, err:
			self.log.exception("Error reading '{name}:channel' configuration option: {err}".format(name=self.name, err=err))
			raise

		# subsystems
		if 'options' not in self.ignored_default_subsystems:
			self.autoload_subsystem('options', sys_options.OptionManager)
		if 'elog' not in self.ignored_default_subsystems:
			self.autoload_subsystem('elog', sys_log.LogManager)
		if 'channels' not in self.ignored_default_subsystems:
			self.autoload_subsystem('channels', sys_channels.ChannelManager)
		if 'auth' not in self.ignored_default_subsystems:
			self.autoload_subsystem('auth', sys_auth.AuthManager)

		if 'users' in self.loaded_optional_subsystems:
			self.autoload_subsystem('users', sys_users.UserManager)
		if 'antiflood' in self.loaded_optional_subsystems:
			self.autoload_subsystem('antiflood', sys_antiflood.AntiFloodManager)

		# command handler
		if not custom_commands:
			self.commands_private = DynamicCommandManager()

		# state
		self.initialized = False

	# starting and stopping
	def start(self, set_running=True):
		"""Startup UModule."""
		try:
			AcidPlugin.start(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for {name} module ({err})'.format(name=self.name, err=err))
			raise

		# included subsystems
		try:
			self.init_subsystems()
		except Exception, err:
			self.log.exception('Error initializing subsystems for {name} module ({err})'.format(name=self.name, err=err))
			raise

		# subsystems
		try:
			self.start_subsystems()
		except Exception, err:
			self.log.exception('Error starting subsystems for {name} module ({err})'.format(name=self.name, err=err))
			raise

		# state
		if set_running:
			self.initialized = True

	def stop(self):
		"""Stop and shutdown."""
		try:
			AcidPlugin.stop(self)
		except Exception, err:
			self.log.exception('Error stopping down AcidPlugin ({err})'.format(err=err))
			raise

		# state
		self.initialized = False

		# subsystems
		try:
			self.stop_subsystems()
		except Exception, err:
			self.log.exception('Error stopping subsystems ({err})'.format(err=err))
			raise

	# subsystems
	def autoload_subsystem(self, name, subsystem_class, args=[], kwargs={}):
		"""Autoloads the given subsystem when we start.

		Args:
			name: subsystem name, for instance, subsystem named 'kiwi' would be accessed by self.kiwi
			subsystem_class: subsystem manager class
			args: list of positional args to give to the subsystem class upon instansiation
			kwargs: dict of keyword args to give to the subsystem class upon instansiation
		"""
		if name in self.subsystems_to_autoload:
			self.log.error("Subsystem {sub} already set to load in {name} module, skipping it".format(name=self.name, sub=name))
			return
		subsystem_tuple = (name, subsystem_class, args, kwargs)
		self.subsystems_to_autoload.append(subsystem_tuple)

	def init_subsystems(self):
		"""Initializes our subsystems."""
		for name, subsystem_class, args, kwargs in self.subsystems_to_autoload:
			if name in self.loaded_subsystems:
				self.log.error("Subsystem {sub} already loaded in {name} module, skipping it".format(name=self.name, sub=name))
				continue
			loaded_subsystem = subsystem_class(self, *args, **kwargs)
			setattr(self, name, loaded_subsystem)
			self.loaded_subsystems.append(name)

	def start_subsystems(self):
		"""Starts our subsystems."""
		# we load subsystems in FIFO order, since the later-loaded
		#   ones can depend on subsystems loaded earlier
		for name in self.loaded_subsystems:
			manager = getattr(self, name, None)

			if manager is not None:
				manager.start()

	def stop_subsystems(self):
		"""Stop and shutdown our subsystems."""
		while self.loaded_subsystems:
			# we unload subsystems in FILO order, since the later-loaded
			#   ones can depend on subsystems loaded earlier
			name = self.loaded_subsystems.pop()

			manager = getattr(self, name, None)

			if manager is not None:
				if manager.sync_db_on_shutdown and self.initialized:
					manager.force()

				manager.stop()

				if manager.sync_db_on_shutdown:
					manager.db_close()

			self.loaded_subsystems.remove(name)

	# actions
	def join(self, channel):
		"""IRC Command: JOIN `channel`"""
		me = self.inter.findUser(self.nick)
		me.joinChan(channel)

	def part(self, channel):
		"""IRC Command: PART `channel`"""
		me = self.inter.findUser(self.nick)
		me.partChan(channel)

	def errormsg(self, target, message, source=None):
		"""IRC Command: PRIVMSG `target` the error message `message`

		Args:
			target: Nick/channel we're sending to
			message: Message to send, should be escaped if control codes not wanted
			source: Nick to send it from, defaults to our own
		"""
		self.msg(target, '@b@c4Error:@o {}'.format(message), format=True, source=source)

	def ctcp(self, target, message, format=False, source=None):
		"""IRC Command: PRIVMSG `target` \\001`message`\\001

		Args:
			target: Nick/channel we're sending to
			message: CTCP to send, eg: 'TIME', 'VERISON'
			format: Whether to change @b to bold, @c to color control char, etc
			source: Nick to send it from, defaults to our own
		"""
		self.msg(target, '\001{}\001'.format(message), format=format, source=source)

	def msg(self, target, message, format=True, source=None):
		"""IRC Command: PRIVMSG `target` `message`

		Args:
			target: Nick/channel we're sending to
			message: Message to send
			format: Whether to change @b to bold, @c to color control char, etc
			source: Nick to send it from, defaults to our own
		"""
		if message != '':
			if format:
				message = format_ascii_irc(message)
			if source is None:
				source = self.nick
			self.inter.privmsg(source, target, message)

	def notice(self, target, message, format=True, source=None):
		"""IRC Command: NOTICE `target` `message`

		Args:
			target: Nick/channel we're sending to
			message: Message to send
			format: Whether to change @b to bold, @c to color control char, etc
			source: Nick to send it from, defaults to our own
		"""
		if message != '':
			if format:
				message = format_ascii_irc(message)
			if source is None:
				source = self.nick
			self.inter.notice(source, target, message)

	def multimsg(self, target, count, intro, separator, pieces, outro='', source=None):
		"""IRC Command: Send a multipart message to someone.

		Args:
			target: Nick/channel we're sending to
			count: How many items of `pieces` we wish to send with each individual msge
			intro: String prefixing every individual msg
			separator: String inbetween pieces parts, if count > 0
			pieces: List of things to send to `target`
			outro: String suffixing every individual msg
			source: Nick to send it from, defaults to our own

		Examples:
			...
		"""
		# TODO: Add some examples to docstring, because this is a complex command
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro, source=source)
			cur += count

	# standard handlers
	def onSync(self):
		"""Called when the server syncs."""
		# join channels
		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')

	def onChanModes(self, prefix, channel, modes):
		"""Handle channel modes change."""
		if not self.initialized:
			return

		if modes == '-z' and channel in self.channels:
			self.channels.remove(channel)
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)

	# override this if you want to add additional crap
	# (ie check if the chan is eligible or not)
	def do_accept(self, nick, channel):
		self.auth.accept(nick)

	def onNotice(self, source, target, message):
		"""Handle incoming notices."""
		print 'notice: {}, {}, {}'.format(source, target, message)

		if not self.initialized:
			return

		me = self.inter.findUser(self.nick)
		user = self.inter.findUser(target)
		userinfo = self.inter.findUser(source)

		if me != user or (userinfo is not None and userinfo['nick'] != 'ChanServ'):
			return

		try:
			msg = message.strip()
		except:
			return

		if msg.startswith('[#'):  # It's a channel welcome message. Let's ignore it.
			return

		self.elog.chanserv('%s' % msg)
		sp = msg.split(' ')

		if userinfo is None:
			if 'tried to kick you from' in msg:
				nick = strip_ascii_irc(sp[1])
				channel = strip_ascii_irc(sp[7])
				self.notice(nick, 'To remove this bot (must be channel founder): @b/msg %s remove %s@b' % (self.nick, channel))

			return

		if anope_major == 2 and msg == 'Password authentication required for that command.':
			self.elog.error('%s is not authed with services (maybe a wrong NickServ password was configured?).' % target)

		# common to both anope1 and anope2
		if "isn't registered" in msg:
			self.auth.reject_not_registered(strip_ascii_irc(sp[1]))
			return

		if len(sp) < 6:
			return

		if 'inviting' in sp[2]:  # It's an invite notice. Let's ignore it.
			return

		nick = strip_ascii_irc(sp[0])
		channel = sp[5][:-1]

		if anope_major == 1:
			should_accept = 'Founder' in sp[2]
		elif anope_major == 2:
			channel = strip_ascii_irc(channel)
			should_accept = ('founder' == sp[3])

		if should_accept:
			self.do_accept(nick, channel)
		else:
			self.auth.reject_not_founder(nick, channel)

	def onPrivmsg(self, source, target, message):
		"""Handle privmsgs."""
		print 'privmsg: {}, {}, {}'.format(source, target, message)
		if not self.initialized:
			return False

		userinfo = self.inter.findUser(source)
		myself = self.inter.findUser(self.nick)

		sender = userinfo['nick']
		channel = target
		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			argument = ''
		else:
			command = msg[:index]
			argument = msg[index + 1:]

		# do we have a deferal command handler?
		if hasattr(self, 'execute'):
			if channel == myself['nick'] and command.startswith(self.commands_private.prefix):  # XXX uid
				self.elog.debug('Deferred parsing of private message (sender: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, command, argument))
				threading.deferToThread(self.execute, self.commands_private, command, argument, sender, sender, userinfo)
			elif self.channels.is_valid(channel) and command.startswith(self.commands_user.prefix):
				self.elog.debug('Deferred parsing of channel message (sender: @b%s@b, channel: @b%s@b, command: @b%s@b, argument: @b%s@b)' % (sender, channel, command, argument))
				threading.deferToThread(self.execute, self.commands_user, command, argument, channel, sender, userinfo)
		elif target == self.nick and hasattr(self, 'commands_private'):
			cmd = self.commands_private.get_command(command)
			if cmd is None:
				# not found, forward through
				return True
			#     (self, manager,               opts,    arg,     channel, sender, userinfo):
			cmd[0](self, self.commands_private, command, argument, sender, sender, userinfo)
		else:
			# forward anyway
			return True


# Sane subsystem wrapper
class UModuleSubsystem(Subsystem):
	"""Provides a saner wrapper for subsystems."""
	name = None  # name of your subsystem, eg 'db' or 'games'

	def __init__(self, module):
		if self.name is None:
			raise NameError("You must set self.name in the class variables of this subsystem. ({name} -> {cls})".format(name=module.name, cls=type(self).__name__))

		Subsystem.__init__(self, module, module.options, self.name)


# Command Manager interface
def private_info(self, manager, opts, arg, channel, sender, userinfo):
	"""Shows info about the module's version and developers."""
	message = '@sep @bRizon {} Bot@b @sep @bVersion@b {} @sep'.format(self.display_name, self.display_version)

	# this is why we separate them with a comma
	if self.developers is not None:
		if ',' in self.developers:
			plural = 's'
		else:
			plural = ''
		message += ' @bDeveloper{}:@b {} @sep'.format(plural, self.developers)

	self.notice(sender, message)


class DynamicCommandManager(CommandManager):
	"""Similar to the normal CommandManager, but the commands are defined at init time."""
	def __init__(self, can_join_channels=True):
		"""Setup basic commands for out DynamicCommandManager.

		Args:
			can_join_channels: Whether this bot can join channels, adds the 'request' and 'remove' commands if true
			compile_commands: Whether to compile commands ourselves. if you call this yourself, this should be false
		"""
		self.dynamic_commands = {}

		if can_join_channels:
			self.add_command('request', inviteable.private_channel_request, ARG_YES | ARG_OFFLINE, 'requests a channel (must be founder)', [], '#channel')
			self.add_command('remove', inviteable.private_channel_remove, ARG_YES | ARG_OFFLINE, 'removes a channel (must be founder)', [], '#channel')
		self.add_command('info', private_info, ARG_NO, 'displays info text')
		self.add_command(('help', 'hi', 'hello'), inviteable.private_help, ARG_OPT, 'displays help text')

		# modules will call this after adding their own commands
		self.compile_commands()

	def add_command(self, name, handler, arg_options, description, sub_arguments=[], arg_followup=None):
		"""Add an argument to this command handler.

		Args:
			name: Name of the argument. This can also be a tuple for multiple aliases
			handler: Function that gets called when this command is used
			arg_options: ARG_YES (there are args), ARG_NO (no args), ARG_OPT (optional), ARG_OFFLINE (unknown)
			description: Command description
			sub_arguments: Very interesting. Refer to Internets for examples
			arg_followup: Shows the user what arguments they need to put in
		"""
		# names
		if isinstance(name, (list, tuple)):
			name = list(name)  # convert to mutable list if it's a tuple

			if len(name) > 0:
				primary_name = name.pop(0)
			else:
				raise NameError("You must supply at least one name for add_command (name was empty tuple or list)")
			other_names = name
		else:
			primary_name = name
			other_names = []

		# primary command
		command = [handler, arg_options, description, sub_arguments]
		if arg_followup is not None:
			command.append(arg_followup)

		self.dynamic_commands[primary_name] = command

		# aliased names
		for alias in other_names:
			self.dynamic_commands[alias] = primary_name

	def remove_command(self, name):
		"""Mostly just to complete it, this removes arguments. If 'name' has aliases, removes those too."""
		# to let us accept lists of names
		if isinstance(name, (list, tuple)):
			names = name
		else:
			names = [name]

		# removal
		for primary_name in names:
			# removing aliases
			for command_name in self.dynamic_commands:
				if self.dynamic_commands[command_name] == primary_name:
					try:
						del self.dynamic_commands[command_name]
					except:
						pass  # just in case race conditions

			# removing actual command
			if primary_name in self.dynamic_commands:
				try:
					del self.dynamic_commands[primary_name]
				except:
					pass  # just in case race conditions

	def get_commands(self):
		"""Returns our command dict."""
		return self.dynamic_commands

	def compile_commands(self):
		"""Compile all of our commands into our dicts/etc. Should be called in __init__"""
		CommandManager.__init__(self)
