#
# Shared admin commands.
#

from sys_base import Subsystem
from datetime import datetime
from utils import *

def build_ban_message(entity):
	message = 'Source: @b%s@b.' % entity.ban_source

	if entity.ban_date != None:
		message += ' Date: @b%s@b.' % datetime.fromtimestamp(entity.ban_date)

	if entity.ban_expiry != None:
		message += ' Expiry: @b%s@b.' % datetime.fromtimestamp(entity.ban_expiry)

	if entity.ban_reason != None:
		message += ' Reason: @b%s@b.' % entity.ban_reason

	return message

def admin_db(self, source, target, pieces):
	if len(pieces) >= 1:
		state = pieces[0].lower()

		if state in ['on', 'enable']:
			Subsystem.set_db_up(True)
		elif state in ['off', 'disable']:
			Subsystem.set_db_up(False)
		elif state in ['state']:
			pass
		else:
			return False

	self.msg(target, 'Automatic database commits: @b%s@b.' % (
		'enabled' if Subsystem.get_db_up() else 'disabled')
	)

	return True

def admin_opt(self, source, target, pieces):
	if len(pieces) < 1:
		for option in sorted(self.options):
			opt = self.options[option]
			self.msg(target, '@b%s@b: %s' % (opt[0], opt[1]))

		return True

	if len(pieces) < 2:
		return False

	operation = pieces[0]
	name = pieces[1]

	if operation == 'get':
		value = self.options.get(name, unicode)

		if value == None:
			self.msg(target, 'Option @b%s@b not found.' % name)
		else:
			self.msg(target, '@b%s@b: %s' % (name, value))
	elif operation == 'set':
		if len(pieces) < 3:
			return False

		value = ' '.join(pieces[2:])
		value = self.options.set(name, value)
		self.msg(target, '@b%s@b: %s' % (name, value))
	elif operation == 'clear':
		if self.options.clear(name):
			self.msg(target, 'Option @b%s@b cleared.' % name)
		else:
			self.msg(target, 'Option @b%s@b not found.' % name)

	return True

def admin_user(self, source, target, pieces, meta=None):
	if len(pieces) < 1:
		return False

	action = pieces[0]

	if len(pieces) < 2:
		if action in ['l', 'list']:
			users = ['%s' % user.name for user in self.users.list_valid()]

			if len(users) == 0:
				self.msg(target, 'No users.')
			else:
				self.multimsg(target, 10, 'Users: ', ', ', users)
		elif action in ['bl', 'blist']:
			bans = ['@c10@b%s@o - %s' % (user.name, build_ban_message(user)) for user in self.users.list_banned()]

			if len(bans) == 0:
				self.msg(target, 'No banned users.')
			else:
				self.multimsg(target, 1, '', ', ', bans)
		else:
			return False

		return True

	username = pieces[1]

	if action in ['b', 'ban']:
		now = unix_time(datetime.now())

		if len(pieces) > 2:
			try:
				duration = parse_timespan(pieces[2])
			except:
				duration = 0

			if duration <= 0:
				self.msg(target, '%s is not a valid duration.' % pieces[2])
				return True

			expiry = now + duration
		else:
			expiry = None

		if len(pieces) > 3:
			reason = ' '.join(pieces[3:])
		else:
			reason = None

		message = 'Banned user @b%s@b.' % username

		if reason != None:
			message += ' Reason: @b%s@b.' % reason

		if expiry != None:
			message += ' Expiry: @b%s@b.' % datetime.fromtimestamp(expiry)

		srcu = self.inter.findUser(source)
		self.users.ban(username, srcu['nick'], reason, now, expiry)
		self.msg(target, message)
	elif action in ['u', 'unban']:
		if not self.users.is_banned(username):
			self.msg(target, 'User @b%s@b is not banned.' % username)
		else:
			self.users.unban(username)
			self.msg(target, 'Unbanned user @b%s@b' % username)
	elif action in ['i', 'info']:
		if username in self.users:
			user = self.users[username]

			message = ''

			if not user.registered:
				message += '(Temporary) '

			message += '@b%s@b.' % user.name

			if isinstance(meta, dict) and 'extra_info' in meta:
				message += meta['extra_info'](user)

			if self.users.is_banned(username):
				message += ' Banned by @b%s@b' % user.ban_source

				if user.ban_reason != None:
					message += ' for @b%s@b' % user.ban_reason

				message += ' on @b%s@b.' % datetime.fromtimestamp(user.ban_date)

				if user.ban_expiry != None:
					message += ' Ban expires on @b%s@b.' % datetime.fromtimestamp(user.ban_expiry)

			self.msg(target, message)
		else:
			self.msg(target, 'User @b%s@b is not in the database.' % username)
	elif action in ['a', 'add']:
		if not username in self.users:
			self.users.add(username)
			self.msg(target, 'Added user @b%s@b.' % username)
		else:
			self.msg(target, 'User @b%s@b is already in the database.' % username)
	elif action in ['r', 'remove']:
		if username in self.users:
			self.users.remove(username)
			self.msg(target, 'Removed user @b%s@b.' % username)
		else:
			self.msg(target, 'User @b%s@b is not in the database.' % username)
	elif isinstance(meta, dict) and 'cmds' in meta and action in meta['cmds']:
		return meta[action](self, action, username, source, target, pieces)
	else:
		return False

	return True

def admin_chan(self, source, target, pieces, meta=None):
	if len(pieces) < 1:
		return False

	action = pieces[0]

	if len(pieces) < 2:
		if action in ['l', 'list']:
			channels = ['%s' % chan.name for chan in self.channels.list_valid()]

			if len(channels) == 0:
				self.msg(target, 'No channels.')
			else:
				self.multimsg(target, 10, 'Channels: ', ', ', channels)
		elif action in ['bl', 'blist']:
			bans = ['@c10@b%s@o - %s' % (chan.name, build_ban_message(chan)) for chan in self.channels.list_banned()]

			if len(bans) == 0:
				self.msg(target, 'No banned channels.')
			else:
				self.multimsg(target, 1, '', ', ', bans)
		else:
			return False

		return True

	channel = pieces[1]

	if action in ['b', 'ban']:
		now = unix_time(datetime.now())

		if len(pieces) > 2:
			try:
				duration = parse_timespan(pieces[2])
			except:
				duration = 0

			if duration <= 0:
				self.msg(target, '%s is not a valid duration.' % pieces[2])
				return True

			expiry = now + duration
		else:
			expiry = None

		if len(pieces) > 3:
			reason = ' '.join(pieces[3:])
		else:
			reason = None

		message = 'Banned channel @b%s@b.' % channel

		if reason != None:
			message += ' Reason: @b%s@b.' % reason

		if expiry != None:
			message += ' Expiry: @b%s@b.' % datetime.fromtimestamp(expiry)

		srcu = self.inter.findUser(source)
		self.channels.ban(channel, srcu['nick'], reason, now, expiry)
		self.msg(target, message)
	elif action in ['u', 'unban']:
		if not self.channels.is_banned(channel):
			self.msg(target, 'Channel @b%s@b is not banned.' % channel)
		else:
			self.channels.unban(channel)
			self.msg(target, 'Unbanned channel @b%s@b' % channel)
	elif action in ['i', 'info']:
		if channel in self.channels:
			chan = self.channels[channel]

			message = ''

			if not chan.registered:
				message += '(Temporary) '

			message += '@b%s@b.' % chan.name

			if isinstance(meta, dict) and 'extra_info' in meta:
				message += meta['extra_info'](chan)

			if self.channels.is_banned(channel):
				message += ' Banned by @b%s@b' % chan.ban_source

				if chan.ban_reason != None:
					message += ' for @b%s@b' % chan.ban_reason

				message += ' on @b%s@b.' % datetime.fromtimestamp(chan.ban_date)

				if chan.ban_expiry != None:
					message += ' Ban expires on @b%s@b.' % datetime.fromtimestamp(chan.ban_expiry)

			self.msg(target, message)
		else:
			self.msg(target, 'Channel @b%s@b is not in the database.' % channel)
	elif action in ['a', 'add']:
		if not channel in self.channels:
			self.channels.add(channel)
			self.msg(target, 'Added channel @b%s@b.' % channel)
		else:
			self.msg(target, 'Channel @b%s@b is already in the database.' % channel)
	elif action in ['r', 'remove']:
		if channel in self.channels:
			self.channels.remove(channel)
			self.msg(target, 'Removed channel @b%s@b.' % channel)
		else:
			self.msg(target, 'Channel @b%s@b is not in the database.' % channel)
	elif action in ['n', 'news']:
		if channel in self.channels:
			cur_state = self.channels[channel].news
			self.channels.set(channel, 'news', not cur_state)
			self.msg(target, 'Toggled news for channel @b%s@b. Current state: %s.' % (channel, 'disabled' if cur_state else 'enabled'))
		else:
			self.msg(target, 'Channel @b%s@b is not in the database.' % channel)
	elif isinstance(meta, dict) and 'cmds' in meta and action in meta['cmds']:
		ret = meta[action](self, action, channel, source, target, pieces)
		# i'm letting None imply that this command succeded
		# just sort of easier that way
		return True if ret is None else ret
	else:
		return False

	return True

def admin_unregistered(self, source, target, pieces):
	if len(pieces) < 1:
		return False

	action = pieces[0]

	if action not in ['check', 'list', 'part']:
		return

	channels = []
	for chan in self.channels.list_all():
		c = self.inter.findChannel(chan.name)
		if not c:
			continue
		modes = c['modes']
		if 'z' not in modes:
			channels.append({'name': chan.name, 'modes': modes, 'users': c.size()})
	
	total = len(channels)
	if total == 0:
		self.msg(target, 'No unregistered channels.')
		return True
	
	if action == 'check':
		self.msg(target, 'There are %d unregistered channels.' % total)
	elif action == 'list':
		for n, chan in enumerate(channels, 1):
			self.msg(target, '%(n)d) @b%(name)s@b [%(modes)s] @b%(users)d@b users' % {
				'n': n,
				'name': chan['name'],
				'modes': chan['modes'] if chan['modes'].startswith('+') else '+' + chan['modes'],
				'users': chan['users']})
	elif action == 'part':
		for n, chan in enumerate(channels, 1):
			self.channels.remove(str(chan['name']))
			self.msg(target, '%(n)d) Removed unregistered channel @b%(name)s@b' % {
				'n': n,
				'name': chan['name']})
	
	return True

def admin_log(self, source, target, pieces):
	if len(pieces) < 1:
		self.msg(target, 'Log level is @b%d@b.' % self.elog.level)
		return True

	try:
		level = int(pieces[0])
	except:
		return False

	if level < 0 or level > 7:
		return False

	self.elog.set_level(level)
	self.msg(target, 'Log level set to @b%d@b.' % level)
	return True

def admin_msg(self, source, target, pieces):
	if len(pieces) < 1:
		return False

	srcu = self.inter.findUser(source)
	sender = srcu['nick']
	message = ' '.join(pieces)

	for channel in self.channels.list_valid():
		self.msg(channel.name, '@b[Global message by %s]@b %s' % (sender, message))

	return True