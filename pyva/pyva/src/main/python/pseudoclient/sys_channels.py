from collection import *
from sys_base import *

class Channel(CollectionEntity):
	def __init__(self, id, name):
		CollectionEntity.__init__(self, id)
		self.name = name
	
	news = None

class ChannelManager(CollectionManager, Subsystem):
	def __init__(self, module, type = Channel):
		Subsystem.__init__(self, module, module.options, 'channels')
		CollectionManager.__init__(self, type)

	def on_added(self, channel):
		self.module.join(channel)

	def on_removed(self, channel):
		self.module.part(channel)

	def on_banned(self, channel):
		self.module.part(channel)

	def on_unbanned(self, channel):
		if channel in self:
			self.module.join(channel)
