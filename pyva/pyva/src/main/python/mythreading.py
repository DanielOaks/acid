from threading import Thread

def deferToThread(func, *args):
	t = Thread(target=func, args=args)
	t.start()


