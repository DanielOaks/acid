package net.rizon.acid.plugins.pyva.pyva.commands;

import java.util.logging.Level;

import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Logger;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.pyva.pyva.pyva;
import net.rizon.pyva.PyvaException;

public class Plugins extends Command
{
	private static final Logger log = Logger.getLogger(Plugins.class.getName());
	
	public Plugins()
	{
		super(1, 2);
	}

	@Override
	public void Run(User u, AcidUser to, Channel c, String[] args)
	{
		String[] loaded;
		try
		{
			loaded = pyva.getPyvaPlugins();
		}
		catch (PyvaException e)
		{
			log.log(Level.WARNING, "Unable to get current plugins", e);
			return;
		}
		
		if (args[0].equalsIgnoreCase("LIST"))
		{
			for (String s : loaded)
				Acidictive.reply(u, to, c, s);
			Acidictive.reply(u, to, c, loaded.length + " plugins loaded.");
		}
		else if (args[0].equalsIgnoreCase("LOAD") && args.length == 2)
		{
			for (String s : loaded)
				if (s.equalsIgnoreCase(args[1]))
				{
					Acidictive.reply(u, to, c, s + " is already loaded.");
					return;
				}
			
			try
			{
				pyva.loadPyvaPlugin(args[1]);
				Acidictive.reply(u, to, c, "Loaded " + args[1]);
			}
			catch (PyvaException e)
			{
				log.log(Level.WARNING, "Unable to load pyva script", e);
			}
		}
		else if (args[0].equalsIgnoreCase("UNLOAD") && args.length == 2)
		{
			boolean found = false;
			for (String s : loaded)
				if (s.equalsIgnoreCase(args[1]))
					found = true;
			
			if (!found)
			{
				Acidictive.reply(u, to, c, args[1] + " is not loaded");
				return;
			}
			
			try
			{
				pyva.unloadPyvaPlugin(args[1]);
				Acidictive.reply(u, to, c, "Unloaded " + args[1]);
			}
			catch (PyvaException e)
			{
				log.log(Level.WARNING, "Unable to unload pyva script", e);
			}
		}
		else if (args[0].equalsIgnoreCase("RELOAD") && args.length == 2)
		{
			try
			{
				pyva.unloadPyvaPlugin(args[1]);
				pyva.loadPyvaPlugin(args[1]);
				Acidictive.reply(u, to, c, args[1] + " has been reloaded.");
			}
			catch (PyvaException e)
			{
				log.log(Level.WARNING, "Unable to reload pyva script", e);
			}
		}
		else
			Acidictive.reply(u, to, c, "Use LOAD, UNLOAD, RELOAD, or LIST");
	}
}
