package net.rizon.acid.plugins.pyva.pyva;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.User;
import net.rizon.pyva.Interfacer;

public class Interface implements Interfacer
{
	public void privmsg(String source, String target, String message)
	{
		Acidictive.privmsg(source, target, message);
	}

	public void notice(String source, String target, String message)
	{
		Acidictive.notice(source, target, message);
	}
	
	public void mode(String source, String target, String modes)
	{
		Acidictive.setMode(source, target, modes);
	}

	public User findUser(String name)
	{
		return User.findUser(name);
	}

	public Channel findChannel(String name)
	{
		return Channel.findChannel(name);
	}
}
