package net.rizon.acid.plugins.pyva.pyva;

import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Event;
import net.rizon.acid.core.Logger;
import net.rizon.acid.core.Plugin;
import net.rizon.acid.core.Timer;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.pyva.pyva.conf.Config;
import net.rizon.acid.util.ClassLoader;
import net.rizon.pyva.Pyva;
import net.rizon.pyva.PyvaException;

class PyvaEvent extends Event
{
	@Override
	public void onUserConnect(final User u)
	{
		this.call("onUserConnect", u);
	}
	
	@Override
	public void onJoin(final String channel, final User[] users)
	{
		for (User u : users)
			this.call("onJoin", u, channel);
	}
	
	@Override
	public void onPart(final String parter, final String channel)
	{
		this.call("onPart", parter, channel);
	}

	@Override
	public void onKick(String kicker, User victim, String channel, String reason)
	{
		this.call("onKick", kicker, victim, channel, reason);
	}

	@Override
	public void onQuit(User user, String msg)
	{
		this.call("onQuit", user, msg);
	}
	
	@Override
	public boolean onPrivmsg(String creator, String recipient, final String msg)
	{
		creator = User.toName(creator);
		recipient = User.toName(recipient);
		this.call("onPrivmsg", creator, recipient, msg);
		return false;
	}
	
	@Override
	public boolean onNotice(String creator, String recipient, final String msg)
	{
		creator = User.toName(creator);
		recipient = User.toName(recipient);
		this.call("onNotice", creator, recipient, msg);
		return false;
	}
	
	@Override
	public boolean onCTCP(final String creator, final String recipient, final String msg)
	{
		this.call("onCtcp", creator, recipient, msg);
		return false;
	}

	@Override
	public void onCTCPReply(User source, String target, String message)
	{
		this.call("onCtcpReply", source.getUID(), target, message);
	}

	@Override
	public void onSync()
	{
		this.call("onSync");
	}

	@Override
	public void onChanModes(String prefix, Channel chan, String modes)
	{
		this.call("onChanModes", prefix, chan.getName(), modes);
	}
	
	private void call(String name, Object... params)
	{
		try
		{
			pyva.call(name, params);
		}
		catch (Exception e)
		{
			pyva.log.log(Level.WARNING, "Error executing pyva call", e);
		}
	}
}

class GCTimer extends Timer
{
	public GCTimer()
	{
		super(60, true);
	}

	@Override
	public void run(Date now)
	{
		System.gc();
		pyva.pyva.gc();
	}
}

public class pyva extends Plugin
{
	protected static final Logger log = Logger.getLogger(pyva.class.getName());

	private Config conf;
	public static Interface i = new Interface();
	private Timer t;
	public static Pyva pyva;

	private Event e;
	
	@Override
	public void start() throws Exception
	{
		reload();

		pyva = new Pyva();
		
		for (String path : conf.path)
			pyva.addToSystemPath(path);

		pyva.init();

		e = new PyvaEvent();
		
		for (String plugin : conf.plugins)
			loadPyvaPlugin(plugin);
		
		t = new GCTimer();
		t.start();
	}

	@Override
	public void stop()
	{
		t.stop();
		
		e.remove();
		
		try
		{
			for (String plugin : conf.plugins)
				unloadPyvaPlugin(plugin);
		}
		catch (PyvaException e)
		{
			log.log(Level.SEVERE, "Unable to unload pyva plugins", e);
		}

		pyva.stop();
	}

	@Override
	public void reload() throws Exception
	{
		conf = (Config) net.rizon.acid.conf.Config.load("pyva.yml", Config.class);
		
		Acidictive.loadClients(this, conf.clients);
	}
	
	public static void loadPyvaPlugin(String name) throws PyvaException
	{
		pyva.invoke("plugin", "loadPlugin", name, i);
	}

	public static void unloadPyvaPlugin(String name) throws PyvaException
	{
		pyva.invoke("plugin", "unloadPlugin", name);
	}

	public static String[] getPyvaPlugins() throws PyvaException
	{
		Object[] obj = (Object[]) pyva.invoke("plugin", "getPlugins");
		String[] stringArray = Arrays.copyOf(obj, obj.length, String[].class);
		return stringArray;
	}
	
	public static void call(String name, Object... args) throws PyvaException
	{
		Object[] object = new Object[args.length + 1];

		/* the first argument is the function, everything else is its parameters */
		object[0] = name;
		for (int i = 0; i < args.length; ++i)
			object[i + 1] = args[i];

		pyva.invoke("plugin", "call", object);
	}
}

