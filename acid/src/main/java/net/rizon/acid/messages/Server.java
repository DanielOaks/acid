package net.rizon.acid.messages;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;

public class Server extends Message
{
	public Server()
	{
		super("SERVER");
	}
	
	//SERVER rizon.hub 1 :Rizon IRC Network - Client Server
	
	@Override
	public void on(String source, String[] params)
	{
		net.rizon.acid.core.Server server = new net.rizon.acid.core.Server(params[0], AcidCore.me, params[2].replaceAll("\\(H\\)", ""), Integer.parseInt(params[1]), Pass.LinkSID);
		Acidictive.onServer(server);
	}
}