package net.rizon.acid.messages;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Message;

public class Ping extends Message
{
	public Ping()
	{
		super("PING");
	}
	
	@Override
	public void on(String source, String[] params)
	{
		AcidCore.sendln("PONG :" + params[0]);
	}
}