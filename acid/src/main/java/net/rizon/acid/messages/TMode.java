package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;

public class TMode extends Message
{
	public TMode()
	{
		super("TMODE");
	}
	
	// :4SSAAAAWB TMODE 1233718499 #a -sOb blah!*@*
	
	@Override
	public void on(String source, String[] params)
	{
		String setter = Server.toName(source);
		if (setter == source)
			setter = User.toName(source);
		if (setter == source)
		{
			AcidCore.log.log(Level.WARNING, "TMODE from nonexistent source " + source);
			return;
		}

		Channel chan = Channel.findChannel(params[1]);
		if (chan == null)
			return;
		
		int ts;
		try
		{
			ts = Integer.parseInt(params[0]);
		}
		catch (NumberFormatException ex)
		{
			ex.printStackTrace();
			return;
		}
		
		if (ts > chan.getTS())
			return;

		String modes = params[2];
		for (int i = 3; i < params.length; i++)
			modes += " " + params[i];

		Acidictive.onChanMode(setter, chan, modes);
		
		if (chan.size() == 0 && !chan.hasMode('z'))
			chan.destroy();
	}
}