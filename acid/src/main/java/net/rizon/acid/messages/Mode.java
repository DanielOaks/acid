package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Mode extends Message
{
	public Mode()
	{
		super("MODE");
	}
	
	// :99hAAAAAB MODE 99hAAAAAB :-y
	
	@Override
	public void on(String source, String[] params)
	{
		if (params[0].startsWith("#")) // This isn't possible I guess?
			return;

		User setter = User.findUser(source), target = User.findUser(params[0]);
		if (setter == null)
		{
			AcidCore.log.log(Level.WARNING, "MODE from nonexistent source " + source);
			return;
		}
		else if (target == null)
		{
			AcidCore.log.log(Level.WARNING, "MODE for nonexistent user " + params[0]);
			return;
		}

		target.setMode(params[1]);
		Acidictive.onMode(setter.getNick(), target.getNick(), params[1]);
	}
}