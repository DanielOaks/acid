package net.rizon.acid.messages;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;

public class Encap extends Message
{
	public Encap()
	{
		super("ENCAP");
	}
	
	@Override
	public void onServer(Server source, String[] params)
	{
		if (params[0].equals("*") == false && params[0].equalsIgnoreCase(AcidCore.me.getName()) == false && params[1].equalsIgnoreCase(AcidCore.me.getSID()) == false)
			;
		else if (params[1].equalsIgnoreCase("SVSMODE"))
		{
			// :services.rizon.net ENCAP * SVSMODE weed 1145782805 +rd :1145782805
			// :im.a.server ENCAP * SVSMODE dizzy 1145772712 :+r

			User target = User.findUser(params[2]);
			if (target == null)
				return;

			String modes = params[4];
			modes = modes.replaceAll("d", ""); // Guess we don't track service stamps?
			target.setMode(modes);
		}
		else if (params[1].equalsIgnoreCase("CHGHOST"))
		{
			// :services.rizon.net ENCAP * CHGHOST NocturneXDCC :is.a.yote.server

			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setVhost(params[3]);
		}
		else if (params[1].equals("SU"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			if (params.length > 3)
				user.setSU(params[3]);
			else
				user.setSU("");
		}
		else if (params[1].equals("CERTFP"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setCertFP(params[3]);
		}
		else if (params[1].equals("AUTHFLAGS"))
		{
			User user = User.findUser(params[2]);
			if (user == null)
				return;

			user.setAuthFlags(params[3]);
		}
	}
}