package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;

public class Error extends Message
{
	public Error()
	{
		super("ERROR");
	}
	
	@Override
	public void on(String source, String[] params)
	{
		Acidictive.onDie(params[0]);
	}
}