package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;

public class Kill extends Message
{
	public Kill()
	{
		super("KILL");
	}
	
	// :pdi KILL kenzie :the.hub!staff.rizon.net!pdi!pdi (testing)
	
	@Override
	public void on(String source, String[] params)
	{
		String killer = User.toName(source);
		if (killer == source)
			killer = Server.toName(source);

		User killed = User.findUser(params[0]);
		if (killed == null)
			return;

		killed.onQuit();
		Acidictive.onKill(killer, killed, params[1]);
	}
}