package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;
import net.rizon.acid.core.User;

public class SJoin extends Message
{
	public SJoin()
	{
		super("SJOIN");
	}
	
	// :4SS SJOIN 1233194735 #c +nt :@4SSAAAATV
	// :80C SJOIN 1233634716 #DontJoinItsATrap +npstz :80CAACM7J
	// :42C SJOIN 1233634716 #DontJoinItsATrap +mnpstz :
	
	private static String stripUser(String user)
	{
		user = user.replaceAll("\\@|\\%", "");
		user = user.replaceAll("\\+|\\!", "");
		user = user.replaceAll("\\&|\\~", "");
		return user;
	}
	
	private static String getModes(String x)
	{
		String modes = "";
		for (int i = 0; i < x.length(); i++)
		{
			if (x.substring(i, i + 1).matches("(?i)[a-zA-Z]"))
				break;
			else if (x.substring(i, i + 1).equals("~"))
				modes += "q";
			else if (x.substring(i, i + 1).equals("&"))
				modes += "a";
			else if (x.substring(i, i + 1).equals("@"))
				modes += "o";
			else if (x.substring(i, i + 1).equals("%"))
				modes += "h";
			else if (x.substring(i, i + 1).equals("+"))
				modes += "v";
		}
		return modes;
	}

	
	@Override
	public void onServer(Server source, String[] params)
	{
		String sts = params[0], channel = params[1], modes = params[2], nicks = params[params.length - 1];
		for (int i = 3; i < params.length - 1; ++i)
			modes += " " + params[i];
		
		int ts = Integer.parseInt(sts);

		boolean keep_their_modes = true;
		Channel chan = Channel.findChannel(channel);
		if (chan == null)
		{
			chan = new Channel(channel, ts);
			Acidictive.onChanMode(source.getName(), chan, modes);
		}
		else if (ts > chan.getTS())
			keep_their_modes = false;
		else if (ts < chan.getTS())
		{
			chan.reset(ts);
			Acidictive.onChanMode(source.getName(), chan, modes);
		}

		if (nicks.isEmpty())
			return;

		String[] u = nicks.split(" ");
		User[] users = new User[u.length];
		for (int i = 0; i < u.length; i++)
		{
			String stripped_user = stripUser(u[i]);
			users[i] = User.findUser(stripped_user);

			if (users[i] != null)
			{
				chan.addUser(users[i].getNick(), keep_their_modes ? getModes(u[i]) : "");
				users[i].addChan(chan.getName(), "" + ts); // wtf?
			}
			else
				AcidCore.log.log(Level.WARNING, "SJOIN for nonexistant user " + stripped_user + " from " + source.getName());
		}

		if (source.isBursting() == false)
			Acidictive.onJoin(channel, users);
	}
}