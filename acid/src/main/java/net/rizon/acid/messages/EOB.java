package net.rizon.acid.messages;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;

public class EOB extends Message
{
	public EOB()
	{
		super("EOB");
	}
	
	// :00C EOB
	
	@Override
	public void onServer(Server server, String[] params)
	{
		server.finishBurst();
		Acidictive.onEOB(server);

		if (server.getHub() == AcidCore.me)
			Acidictive.onSync();
	}
}