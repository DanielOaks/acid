package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;
import net.rizon.acid.util.Blowfish;

public class Privmsg extends Message
{
	public Privmsg()
	{
		super("PRIVMSG");
	}
	
	// :99hAAAAAB PRIVMSG moo :hi there
	
	@Override
	public void onUser(User sender, String[] params)
	{
		String target = params[0], msg = params[1];

		if (msg.startsWith("\1") && msg.endsWith("\1"))
			Acidictive.onCtcp(sender.getNick(), target, msg);
		else
		{
			String crypto_pass = Acidictive.conf.getCryptoPass(target);
			if (crypto_pass != null)
			{
				try
				{
					Blowfish bf = new Blowfish(crypto_pass);
					String dec = bf.Decrypt(msg);
					if (dec == null)
						return; // Don't process non-encrypted messages sent to targets expecting encrypted messages

					msg = dec;
					AcidCore.log.log(Level.FINE, "Blowfish decrypted: " + msg);
				}
				catch (Exception e)
				{
					AcidCore.log.log(Level.WARNING, "Unable to blowfish decrypt: " + msg, e);
					return; // Don't process non-encrypted messages sent to targets expecting encrypted messages
				}
			}

			Acidictive.onPrivmsg(sender.getNick(), params[0], msg);
		}
	}
}