package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Operwall extends Message
{
	public Operwall()
	{
		super("OPERWALL");
	}
	
	// :OperServ OPERWALL :USERS: py-ctcp!ctcp@ctcp-scanner.rizon.net is now an IRC operator.
	
	@Override
	public void onUser(User u, String[] params)
	{
	}
}