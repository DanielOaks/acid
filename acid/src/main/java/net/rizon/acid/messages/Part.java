package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Part extends Message
{
	public Part()
	{
		super("PART");
	}
	
	// :99hAAAAAB PART #geo
	
	@Override
	public void onUser(User u, String[] params)
	{
		Channel chan = Channel.findChannel(params[0]);
		if (chan == null)
		{
			AcidCore.log.log(Level.WARNING, "PART from " + u.getNick() + " for nonexistent channel " + params[0]);
			return;
		}

		chan.removeUser(u.getNick());
		u.remChan(chan.getName());

		Acidictive.onPart(u.getNick(), chan.getName());
	}
}