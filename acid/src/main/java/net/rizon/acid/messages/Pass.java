package net.rizon.acid.messages;

import net.rizon.acid.core.Message;

public class Pass extends Message
{
	public static String LinkSID;
	
	public Pass()
	{
		super("PASS");
	}
	
	@Override
	public void on(String source, String[] params)
	{
		LinkSID = params[3];
	}
}