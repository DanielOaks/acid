package net.rizon.acid.messages;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Server;

public class SQuit extends Message
{
	public SQuit()
	{
		super("SQUIT");
	}
	
	// SQUIT test.geodns.org :irc.geodns.org
	// SQUIT 98C :jupe over
	
	@Override
	public void on(String source, String[] params)
	{
		Server x = Server.findServer(params[0]);
		if (x == null)
		{
			AcidCore.log.log(Level.WARNING, "SQUIT for nonexistent server " + params[0]);
			return;
		}

		Acidictive.onSquit(x);
		x.onQuit();
	}
}