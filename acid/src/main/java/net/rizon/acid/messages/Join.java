package net.rizon.acid.messages;

import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Join extends Message
{
	public Join()
	{
		super("JOIN");
	}
	
	// :4SSAAAATV JOIN 1218474093 #oper +
	
	@Override
	public void onUser(User u, String[] params)
	{
		String ts = params[0], channel = params[1]/*, modes = params[2]*/ ;

		Channel chan = Channel.findChannel(channel);
		if (chan == null)
			chan = new Channel(channel, Integer.parseInt(ts));

		chan.addUser(u.getNick(), "");
		u.addChan(chan.getName(), ts);

		User[] user = new User[1];
		user[0] = u;
		Acidictive.onJoin(channel, user);
	}
}