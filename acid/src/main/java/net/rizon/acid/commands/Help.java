package net.rizon.acid.commands;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.User;

public class Help extends Command
{
	public Help()
	{
		super(0, 1);
	}
	
	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		Acidictive.reply(x, to, c, Message.BOLD + "--== " + AcidCore.me.getName() + " Help ==--" + Message.BOLD);
		Acidictive.reply(x, to, c, "Running " + Message.BOLD + Acidictive.getVersion() + Message.BOLD);
		Acidictive.reply(x, to, c, "Uptime: " + Message.BOLD + Acidictive.getUptime() + Message.BOLD);
		if (x.getIdentNick().isEmpty())
			Acidictive.reply(x, to, c, "You are not logged in.");
		else
			Acidictive.reply(x, to, c, "You are logged in with flags +" + Message.BOLD + x.getFlags() + Message.BOLD);
		Acidictive.reply(x, to, c, "All commands begin with " + Message.BOLD + "/msg " + to.getNick() + Message.BOLD);
		Acidictive.reply(x, to, c, "Key: [] indicates an optional field. <> indicates a type " +
				"of user input, otherwise it is as stated.");
			
		int total = 0;
		for (net.rizon.acid.conf.Command confCmd : to.getConfCommands())
		{
			Command cmd;
			try
			{
				ClassLoader loader = to.pkg != null ? to.pkg.loader : Acidictive.loader;
				cmd = (Command) loader.loadClass(confCmd.clazz).newInstance();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();//XXX
				continue;
			}
			
			if (confCmd.privilege == null || !x.hasFlags(confCmd.privilege))
				continue;
				
			++total;
			
			String cmdName = confCmd.name;
			String notice = "\2--== " + cmdName.toUpperCase() + " [" + confCmd.privilege + "] ==--\2";
				
			if (args.length == 0)
			{
				Acidictive.reply(x, to, c, notice);
				cmd.onHelp(x, to, c);
				continue;
			}
				
			if (cmdName.equalsIgnoreCase(args[0]))
			{
				Acidictive.reply(x, to, c, notice);
				// Help MUST return false if and only if no help is available
				if (!cmd.onHelpCommand(x, to, c))
				{
					Acidictive.reply(x, to, c, "No help available for " +
							Message.BOLD + cmdName.toLowerCase() + Message.BOLD + ".");
					return;
				}

				Acidictive.reply(x, to, c, " ");
				Acidictive.reply(x, to, c, "End of help for " +
						Message.BOLD +cmdName.toLowerCase() + Message.BOLD + ".");
				return;
			}
		}
			
		// If we have an argument and are STILL here, bail.
		if (args.length > 0)
		{
			Acidictive.reply(x, to, c, "Command " + Message.BOLD + args[0] + Message.BOLD + " not found.");
			return;
		}
			
		if (total > 0)
			Acidictive.reply(x, to, c, "End of help. [" + Message.BOLD + total + Message.BOLD + " commands displayed]");
		else
			Acidictive.reply(x, to, c, "You do not have access to any commands.");
	}
	
	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2help [command]\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "This command allows you to retrieve help on other commands. If called");
		Acidictive.reply(u, to, c, "without a command to give help for, an overview of commands that you have");
		Acidictive.reply(u, to, c, "access to use is printed. The character to the right of a command (e.g., ");
		Acidictive.reply(u, to, c, "z in \"--== GLOBAL [z] ==--\") tells you what the flag requirement for the");
		Acidictive.reply(u, to, c, "command is; if the flag requirement character is *, any oper who may or");
		Acidictive.reply(u, to, c, "may not have any access at all can use the command.");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "If a command is given, more verbose information about it will be shown. Note");
		Acidictive.reply(u, to, c, "that only commands that are not bound to a channel have help entries.");
		
		return true;
	}
}
