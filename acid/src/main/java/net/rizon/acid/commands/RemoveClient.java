package net.rizon.acid.commands;

import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;

public class RemoveClient extends Command
{
	public RemoveClient()
	{
		super(1, 2);
	}

	@Override
	public void Run(User source, AcidUser to, Channel c, String[] args)
	{
		User target = User.findUser(args[0]);
		if (target == null || !(target instanceof AcidUser) || target == to)
			return;
		
		String reason = args.length > 1 ? args[1] : "";
		((AcidUser) target).quit(reason);
	}
	
	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2removeclient <nick> [reason]\2");
	}
	
	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2removeclient <nick> <reason>\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "This command allows forcibly removing a pseudoclient.");
		Acidictive.reply(u, to, c, "Please exercise caution when using this command; you may");
		Acidictive.reply(u, to, c, "possibly break internal state in a way that requires a");
		Acidictive.reply(u, to, c, "plugin reload.");
		return true;
	}
}
