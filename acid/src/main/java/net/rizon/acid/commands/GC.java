package net.rizon.acid.commands;

import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;

/**
 * Run the garbage collector.
 */
public class GC extends Command
{
	public GC()
	{
		super(0, 0);
	}

	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		System.gc();
		Acidictive.reply(x, to, c, "Ran gc().");
	}
	
	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2gc\2 / Runs the garbage collector");
	}
	
	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2gc\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "This command calls the Java System.gc() function. This will force running");
		Acidictive.reply(u, to, c, "the garbage collector and usually free up some memory.");
		
		return true;
	}
}
