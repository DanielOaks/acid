package net.rizon.acid.commands;

import net.rizon.acid.conf.Config;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Event;
import net.rizon.acid.core.Plugin;
import net.rizon.acid.core.User;
import net.rizon.acid.util.ClassLoader;

/**
 * Reloads the configuration files.
 */
public class Reload extends Command
{
	public Reload()
	{
		super(0, 1);
	}
	
	@Override
	public void Run(User x, AcidUser to, Channel ch, final String[] args)
	{	
		try
		{
			if (args.length > 0)
			{
				Plugin p = Plugin.findPlugin(args[0]);
				if (p == null)
					return;
				p.reload();
				return;
			}
			
			Config c = (Config) Config.load("acidictive.yml", Config.class);
			c.validate();
			Acidictive.conf.rehash(c);
			
			Acidictive.conf = c;
			Acidictive.loadClients(null, c.clients);
			Acidictive.loader = new ClassLoader(Acidictive.loaderBase);

			try
			{
				for (Event e : Event.getEvents())
					e.onRehash();
			}
			catch (Exception e)
			{
				Acidictive.reply(x, to, ch, "Error reloading configuration from an event, non fatal: " + e.getMessage());
			}
		}
		catch (Exception e)
		{
			Acidictive.reply(x, to, ch,
					"Error reloading configuration: " + e.getMessage());
			return;
		}
		
		Acidictive.reply(x, to, ch, "Reloaded configuration");
	}
	
	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2reload\2 / Reloads the configuration");
	}
	
	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2reload\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "This command reloads the configuration files.");
		Acidictive.reply(u, to, c, "The reload command is supposed to be a non-destructive operation; if");
		Acidictive.reply(u, to, c, "you are unsure if a reload would actually affect a setting that was");
		Acidictive.reply(u, to, c, "changed, it is always safe to try it out.");
		return true;
	}
}
