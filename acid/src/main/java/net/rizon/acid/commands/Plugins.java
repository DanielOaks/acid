package net.rizon.acid.commands;

import java.util.logging.Level;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.Plugin;
import net.rizon.acid.core.User;

public class Plugins extends Command
{
	public Plugins()
	{
		super(1, 2);
	}

	@Override
	public void Run(User u, AcidUser to, Channel c, String[] args)
	{
		if (args[0].equalsIgnoreCase("LIST"))
		{
			int i = 0;
			for (Plugin p : Plugin.getPlugins())
			{
				++i;
				Acidictive.reply(u, to, c, p.getName());
			}
			Acidictive.reply(u, to, c, i + " plugins loaded.");
		}
		else if (args[0].equalsIgnoreCase("LOAD") && args.length == 2)
		{
			Plugin p = Plugin.findPlugin(args[1]);
			if (p != null)
			{
				Acidictive.reply(u, to, c, p.getName() + " is already loaded.");
				return;
			}
			
			try
			{
				p = Plugin.loadPlugin(args[1]);
				Acidictive.reply(u, to, c, "Plugin " + p.getName() + " successfully loaded");
				AcidCore.log.log(Level.INFO, "PLUGINS LOAD for " + p.getName() + " from " + u.getNick());
			}
			catch (Exception ex)
			{
				AcidCore.log.log(Level.WARNING, "Unable to load plugin " + args[1], ex);
				Acidictive.reply(u, to, c, "Unable to load plugin " + args[1] + ": " + ex.getMessage());
			}	
		}
		else if (args[0].equalsIgnoreCase("UNLOAD") && args.length == 2)
		{
			Plugin p = Plugin.findPlugin(args[1]);
			if (p == null)
			{
				Acidictive.reply(u, to, c, args[1] + " is not loaded.");
				return;
			}

			if (p.isPermanent())
			{
				Acidictive.reply(u, to, c, p.getName() + " is permanent and can not be unloaded.");
				return;
			}
			
			p.remove();
			
			Acidictive.reply(u, to, c, "Unloaded plugin " + p.getName());
			AcidCore.log.log(Level.INFO, "PLUGINS UNLOAD for " + p.getName() + " from " + u.getNick());
		}
		else if (args[0].equalsIgnoreCase("RELOAD") && args.length == 2)
		{
			Plugin p = Plugin.findPlugin(args[1]);
			if (p == null)
			{
				Acidictive.reply(u, to, c, args[1] + " is not loaded.");
				return;
			}

			if (p.isPermanent())
			{
				Acidictive.reply(u, to, c, p.getName() + " is permanent and can not be unloaded.");
				return;
			}
			
			p.remove();
			
			try
			{
				p = Plugin.loadPlugin(args[1]);
				Acidictive.reply(u, to, c, "Plugin " + p.getName() + " successfully reloaded");
				AcidCore.log.log(Level.INFO, "PLUGINS RELOAD for " + p.getName() + " from " + u.getNick());
			}
			catch (Exception ex)
			{
				AcidCore.log.log(Level.WARNING, "Unable to reload plugin " + args[1], ex);
				Acidictive.reply(u, to, c, "Unable to reload plugin " + args[1] + ": " + ex.getMessage());
			}	
		}
		else
			Acidictive.reply(u, to, c, "Use LOAD, UNLOAD, RELOAD, or LIST");
	}
	
	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2plugin list\2 / Lists currently loaded plugins");
		Acidictive.reply(u, to, c, "\2plugin load <plugin>\2 / Loads a plugin");
		Acidictive.reply(u, to, c, "\2plugin reload <plugin>\2 / Reloads a plugin");
		Acidictive.reply(u, to, c, "\2plugin unload <plugin>\2 / Unloads a plugin");
	}
	
	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2shutdown\2");
		Acidictive.reply(u, to, c, "        \2plugin list\2 / Lists currently loaded plugins");
		Acidictive.reply(u, to, c, "        \2plugin load <plugin>\2 / Loads a plugin");
		Acidictive.reply(u, to, c, "        \2plugin reload <plugin>\2 / Reloads a plugin");
		Acidictive.reply(u, to, c, "        \2plugin unload <plugin>\2 / Unloads a plugin");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "The plugin command allows managing plugins. Note that plugins");
		Acidictive.reply(u, to, c, "may contain some lower unit of management (such as scripts in");
		Acidictive.reply(u, to, c, "the case of pyva), which cannot be directly manipulated using");
		Acidictive.reply(u, to, c, "the plugin command.");
		return true;
	}
}