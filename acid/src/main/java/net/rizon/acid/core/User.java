package net.rizon.acid.core;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

import net.rizon.acid.conf.AccessPreset;

public class User implements Comparable<User>
{
	private static final Logger log = Logger.getLogger(User.class.getName());
	private String nick, user, host, vhost, name, identnick, modes, UID, ip,
			certfp, authflags, su;
	private int nickTS, conTS;
	private Server server;
	private String flags; // Access flags
	private ArrayList<String> oldNicks;
	private Hashtable<String, ArrayList<Integer>> chans;
	private Hashtable<String, String> chanList;

	public User(String nick, String user, String host, String vhost, String name,
			Server server, int nickTS, int conTS, String modes, String UID, String ip)
	{
		this.nick = nick;
		this.user = user;
		this.host = host;
		this.vhost = vhost;
		this.host = host;
		this.name = name;
		this.server = server;
		this.identnick = "";
		this.nickTS = nickTS;
		this.conTS = conTS;
		this.modes = modes;
		this.UID = UID;
		this.ip = ip;
		this.flags = "";
		this.certfp = "";
		this.authflags = "";
		this.su = "";
		oldNicks = new ArrayList<String>();
		chans = new Hashtable<String, ArrayList<Integer>>();
		chanList = new Hashtable<String, String>();

		uidMap.put(UID, this);
		nickMap.put(nick.toLowerCase(), this);

		this.getServer().incUsers();
	}

	public void onQuit()
	{
		for (Iterator<String> it = this.getChannels().iterator(); it.hasNext();)
		{
			Channel chan = Channel.findChannel(it.next());
			if (chan == null)
				continue;

			chan.removeUser(this.getNick());
			it.remove();
		}

		this.getServer().decUsers();
		if (!(this instanceof AcidUser))
			UserList.decreaseHost(this.ip);
		
		uidMap.remove(UID);
		nickMap.remove(nick.toLowerCase());
	}

	public void flush()
	{
		oldNicks.clear();
		chans.clear();
	}

	public void clearFlood()
	{
		if (chans != null)
			chans.clear();
	}

	public ArrayList<Integer> addChanJoinPart(String chan, int TS)
	{
		ArrayList<Integer> x = chans.get(chan.toLowerCase());
		if (x == null)
		{
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			tmp.add(new Integer(TS));
			chans.put(chan.toLowerCase(), tmp);
			return tmp;
		}
		else
		{
			x.add(new Integer(TS));
			if (x.size() > 4)
				x.remove(0);
			return x;
		}

	}

	public boolean isOnChan(String chan)
	{
		return chanList.get(chan.toLowerCase()) != null;
	}

	public void addChan(String chan, String TS)
	{
		if (chan != null && !chan.equals(""))
		{
			if (chanList.get(chan.toLowerCase()) == null)
				chanList.put(chan.toLowerCase(), TS);
		}
	}

	public void remChan(String chan)
	{
		if (chan != null && !chan.equals(""))
		{
			chanList.remove(chan.toLowerCase());
		}
	}

	public Set<String> getChannels()
	{
		return this.chanList.keySet();
	}

	public String getChanStr()
	{
		String s = "none";

		for (Iterator<String> it = chanList.keySet().iterator(); it.hasNext();)
		{
			Channel chan = Channel.findChannel(it.next());

			String temp = chan.getModes(this.getNick()) + chan.getName();
			if (s.equals("none"))
				s = temp;
			else
				s += " " + temp;
		}

		return s;
	}

	public void setFlags(String flags)
	{
		this.flags = flags;
	}

	public void setUID(String uid)
	{
		this.UID = uid;
	}

	public void changeNick(String newNick)
	{
		String oldnick = this.getNick();

		nickMap.remove(this.getNick().toLowerCase());
		oldNicks.add(this.getNick());
		if (oldNicks.size() > 20)
			oldNicks.remove(0);
		this.nick = newNick;
		nickMap.put(this.getNick().toLowerCase(), this);

		this.setMode("-r");

		for (Iterator<String> it = this.getChannels().iterator(); it.hasNext();)
		{
			Channel chan = Channel.findChannel(it.next());
			if (chan == null)
				continue;

			chan.onNick(oldnick, this.getNick());
		}
	}

	// accessor methods
	public String getIdentNick()
	{
		return identnick;
	}

	public String getIP()
	{
		return ip;
	}

	public String getNick()
	{
		return nick;
	}

	public boolean hasMode(String mode)
	{
		if (modes.indexOf(mode) != -1)
			return true;
		return false;
	}

	public void setMode(String mode)
	{
		if (mode.equals("") || mode == null)
			return;
		
		String old = modes;
		
		boolean plus = true;
		String chr;
		for (int x = 0; x < mode.length(); x++)
		{
			chr = mode.substring(x, x + 1);
			if (chr.equals("+"))
			{
				plus = true;
			}
			else if (chr.equals("-"))
			{
				plus = false;
			}
			else if (plus && modes.indexOf(chr) == -1)
			{
				modes += chr;
			}
			else if (!plus && modes.indexOf(chr) >= 0)
			{
				if (chr.equals("x"))
					this.vhost = this.host;
				modes = modes.replaceAll(chr, "");
			}
		}
		
		for (Event e : Event.getEvents())
			e.onUserMode(this, old, modes);
	}

	public String getUser()
	{
		return user;
	}

	public String getUID()
	{
		return UID;
	}

	public String getHost()
	{
		return host;
	}

	public String getVhost()
	{
		return vhost;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}

	public String getRealName()
	{
		return name;
	}

	public int getNickTS()
	{
		return nickTS;
	}

	public int getConTS()
	{
		return conTS;
	}

	public void changeNickTS(int nickTS)
	{
		this.nickTS = nickTS;
	}

	public Server getServer()
	{
		return server;
	}

	/* Does this user have all of these flags? Can also take a type (SRA/SA/SO/whatever) */
	public boolean hasFlags(String flags)
	{
		if (flags.isEmpty())
			return false;
		
		String my_flags = this.flags;
		
		for (AccessPreset p : Acidictive.conf.access_preset)
		{
			for (String s : p.name)
			{
				my_flags = my_flags.replace(s, p.privileges);
				flags = flags.replace(s, p.privileges);
			}
		}
		
		for (int i = 0; i < flags.length(); ++i)
			if (my_flags.indexOf(flags.charAt(i)) == -1)
				return false;
		
		return true;
	}

	public String getFlags()
	{
		return this.flags;
	}

	public String getModes()
	{
		return modes;
	}
	
	public String getCertFP()
	{
		return certfp;
	}
	
	public void setCertFP(String certfp)
	{
		this.certfp = certfp;
	}
	
	public String getAuthFlags()
	{
		return authflags;
	}
	
	public void setAuthFlags(String authflags)
	{
		this.authflags = authflags;
	}

	public String getSU()
	{
		return su;
	}
	
	public void setSU(String su)
	{
		this.su = su;
	}
	
	@Override
	public int compareTo(User user)
	{
		return nick.compareTo(user.getNick());
	}

	@Override
	public String toString()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + "(" + name + ")[" + server + "] / " + oldNicksList();
	}

	public String getNickString()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + vhost + "(" + name + ")";
	}

	public String getSNString()
	{
		return nick + "!" + user + "@" + host;
	}

	public String getNString()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + vhost + "(" + name + ")(" + modes + ")[" + server + "]";
	}

	public String getNStringON()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + vhost + "(" + name + ")(" + modes + ")[" + server + "] / " + oldNicksList();
	}

	public String getNickLine()
	{
		return getNick() + "!" + getUser() + "@" + getHost() + "/" + getRealName();
	}

	public ArrayList<String> oldNicks()
	{
		return oldNicks == null ? new ArrayList<String>() : oldNicks;
	}

	public String oldNicksList()
	{
		if (oldNicks == null)
			return "No nick changes.";
		if (oldNicks.size() > 0)
		{
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < oldNicks.size(); i++)
			{
				sb.append(oldNicks.get(i) + " -> ");
			}
			sb.append(nick);
			return sb.toString();
		}
		return "No nick changes.";
	}

	// TODO: Fixme to make this either properly static or a "real" method
	// XXX ??????????????????????????????????? wtf is this
	public void loadAccess(final User user)
	{
		try
		{
			PreparedStatement ps = Acidictive.acidcore_sql.prepare("SELECT `user`, `flags` FROM `access` WHERE `user` = ?");
			ps.setString(1, user.getSU());
			ResultSet rs = Acidictive.acidcore_sql.executeQuery(ps);
			if (rs.next())
			{
				String oldflags = this.flags;

				this.flags = rs.getString("flags");
				this.identnick = user.getSU();

				if (oldflags.equals(this.flags) == false)
					Acidictive.notice(this, "You have been logged in with flags " + this.flags);
			}
		}
		catch (SQLException ex)
		{
			log.log(Level.WARNING, "Unable to load access for user", ex);
		}
	}

	private static HashMap<String, User> uidMap = new HashMap<String, User>();
	private static HashMap<String, User> nickMap = new HashMap<String, User>();

	public static User findUser(final String nick)
	{
		if (nick != null && nick.isEmpty() == false && Character.isDigit(nick.charAt(0)))
			return uidMap.get(nick);
		return nickMap.get(nick.toLowerCase());
	}

	public static final Set<String> getUsers()
	{
		return nickMap.keySet();
	}

	public static Collection<User> getUsersC() { return nickMap.values(); }

	public static final String toName(final String uid)
	{
		User u = findUser(uid);
		if (u != null)
			return u.getNick();
		return uid;
	}

	public static int userCount()
	{
		return nickMap.size();
	}
	
	private static char[] currentUid = new char[] { 'A', 'A', 'A', 'A', 'A', '@' };
	
	public static String generateUID()
	{
		for (int i = 5; i >= 0; --i)
		{
			if (currentUid[i] == 'Z')
			{
				currentUid[i] = '0';
				break;
			}
			else if (currentUid[i] != '9')
			{
				currentUid[i]++;
				break;
			}
			else
				currentUid[i] = 'A';
		}
		
		return AcidCore.me.getSID() + new String(currentUid);
	}
}
