package net.rizon.acid.core;

import java.util.HashMap;
import java.util.logging.Level;

public abstract class Message
{
	public static final char BOLD = 2;
	
	private static final String messageBase = "net.rizon.acid.messages";
	private static final String[] messageClasses = {
		"Encap", "EOB", "Error", "Join", "Kick", "Kill", "Mode", "Nick",
		"Notice", "Part", "Pass", "Ping", "Privmsg", "Quit", "Server", "SID",
		"SJoin", "SQuit", "TMode", "UID", "Whois", "Operwall"
	};
	
	static
	{
		messages = new HashMap<String, Message>();
		
		try
		{
			for (String s : messageClasses)
				Class.forName(messageBase + "." + s).newInstance();
		}
		catch (Exception ex)
		{
			AcidCore.log.log(Level.SEVERE, "Error initializing messages", ex);
			System.exit(-1);
		}
	}
	
	protected Message(String name)
	{	
		messages.put(name.toUpperCase(), this);
	}
	
	public void onUser(User source, String[] params) { }
	public void onServer(Server source, String[] params) { }
	public void on(String source, String[] params) { }
	
	private static HashMap<String, Message> messages;
	
	public static Message findMessage(String name)
	{
		return messages.get(name.toUpperCase());
	}
}