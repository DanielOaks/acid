package net.rizon.acid.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import net.rizon.acid.conf.Client;
import net.rizon.acid.conf.Config;
import net.rizon.acid.sql.SQL;
import net.rizon.acid.util.Blowfish;
import net.rizon.acid.util.ClassLoader;
import net.rizon.acid.util.Util;

public class Acidictive extends AcidCore
{
	private static final Logger log = Logger.getLogger(Acidictive.class.getName());
	public static Config conf;
	public static long startTime;
	public static int syncTime;
	public static SQL acidcore_sql;
	public static String bold = "";
	
	public static String loaderBase = "commands";
	public static ClassLoader loader; // loader for the core commands
	
	public static String getVersion()
	{
		return "acid4 " + Version.getAttribute("revision");
	}

	public static void main(String[] args)
	{
		Version.load();

		log.log(Level.INFO, getVersion() + " (" + Version.getFullVersion() + ") starting up...");

		try
		{
			conf = (Config) Config.load("acidictive.yml", Config.class);
		}
		catch (Exception ex)
		{
			log.log(Level.CONFIG, "Unable to start due to configuration problems", ex);
			System.exit(1);
		}

		AcidCore.start(conf.uplink.host, conf.uplink.port, conf.serverinfo.name, conf.serverinfo.description, conf.uplink.pass, conf.serverinfo.id, conf.uplink.ssl);
		
		acidcore_sql = SQL.getConnection("acidcore");
		if (acidcore_sql == null)
		{
			log.log(Level.SEVERE, "Unable to get connection for `acidcore`");
			System.exit(-1);
		}
		
		loader = new ClassLoader(loaderBase);
		if (conf.clients != null)
			for (Client c : conf.clients)
				new AcidUser(null, c);

		if (conf.plugins != null)
			for (String s : conf.plugins)
			{
				try
				{
					Plugin p = Plugin.loadPlugin(s);
					log.log(Level.INFO, "Loaded plugin " + p.getName());
				}
				catch (Exception ex)
				{
					log.log(Level.SEVERE, "Unable to load plugin " + s, ex);
					System.exit(-1);
				}
			}

		try
		{
			AcidCore.run();
		}
		catch (UnknownHostException ex)
		{
			log.log(Level.SEVERE, "Unable to connect to uplink - unknown host");
			System.exit(-1);
		}
		catch (IOException ex)
		{
			log.log(Level.SEVERE, "Unable to connect to uplink", ex);
			System.exit(-1);
		}

		System.exit(0);
	}

	public static void onStart()
	{
		startTime = new Date().getTime();

		for (String s : User.getUsers())
		{
			User u = User.findUser(s);
			if (!(u instanceof AcidUser))
				continue;
			
			((AcidUser) u).introduce();
		}

		Protocol.eob();
	}

	public static void onNick(User user)
	{
		for (Event e : Event.getEvents())
			e.onUserConnect(user);
	}

	public static void onNickChange(User user, String oldnick)
	{
		for (Event e : Event.getEvents())
			e.onNickChange(user, oldnick);
	}

	public static void onEOB(Server server)
	{
		for (Event e : Event.getEvents())
			e.onEOB(server);
	}

	public static void onJoin(String channel, User[] users)
	{
		for (Event e : Event.getEvents())
			e.onJoin(channel, users);
	}

	public static void onPart(String parter, String channel)
	{
		for (Event e : Event.getEvents())
			e.onPart(parter, channel);
	}

	public static void onQuit(User user, String msg)
	{
		for (Event e : Event.getEvents())
			e.onQuit(user, msg);
	}

	public static void onServer(Server server)
	{
		for (Event e : Event.getEvents())
			e.onServerLink(server);
	}

	public static void onPrivmsg(String creator, String recipient, String msg)
	{
		try
		{
			User x = User.findUser(creator);

			boolean stop = false;
			for (Event e : Event.getEvents())
				stop = stop || e.onPrivmsg(creator, recipient, msg);
			if (stop)
				return;

			// All channel commands start with .
			if (recipient.startsWith("#") && !msg.startsWith(conf.general.command_prefix))
				return;

			if (x.getIdentNick().isEmpty())
			{
				// Only lookup flags for people who are +o and +r
				if (x.hasMode("o"))
				{
					// However +o and -r can do some commands (identify!)
					if (!x.getSU().isEmpty())
					{
						x.loadAccess(x);

						// Try to save a certfp if possible
						//if (!x.getFlags().isEmpty())
						//	Access.addCertFP(x, true);
					}
					// If +o-r, try to auth via cert; in case services are gone
					else if (!x.getCertFP().isEmpty())
					{
						// There might also be a CertFP for said user to use, check!
						try
						{
							PreparedStatement ps = Acidictive.acidcore_sql.prepare("SELECT `certfp` FROM `access` WHERE `user` = ?");
							ps.setString(1, x.getSU());
							ResultSet rs = Acidictive.acidcore_sql.executeQuery(ps);
							if (rs.next() && !rs.getString("certfp").isEmpty())
							{
								if (rs.getString("certfp").equalsIgnoreCase(x.getCertFP()))
								{
									x.loadAccess(x);
								}
								else
								{
									for (Event e : Event.getEvents())
										e.onCommandCertFPMismatch(x, rs.getString("certfp"));
								}
							}
						}
						catch (SQLException ex)
						{
							log.log(Level.WARNING, "Unable to load certfp access", ex);
						}
					}
				}
				else
					return;
			}

			// Not destined for anything else, must be a command.
			String[] parts = msg.split("\\s+");
			// strip leading "." if needed
			String command = (parts[0].startsWith(conf.general.command_prefix) ? parts[0].substring(conf.general.command_prefix.length()) : parts[0]).toLowerCase();
			String[] args = Arrays.copyOfRange(parts, 1, parts.length);
			
			Channel c = null;
			AcidUser to = null;
			net.rizon.acid.conf.Command confCommand = null;
			if (recipient.startsWith("#"))
			{
				c = Channel.findChannel(recipient);
				if (c == null)
					return;
				
				// lol?
				for (String s : c.getUsers())
				{
					User u = User.findUser(s);
					if (u instanceof AcidUser)
					{
						AcidUser t = (AcidUser) u;
						net.rizon.acid.conf.Command cmd = t.findConfCommand(command);
						
						if (cmd != null)
						{
							to = t;
							confCommand = cmd;
						}
					}
				}
				
				if (to == null)
					return;
			}
			else
			{
				int i = recipient.indexOf('@');
				if (i != -1)
					recipient = recipient.substring(0, i);

				User targ = User.findUser(recipient);
				if (targ == null || !(targ instanceof AcidUser))
					return;
				
				to = (AcidUser) targ;
				confCommand = to.findConfCommand(command);
			}
			
			if (confCommand == null)
				return;

			// This isn't really necessary, but is faster than causing the core loader to fail and then fall back
			// by searching the plugin loaders.
			Plugin p = to.pkg;
			// Use our loader if there is no plugin
			ClassLoader cl = p != null ? p.loader : loader; 
			Class<?> commandClass = cl.loadClass(confCommand.clazz);
			if (commandClass == null)
				return;
			
			Command cmd = (Command) commandClass.newInstance();

			if (recipient.startsWith("#"))
			{
				// In the proper channels commands can go without access checks. (ish)
				if (!confCommand.allowsChannel(recipient))
				{
					return; // Access
				}
					
				// Now that we're in the right channel check for just being logged in OR +o OR spoof
				if (x.getIdentNick().isEmpty() && !x.hasMode("o") && !x.getIP().equals("0"))
				{
					return; // Access
				}
			}
			// Message to me, check priv
			else
			{
				// Non priv commands can be used by any oper
				if (confCommand.privilege == null)
				{
					log.log(Level.INFO, "Denied access to " + confCommand.name + " to " + x + " [command has no privilege]");
					// Commands with no priv can not be executed in PM
					return;
				}
					
				// Explicitly requires no privilege
				if (confCommand.privilege.equals("none"))
					;
				else if (x.hasFlags(confCommand.privilege) == false)
				{
					log.log(Level.INFO, "Denied access to " + confCommand.name + " to " + x + " [user has no priv]");
					return;
				}
			}

			if (args.length < cmd.GetMinArgs())
			{
				reply(x, to, c, "Syntax error for \2" +
						confCommand.name + "\2. Available commands are:");
				cmd.onHelp(x, to, c);
				return; // Syntax
			}

			if (args.length > cmd.GetMaxArgs())
			{
				String last = arrayFormat(args, cmd.GetMaxArgs() - 1, args.length - 1);
					String[] parsed_args = new String[cmd.GetMaxArgs()];
				for (int i = 0; i < cmd.GetMaxArgs() - 1; ++i)
					parsed_args[i] = args[i];
				parsed_args[cmd.GetMaxArgs() - 1] = last;
				args = parsed_args;
			}

			try
			{
				cmd.Run(x, to, c, args);
				
				/* log SUCCESSFUL privmsg command usages as well
				 * except help because that's just silly and nobody cares.
				 */
				if (!command.equalsIgnoreCase("help")
						&& !recipient.startsWith("#"))
					privmsg(conf.getChannelNamed("cmdlogchan"), x.getNick() + ": " + msg);
			}
			catch (Exception ex)
			{
				log.log(Level.WARNING, "Error running command " + confCommand.name, ex);
			}
		}
		catch (Exception e)
		{
			log.log(Level.SEVERE, "Error processing PRIVMSG: " + msg, e);
		}
	}

	public static void onCtcp(String creator, String recipient, String msg)
	{
		boolean stop = false;
		
		if (msg.startsWith("\1VERSION"))
		{
			User u = User.findUser(recipient);
			if (u instanceof AcidUser)
			{
				AcidUser au = (AcidUser) u;
				if (au.client.version != null)
					Acidictive.notice(au.getUID(), creator, "\1VERSION " + au.client.version + "\1");
			}
		}
		
		for (Event e : Event.getEvents())
		{
			stop = stop || e.onCTCP(creator, recipient, msg);
			if (stop)
				return;
		}
	}

	public static void onCtcpReply(User source, String target, String message)
	{
		for (Event e : Event.getEvents())
			e.onCTCPReply(source, target, message);
	}

	public static void onNotice(String creator, String recipient, String msg)
	{
		if (creator.equalsIgnoreCase("nickserv") && msg.matches("^.*registered\\s+and\\s+protected.*$"))
		{
			AcidUser au = (AcidUser) User.findUser(recipient);
			if (au != null && !au.getNSPass().isEmpty())
				Protocol.privmsg(recipient, creator, "IDENTIFY " + au.getNSPass());
			return;
		}

		boolean stop = false;
		for (Event e : Event.getEvents())
		{
			stop = stop || e.onNotice(creator, recipient, msg);
			if (stop)
				return;
		}
	}

	public static void onDie(String msg)
	{
		log.log(Level.INFO, msg);
	}

	public static void onSync()
	{
		syncTime = (int) new Date().getTime() - (int) startTime;
		log.log(Level.INFO, "Synced in " + (double) syncTime / 1000 + " seconds.");

		for (Event e : Event.getEvents())
			e.onSync();
	}

	public static void onKill(final String killer, User user, final String reason)
	{
		for (Event e : Event.getEvents())
			e.onKill(killer, user, reason);
	}

	public static int getUptimeTS()
	{
		return getTS() - (int) ((double) startTime / 1000);
	}

	public static String getUptime()
	{
		return Util.formatTime(getUptimeTS());
	}

	public static void onKick(String kicker, User victim, String channel, String reason)
	{
		for (Event e : Event.getEvents())
			e.onKick(kicker, victim, channel, reason);

		if (victim.getServer() == AcidCore.me)
		{
			AcidUser au = (AcidUser) victim;
			au.joinChan(channel);
		}
	}

	public static void onMode(String creator, String recipient, String modes)
	{
	}

	public static void onChanMode(String creator, Channel chan, String modes)
	{
		String[] x = modes.split("\\s+");
		if (x.length >= 1)
		{
			boolean give = true;
			int whatnick = 1;
			String m;
			for (int i = 0; i < x[0].length(); i++)
			{
				m = x[0].substring(i, i + 1);
				
				if (m.equals("+"))
					give = true;
				else if (m.equals("-"))
					give = false;
				else if (m.matches("(b|I|e)"))
					whatnick++;
				else if (m.equals("k"))
				{
					if (give)
						chan.setKey(x[whatnick]);
					else
						chan.setKey(null);
					
					++whatnick;
					
					if (give)
						chan.setMode(m.charAt(0));
					else
						chan.unsetMode(m.charAt(0));
				}
				else if (m.contains("l"))
				{
					if (give)
					{
						chan.setLimit(Integer.parseInt(x[whatnick]));
						whatnick++;
					}
					else
						chan.setLimit(0);
					
					if (give)
						chan.setMode(m.charAt(0));
					else
						chan.unsetMode(m.charAt(0));
				}
				else if (m.matches("(v|h|o|a|q)"))
				{
					if (give)
						chan.setMode(User.toName(x[whatnick]), m);
					else
						chan.remMode(User.toName(x[whatnick]), m);
					
					whatnick++;
				}
				else
				{
					if (give)
						chan.setMode(m.charAt(0));
					else
						chan.unsetMode(m.charAt(0));
				}
			}
		}
		
		for (Event e : Event.getEvents())
			e.onChanModes(creator, chan, modes);
	}
	
	public static void setMode(String source, String target, String modes)
	{
		Channel c = Channel.findChannel(target);
		if (c != null)
			onChanMode(source, c, modes);
		
		Protocol.mode(source, target, modes);
	}

	// The only calling instance, KKill, already checks if user exists
	public static void skill(User u, String msg)
	{
		Protocol.kill(u.getUID(), msg);
		u.onQuit();
		onQuit(u, "Killed (" + me.getName() + " (" + msg + "))");
	}

	public static void onSquit(Server server)
	{
		for (Event e : Event.getEvents())
			e.onServerDelink(server);
	}

	public static void privmsg(final Channel recipient, String message)
	{
		String crypto_pass = Acidictive.conf.getCryptoPass(recipient.getName());
		if (crypto_pass != null)
		{
			Blowfish bf = new Blowfish(crypto_pass);
			message = bf.Encrypt(message);
			log.log(Level.FINE, "Blowfish encrypted:" + message);
		}

		Protocol.privmsg(conf.general.control, recipient.getName(), message);
	}

	public static void privmsg(final User recipient, String message)
	{
		Protocol.privmsg(conf.general.control, recipient.getUID(), message);
	}

	//@Deprecated This should be deprecated however I need the warnings to stfu atm in eclipse
	public static void privmsg(final String recipient, String message)
	{
		privmsg(conf.general.control, recipient, message);
	}
	
	public static void privmsg(String source, String target, String message)
	{
		if (target.startsWith("#"))
		{
			String crypto_pass = Acidictive.conf.getCryptoPass(target);
			if (crypto_pass != null)
			{
				Blowfish bf = new Blowfish(crypto_pass);
				message = bf.Encrypt(message);
				log.log(Level.FINE, "Blowfish encrypted:" + message);
			}
		}
		Protocol.privmsg(source, target, message);
	}

	public static void notice(final Channel recipient, final String message)
	{
		Protocol.notice(conf.general.control, recipient.getName(), message);
	}

	public static void notice(final User recipient, final String message)
	{
		Protocol.notice(conf.general.control, recipient.getUID(), message);
	}
	
	public static void notice(final String source, final String recipient, final String message)
	{
		Protocol.notice(source, recipient, message);
	}

	@Deprecated
	public static void notice(final String recipient, final String message)
	{
		Protocol.notice(conf.general.control, recipient, message);
	}

	@Deprecated
	public static void sNotice(final String recipient, final String message)
	{
		Protocol.notice(me.getSID(), recipient, message);
	}

	static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

	public static void log(String str)
	{
		Calendar now = Calendar.getInstance();
		File f = new File("logs//acidictive.log." + formatter.format(now.getTime()));
		now.clear();
		now = null;
		String date = "[" + new Date().toString() + "]: ";
		BufferedWriter print = createWriter(f, true);
		try
		{
			print.write(date + str);
			print.newLine();
			print.close();
			print = null;
		}
		catch (Exception e)
		{
			log.log(Level.WARNING, "Unable to write to logfile", e);
		}
		f = null;
	}

	public static BufferedWriter createWriter(File f, boolean append)
	{
		// delete the file if we are told not to append
		if (!append)
			f.delete();
		// check if the folder exists, create if it doesn't
		if (f.getParentFile() != null && !f.getParentFile().exists())
			f.getParentFile().mkdir();
		// check if the file exists, create if it doesn't
		if (!f.exists())
		{
			try
			{
				f.createNewFile();
			}
			catch (Exception e)
			{
				log.log(Level.WARNING, "Unable to create file", e);
			}
		}
		// all successful, let's create and return the BufferedWriter
		try
		{
			return new BufferedWriter(new FileWriter(f.getPath(), append));
		}
		catch (Exception e)
		{
			log.log(Level.WARNING, e.getMessage(), e);
		}
		return null;
	}

	public static void reply(String source, String target, String msg)
	{
		if (!target.startsWith("#"))
		// Reply to PMs in notice, reply to channel in privmsg
			/* TODO: How the hell do we avoid using notice(String,String)?
			 * Thinking of adding a "Target" interface to make this at least
			 * target-agnostic.
			 */
			notice(source, msg);
		else
			privmsg(target, msg);
	}
	
	public static void reply(User source, AcidUser to, Channel c, String message)
	{
		if (c != null)
			privmsg(to.getNick(), c.getName(), message);
		else
			notice(to.getNick(), source.getNick(), message);
	}
	
	public static void loadClients(Plugin pkg, List<Client> clients)
	{
		for (Client c : clients)
		{
			User u = User.findUser(c.nick);
			if (u != null)
			{
				if (u instanceof AcidUser)
				{
					AcidUser au = (AcidUser) u;
					
					// update config and plugin
					au.client = c;
					au.pkg = pkg;
				}
				continue;
			}
			
			AcidUser au = new AcidUser(pkg, c);
			if (syncTime != 0)
				au.introduce();
		}
	}
}
