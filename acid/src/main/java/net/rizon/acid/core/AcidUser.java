package net.rizon.acid.core;

import java.util.List;

import net.rizon.acid.conf.Client;

public class AcidUser extends User
{
	public Plugin pkg;
	public Client client;

	public AcidUser(final String nick, final String user, final String host, final String vhost, final String name, final String modes)
	{
		super(nick, user, host, vhost, name, AcidCore.me, AcidCore.getTS(), AcidCore.getTS(), modes, User.generateUID(), "255.255.255.255");
	}
	
	public AcidUser(Plugin pkg, Client c)
	{
		super(c.nick, c.user, c.host, c.vhost, c.name, AcidCore.me, AcidCore.getTS(), AcidCore.getTS(), c.modes, User.generateUID(), "255.255.255.255");
		this.pkg = pkg;
		this.client = c;
	}
	
	public void introduce()
	{
		Protocol.uid(this);

		if (this.getNSPass() != null && !this.getNSPass().isEmpty())
			Protocol.privmsg(this.getUID(), "NickServ@services.rizon.net", "IDENTIFY " + this.getNSPass()); // XXX
		
		if (client != null && client.channels != null)
			for (String c : client.channels)
				this.joinChan(Acidictive.conf.getChannelNamed(c));
	}
	
	public void joinChan(final String channel)
	{
		if (this.isOnChan(channel))
			return;
		
		Channel chan = Channel.findChannel(channel);
		if (chan == null)
		{
			chan = new Channel(channel, AcidCore.getTS());
			chan.setMode('n'); chan.setMode('t');
		}
		Protocol.join(this, chan);
		chan.addUser(this.getNick(), "");
		this.addChan(chan.getName(), "" + AcidCore.getTS());
	}
	
	public void partChan(final String channel)
	{
		if (!this.isOnChan(channel))
			return;
		
		Protocol.part(this, channel);
		Channel chan = Channel.findChannel(channel);
		if (chan != null)
			chan.removeUser(this.getNick());
		this.remChan(channel);
	}
	
	public void quit(final String reason)
	{
		Protocol.quit(this, reason);
		this.onQuit();
	}
	
	public final String getNSPass()
	{
		if (client != null)
			return client.nspass;
		else
			return null;
	}
	
	public net.rizon.acid.conf.Command findConfCommand(final String name)
	{
		if (client != null && this.client.commands != null)
			for (net.rizon.acid.conf.Command c : this.client.commands)
				if (c.name.equalsIgnoreCase(name))
					return c;
		return null;
	}
	
	public List<net.rizon.acid.conf.Command> getConfCommands()
	{
		return this.client.commands;
	}
}
