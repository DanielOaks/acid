package net.rizon.acid.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;

public class Channel
{
	private static final Logger log = Logger.getLogger(Channel.class.getName());
	
	private int ts;
	private String channel;
	// User list: nick => channel user status modes. TreeMap auto-sorts keys
	private TreeMap<String, CModes> list = new TreeMap<String, CModes>();
	private String modes = "";
	private String key;
	private int limit;

	public Channel(String channel, int ts)
	{
		this.ts = ts;
		this.channel = channel;
		
		channelMap.put(channel.toLowerCase(), this);
	}
	
	public void destroy()
	{
		channelMap.remove(this.channel.toLowerCase());
	}
	
	public Set<String> getUsers()
	{
		return list.keySet();
	}

	public void addUser(String nick, String modes)
	{
		list.put(nick.toLowerCase(), new CModes(modes));
	}

	public void removeUser(String nick)
	{
		list.remove(nick.toLowerCase());
		if (list.isEmpty() && !hasMode('z'))
			this.destroy();
	}

	public void setMode(String nick, String mode)
	{
		CModes user = list.get(nick.toLowerCase());
		if (user != null)
		{
			user.addMode(mode);
		}
	}

	public void remMode(String nick, String mode)
	{
		CModes user = list.get(nick.toLowerCase());
		if (user != null)
		{
			user.remMode(mode);
		}
	}

	public String getName()
	{
		return channel;
	}
	
	public int getTS()
	{
		return ts;
	}
	
	public void reset(int ts)
	{
		log.log(Level.FINE, "Lowering TS of " + channel + " from " + this.ts + " to " + ts);
		
		this.ts = ts;
		for (CModes cm : list.values())
			cm.clear();
		this.modes = "";
		this.key = null;
		this.limit = 0;
	}

	public int size()
	{
		return list.size();
	}
	
	public void setMode(char mode)
	{
		if (modes.indexOf(mode) == -1)
			modes += mode;
	}
	
	public void unsetMode(char mode)
	{
		modes = modes.replace("" + mode, "");
	}
	
	public boolean hasMode(char mode)
	{
		return modes.indexOf(mode) != -1;
	}
	
	public String getKey()
	{
		return key;
	}
	
	public int getLimit()
	{
		return limit;
	}
	
	public void setKey(String key)
	{
		this.key = key;
	}
	
	public void setLimit(int limit)
	{
		this.limit = limit;
	}
	
	public String getModes(boolean full)
	{
		if (!full)
			return modes;
		else
		{
			String m = modes;
			int lim = m.indexOf('l');
			int key = m.indexOf('k');
			if (lim != -1 && key != -1)
				if (lim < key)
					m += " " + limit + " " + key;
				else
					m += " " + key + " " + limit;
			else if (lim != -1)
				m += " " + limit;
			else if (key != -1)
				m += " " + key;
			return m;
		}
	}

	public String getModes(String nick)
	{
		CModes user = list.get(nick.toLowerCase());
		if (user != null)
		{
			return user.getModes();
		}
		return "";
	}

	public void onNick(String oldnick, String newnick)
	{
		CModes user = list.get(oldnick.toLowerCase());
		if (user != null)
		{
			list.remove(oldnick.toLowerCase());
			list.put(newnick.toLowerCase(), user);
		}
		else
			log.log(Level.WARNING, "Channel::onNick unknown nick change for " + oldnick + " -> " + newnick);
	}


	public void listUsers(User target)
	{
		ArrayList<String> owners = new ArrayList<String>();
		ArrayList<String> admins = new ArrayList<String>();
		ArrayList<String> ops = new ArrayList<String>();
		ArrayList<String> hops = new ArrayList<String>();
		ArrayList<String> voices = new ArrayList<String>();
		ArrayList<String> regs = new ArrayList<String>();
		CModes modes;
		User user;
		for (String s : list.keySet())
		{
			modes = list.get(s);
			user = User.findUser(s);
			if (modes != null && user != null)
			{
				if (modes.hasOwner())
					owners.add(modes.getModes() + user.getNString());
				else if (modes.hasAdmin())
					admins.add(modes.getModes() + user.getNString());
				else if (modes.hasOp())
					ops.add(modes.getModes() + user.getNString());
				else if (modes.hasHalfop())
					hops.add(modes.getModes() + user.getNString());
				else if (modes.hasVoice())
					voices.add(modes.getModes() + user.getNString());
				else
					regs.add(modes.getModes() + user.getNString());
			}
		}
		for (int i = 0; i < owners.size(); i++)
			Acidictive.notice(target, owners.get(i));
		for (int i = 0; i < admins.size(); i++)
			Acidictive.notice(target, admins.get(i));
		for (int i = 0; i < ops.size(); i++)
			Acidictive.notice(target, ops.get(i));
		for (int i = 0; i < hops.size(); i++)
			Acidictive.notice(target, hops.get(i));
		for (int i = 0; i < voices.size(); i++)
			Acidictive.notice(target, voices.get(i));
		for (int i = 0; i < regs.size(); i++)
			Acidictive.notice(target, regs.get(i));
		Acidictive.notice(target, "Listed " + (owners.size() + admins.size() + ops.size() + hops.size() + voices.size() + regs.size()) + " users that are currently in " + channel);
	}
	
	private static HashMap<String, Channel> channelMap = new HashMap<String, Channel>();
	
	public static Set<String> getChannels()
	{
		return channelMap.keySet();
	}
	
	public static Channel findChannel(final String name)
	{
		return channelMap.get(name.toLowerCase());
	}
}
