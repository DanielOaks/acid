package net.rizon.acid.core;

import java.lang.reflect.Constructor;
import java.util.LinkedList;

import net.rizon.acid.util.ClassLoader;

public abstract class Plugin
{
	private static final String pluginBase = "net.rizon.acid.plugins.";
	private String name;
	private boolean permanent;
	public ClassLoader loader; // Loader for this class
	
	protected Plugin()
	{
		plugins.add(this);
	}
	
	public void remove()
	{
		for (User u : User.getUsersC())
			if (u instanceof AcidUser)
			{
				AcidUser au = (AcidUser) u;
				if (au.pkg == this)
					au.pkg = null;
			}

		this.stop();
		plugins.remove(this);
	}

	protected void setPermanent()
	{
		permanent = true;
	}

	public boolean isPermanent()
	{
		return permanent;
	}
	
	public String getName()
	{
		return name;
	}
	
	public abstract void start() throws Exception;
	public abstract void stop();
	public void reload() throws Exception { }
	
	public static Plugin loadPlugin(String name) throws Exception
	{
		Plugin p = findPlugin(name);
		if (p != null)
			return p;
		
		String finalName = name.substring(name.lastIndexOf('.') + 1);

		ClassLoader cl = new ClassLoader(name, pluginBase + name);
		Class<?> c = cl.loadClass(pluginBase + name + "." + finalName);
		Constructor<?> con = c.getConstructor();
		p = (Plugin) con.newInstance();
		p.name = name;
		p.loader = cl;
		p.start();
		return p;
	}
	
	private static LinkedList<Plugin> plugins = new LinkedList<Plugin>();
	
	public static final Plugin[] getPlugins()
	{
		Plugin[] a = new Plugin[plugins.size()];
		plugins.toArray(a);
		return a;
	}
	
	public static Plugin findPlugin(String name)
	{
		for (Plugin p : plugins)
			if (p.name.equals(name))
				return p;
		return null;
	}
	
	public static Plugin findPluginByClassName(String name)
	{
		if (!name.startsWith(Plugin.pluginBase))
			return null;
		
		name = name.substring(Plugin.pluginBase.length());
		int idx = name.indexOf('.');
		if (idx != -1)
			name = name.substring(0, idx);
		
		for (final Plugin p : plugins)
		{
			if (p.getName().equals(name))
				return p;
		}
		
		return null;
	}
}
