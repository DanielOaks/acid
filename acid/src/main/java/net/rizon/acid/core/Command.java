package net.rizon.acid.core;

public abstract class Command
{	
	private int min;
	private int max;
	
	protected Command(int min, int max)
	{
		this.min = min;
		this.max = max;
	}
	
	public void remove()
	{
	}

	public int GetMinArgs()
	{
		return this.min;
	}
	
	public int GetMaxArgs()
	{
		return this.max;
	}
	
	public abstract void Run(User source, AcidUser to, Channel c, final String[] args);
	
	// For HELP COMMAND
	public boolean onHelpCommand(User u, AcidUser to, Channel c) { return false; }
	
	// For just HELP
	public void onHelp(User u, AcidUser to, Channel c)
	{
		//Acidictive.reply(u, to, c, "\2" + this.GetName().toLowerCase() + "\2");
	}
}