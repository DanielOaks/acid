package net.rizon.acid.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;

import net.rizon.acid.conf.Database;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Logger;

public class SQL extends Thread
{
	private static final Logger log = Logger.getLogger(SQL.class.getName());
	private PreparedStatement statement;
	private ResultSet result;
	private String url, username, password;
	private volatile Connection con = null;
	private volatile boolean shuttingDown = false;
	private long last_connect = 0;
	private volatile Object queryLock = new Object();
	private volatile LinkedList<PreparedStatement> pendingQueries = new LinkedList<PreparedStatement>();
	private static final HashMap<String, SQL> connections = new HashMap<String, SQL>();
	private int referenceCount;

	public SQL(final String url, final String username, final String password) throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");

		this.url = url;
		this.username = username;
		this.password = password;
		this.referenceCount = 1;

		this.connect();
		this.start();
	}

	public String version()
	{
		try
		{
			if (this.con != null)
				return this.con.getMetaData().getDatabaseProductName() + "-" + this.con.getMetaData().getDatabaseProductVersion();
		}
		catch (SQLException ex) { }
		return "Unknown";
	}

	public void shutdown()
	{
		if (--this.referenceCount > 0)
			/* Another plugin still needs the connection here */
			return;
		
		this.shuttingDown = true;

		log.log(Level.INFO, "Flushing pending SQL queries...");

		synchronized (this.queryLock)
		{
			this.queryLock.notify();
		}

		while (this.isAlive())
		{
			try
			{
				Thread.sleep(100L);
			}
			catch (InterruptedException ex) { }
		}

		log.log(Level.INFO, "All SQL queries successfully flushed");

		try { this.con.close(); }
		catch (Exception ex) { }
	}

	private void connect() throws SQLException
	{
		if (this.last_connect > System.currentTimeMillis() - 60 * 1000) // one minute
			throw new SQLException("Reconnecting too fast");
		this.last_connect = System.currentTimeMillis();
		this.con = DriverManager.getConnection(this.url, this.username, this.password);

		log.log(Level.INFO, "Successfully connected to " + this.version() + " using " + this.con.getMetaData().getDriverName() + " (" + this.con.getMetaData().getDriverVersion() + ")");
	}

	public PreparedStatement prepare(final String statement) throws SQLException
	{
		this.close(this.statement, this.result);

		if (this.con == null || this.con.isClosed())
		{
			try
			{
				this.connect();
			}
			catch (SQLException ex)
			{
				handleException("Unable to connect to SQL", ex);
				throw ex;
			}
		}

		this.statement = this.con.prepareStatement(statement);
		return this.statement;
	}

	public void executeThread(PreparedStatement statement)
	{
		this.statement = null;
		this.result = null;

		synchronized (this.queryLock)
		{
			this.pendingQueries.addLast(statement);
			this.queryLock.notify();
		}
	}

	public int executeUpdateBlocking(PreparedStatement statement) throws SQLException
	{
		int i = statement.executeUpdate();
		log.log(Level.FINE, "Successfully executed " + statement);
		return i;
	}

	public ResultSet executeQuery(PreparedStatement statement) throws SQLException
	{
		this.result = statement.executeQuery();

		log.log(Level.FINE, "Successfully executed " + statement);

		return this.result;
	}

	public PreparedStatement persist()
	{
		PreparedStatement stmt = this.statement;
		this.statement = null;
		this.result = null;
		return stmt;
	}

	public void close(PreparedStatement p, ResultSet r)
	{
		try { if (r != null) r.close(); }
		catch (Exception ex) { }

		try { if (p != null) p.close(); }
		catch (Exception ex) { }
	}

	public int getPendingQueryCount()
	{
		synchronized (this.queryLock)
		{
			return this.pendingQueries.size();
		}
	}
	
	public void setAutoCommit(boolean state) throws SQLException
	{
		this.con.setAutoCommit(state);
	}

	@Override
	public void run()
	{
		while (true)
		{
			PreparedStatement q = null;

			synchronized (this.queryLock)
			{
				if (this.pendingQueries.isEmpty() == false)
					q = this.pendingQueries.remove();
				else
				{
					if (this.shuttingDown)
						break;

					try
					{
						this.queryLock.wait();
					}
					catch (InterruptedException e) { }
				}
			}

			if (q != null)
			{
				try
				{
					q.executeUpdate();

					log.log(Level.FINE, "Successfully executed " + q + " in worker thread");
				}
				catch (SQLException ex)
				{
					handleException("Unable to execute query in worker thread: " + q, ex);
				}

				this.close(q, null);
			}
		}
	}
	
	public void increaseReferenceCount()
	{
		this.referenceCount++;
	}

	private static long lastWarn = 0;
	public static void handleException(final String reason, SQLException ex)
	{
		long now = System.currentTimeMillis() / 1000L;
		if (lastWarn + 60 < now)
		{
			log.log(Level.SEVERE, reason, ex);
			lastWarn = now;
		}
	}
	
	public static void shutdownAllConnections()
	{
		for (final SQL conn : SQL.connections.values())
			conn.shutdown();
		
		SQL.connections.clear();
	}
	
	public static SQL getConnection(final String name)
	{
		SQL sql = null;
		
		try
		{
			for (Database d : Acidictive.conf.database)
			{
				if (!d.name.equals(name))
					continue;
				
				if (SQL.connections.containsKey(name))
				{
					sql = SQL.connections.get(name);
					sql.increaseReferenceCount();
					return sql;
				}
				
				sql = new SQL(d.host, d.user, d.pass);
				SQL.connections.put(d.name, sql);
			}
		}
		catch (ClassNotFoundException ex)
		{
			throw new RuntimeException(ex.getMessage());
		}
		catch (SQLException e)
		{
			throw new RuntimeException(e.getMessage());
		}
		
		return sql;
	}
}
