package net.rizon.acid.util;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util
{
	// *** DIZZY ***
	public static boolean iswm(String matchtxt, String txt)
	{
		// simple check to see if matchtxt isin txt. uses mirc format *bl?h*
		// etc..
		String mt = "";
		for (int x = 0; x < matchtxt.length(); x++)
		{
			if (matchtxt.substring(x, x + 1).equals("*"))
			{
				mt += ".*";
			}
			else if (matchtxt.substring(x, x + 1).equals("?"))
			{
				mt += ".";
			}
			else if (matchtxt.substring(x, x + 1).equals("^"))
			{
				mt += "\\^";
			}
			else if (matchtxt.substring(x, x + 1).equals("$"))
			{
				mt += "\\$";
			}
			else if (matchtxt.substring(x, x + 1).equals("("))
			{
				mt += "\\(";
			}
			else if (matchtxt.substring(x, x + 1).equals(")"))
			{
				mt += "\\)";
			}
			else if (matchtxt.substring(x, x + 1).equals("["))
			{
				mt += "\\[";
			}
			else if (matchtxt.substring(x, x + 1).equals("]"))
			{
				mt += "\\]";
			}
			else if (matchtxt.substring(x, x + 1).equals("."))
			{
				mt += "\\.";
			}
			else if (matchtxt.substring(x, x + 1).equals("|"))
			{
				mt += "\\|";
			}
			else if (matchtxt.substring(x, x + 1).equals("+"))
			{
				mt += "\\+";
			}
			else if (matchtxt.substring(x, x + 1).equals("{"))
			{
				mt += "\\{";
			}
			else if (matchtxt.substring(x, x + 1).equals("}"))
			{
				mt += "\\}";
			}
			else if (matchtxt.substring(x, x + 1).equals("\\"))
			{
				mt += "\\\\";
			}
			else
			{
				mt += matchtxt.substring(x, x + 1);
			}
		}
		Pattern ppp = Pattern.compile("(?uis)^" + mt + "$");
		Matcher mmm = ppp.matcher(txt);
		if (mmm.find() == true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	

	public static int getSeconds(String time) throws NumberFormatException
	{
		int rtime = 0;
		if (iswm("*h", time))
		{
			time = time.substring(0, time.length() - 1);
			rtime = Integer.parseInt(time);
			rtime = rtime * 60 * 60;
			return rtime;
		}
		else if (iswm("*m", time))
		{
			time = time.substring(0, time.length() - 1);
			rtime = Integer.parseInt(time);
			rtime = rtime * 60;
			return rtime;
		}
		else if (iswm("*d", time))
		{
			time = time.substring(0, time.length() - 1);
			rtime = Integer.parseInt(time);
			rtime = rtime * 24 * 60 * 60;
			return rtime;
		}
		return rtime;
	}
	
	public static String fTime(int seconds)
	{
		int weeks = seconds / 604800;
		int days = seconds % 604800 / 86400;
		int hours = seconds % 86400 / 3600;
		int minutes = seconds % 3600 / 60;
		seconds = seconds % 60;
		StringBuffer str = new StringBuffer();
		if (weeks == 1)
		{
			str.append(" " + weeks + "w");
		}
		else if (weeks > 1)
		{
			str.append(" " + weeks + "w");
		}
		if (days == 1)
		{
			str.append(" " + days + "d");
		}
		else if (days > 1)
		{
			str.append(" " + days + "d");
		}
		if (hours == 1)
		{
			str.append(" " + hours + "h");
		}
		else if (hours > 1)
		{
			str.append(" " + hours + "h");
		}
		if (minutes == 1)
		{
			str.append(" " + minutes + "m");
		}
		else if (minutes > 1)
		{
			str.append(" " + minutes + "m");
		}
		if (seconds == 1)
		{
			str.append(" " + seconds + "s");
		}
		if (seconds > 1)
		{
			str.append(" " + seconds + "s");
		}
		return str.toString();
	}
	
	public static String safeRegex(String str)
	{
		String sr = "";
		for (int x = 0; x < str.length(); x++)
		{
			if (str.substring(x, x + 1).equals("*"))
			{
				sr += "\\*";
			}
			else if (str.substring(x, x + 1).equals("?"))
			{
				sr += "\\?";
			}
			else if (str.substring(x, x + 1).equals("^"))
			{
				sr += "\\^";
			}
			else if (str.substring(x, x + 1).equals("$"))
			{
				sr += "\\$";
			}
			else if (str.substring(x, x + 1).equals("("))
			{
				sr += "\\(";
			}
			else if (str.substring(x, x + 1).equals(")"))
			{
				sr += "\\)";
			}
			else if (str.substring(x, x + 1).equals("["))
			{
				sr += "\\[";
			}
			else if (str.substring(x, x + 1).equals("]"))
			{
				sr += "\\]";
			}
			else if (str.substring(x, x + 1).equals("."))
			{
				sr += "\\.";
			}
			else if (str.substring(x, x + 1).equals("|"))
			{
				sr += "\\|";
			}
			else if (str.substring(x, x + 1).equals("+"))
			{
				sr += "\\+";
			}
			else if (str.substring(x, x + 1).equals("{"))
			{
				sr += "\\{";
			}
			else if (str.substring(x, x + 1).equals("}"))
			{
				sr += "\\}";
			}
			else if (str.substring(x, x + 1).equals("\\"))
			{
				sr += "\\\\";
			}
			else
			{
				sr += str.substring(x, x + 1);
			}
		}
		return sr;
	}
	
	public static String randomString(int length)
	{
		Random rand = new Random();
		StringBuffer rS = new StringBuffer();
		for (int i = 0; i < length; i++)
		{
			rS.append(randChars.charAt(rand.nextInt(randChars.length())));
		}
		return rS.toString();
	}

	private static String randChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	public static String formatTime(int seconds)
	{
		int weeks = seconds / 604800;
		int days = seconds % 604800 / 86400;
		int hours = seconds % 86400 / 3600;
		int minutes = seconds % 3600 / 60;
		seconds = seconds % 60;
		StringBuffer str = new StringBuffer();
		if (weeks > 0)
			str.append(weeks + " weeks, ");
		if (days > 0)
			str.append(days + " days, ");
		if (hours > 0)
			str.append(hours + " hours, ");
		if (minutes > 0)
			str.append(minutes + " minutes, ");
		str.append(seconds + " seconds");
		return str.toString();
	}
}