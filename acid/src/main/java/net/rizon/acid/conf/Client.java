package net.rizon.acid.conf;

import java.util.List;

public class Client implements Validatable
{
	public String nick, user, host, vhost, name, nspass, modes, version;
	public List<String> channels;
	public List<Command> commands;
	
	public Command findCommand(String name)
	{
		for (Command c : commands)
			if (c.name.equalsIgnoreCase(name))
				return c;
		return null;
	}

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("nick", nick);
		Validator.validateNotEmpty("user", user);
		Validator.validateNotEmpty("host", host);
		Validator.validateNotEmpty("vhost", vhost);
		Validator.validateNotEmpty("name", name);
		Validator.validateNotEmpty("modes", modes);
		Validator.validateList(commands);
	}
}