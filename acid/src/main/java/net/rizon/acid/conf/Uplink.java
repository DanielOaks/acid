package net.rizon.acid.conf;

public class Uplink implements Validatable
{
	public String host, pass;
	public int port;
	public boolean ssl;
	
	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("host", host);
		Validator.validateNotEmpty("pass", pass);
		Validator.validatePort("port", port);
	}
}