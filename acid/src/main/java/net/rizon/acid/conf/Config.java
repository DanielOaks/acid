package net.rizon.acid.conf;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.CustomClassLoaderConstructor;

public class Config extends Configuration
{
	public boolean debug, protocol_debug;
	public ServerInfo serverinfo;
	public Uplink uplink;
	public General general;
	public List<Database> database;
	public List<Client> clients;
	public List<Channel> channel;
	public List<String> plugins;
	public List<AccessPreset> access_preset;
	
	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotNull("serverinfo", serverinfo);
		Validator.validateNotNull("uplink", uplink);
		Validator.validateNotNull("general", general);
		
		serverinfo.validate();
		uplink.validate();
		general.validate();
		
		Validator.validateList(database);
		Validator.validateList(clients);
		Validator.validateList(channel);
	}
	
	public void rehash(Config other)
	{
		// Rehashing, other = newer config
		
		other.serverinfo = this.serverinfo;
		other.database = this.database;
	}
	
	public String getChannelNamed(String name)
	{
		for (Channel c : channel)
			if (c.name.equals(name))
				return c.channel;
		if (name.startsWith("#"))
			return name;
		return "#a";
	}
	
	public String getCryptoPass(String ch)
	{
		for (Channel c : channel)
			if (c.channel.equalsIgnoreCase(ch))
				return c.blowfish;
		return null;
	}

	public static Configuration load(String file, Class<?> c) throws Exception
	{
		CustomClassLoaderConstructor ctor = new CustomClassLoaderConstructor(c, c.getClassLoader());
		Yaml yaml = new Yaml(ctor);
		InputStream io = null;
		try
		{
			io = new FileInputStream(file);
			Configuration conf = (Configuration) yaml.load(io);
			conf.validate();
			return conf;
		}
		finally
		{
			try { io.close(); }
			catch (Exception ex) { }
		}
	}
}
