package net.rizon.acid.conf;

import java.util.Collection;
import java.util.List;

public class Validator
{
	public static <T extends Validatable> void validateList(List<T> list) throws ConfigException
	{
		if (list == null)
			return;
		for (T t : list)
			t.validate();
	}
	
	public static <T> void validateNotNull(String name, T obj) throws ConfigException
	{
		if (obj == null)
			throw new ConfigException(name + " must be defined");
	}
	
	public static void validateNotEmpty(String name, String obj) throws ConfigException
	{
		validateNotNull(name, obj);
		if (obj.isEmpty())
			throw new ConfigException(name + " must not be empty");
	}
	
	public static <T extends Collection<?>> void validateNotEmpty(String name, T obj) throws ConfigException
	{
		validateNotNull(name, obj);
		if (obj.isEmpty())
			throw new ConfigException(name + " must not be empty");
	}
	
	public static void validateNotZero(String name, int i) throws ConfigException
	{
		if (i == 0)
			throw new ConfigException(name + " must be non zero");
	}
	
	public static void validatePort(String name, int i) throws ConfigException
	{
		if (i < 0 || i > 65535)
			throw new ConfigException(name + " (" + i + ") is not a valid port");
	}
}